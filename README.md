# Xilinx ZCU102 Memory Tester

The following repository contains the code implementation of a memory testing environment based on a Xilinx ZCU102 MPSoc. It provides functionality to send commands to the CPU on the board, e.g., to start read-latency tests. These commands are parsed and forwarded to the Programmable Logic, which implements a bare-metal memory controller, executing the tests on the connected memory module.

This environment consists of three basic components: 

1. **Custom SRAM Memory Controller:**
   - Utilizes GPIO pins of the FMC interface to seamlessly interact with memory modules supporting the SRAM protocol.

2. **PS/PL Interconnection Module:**
   - Orchestrates commands received from the CPU, exercising control over the memory controller.

3. **Firmware on the CPU:**
   - Implements a sophisticated IP-Network stack.
   - Houses a command parser that transforms input data into a format interpretable by the FPGA.
   - Features an implementation that dispatches parsed data through memory mapping to the AXI-interface.

An overview of the different used components and software stacks on the ZCU102 are displayed in the following figure: 

<img src='./docs/figures/system_overview.png' alt='system overview'/>

### Software-Stack running on AMD A53 

This sample design is running a bare-metal program on the First core of the Cortex ARM A53. This application is based on the exported Hardware design and exported code of the Block design explained later.

##### LWIP Network Stack
 It uses the LWIP IP-Stack to implement a TCP-interface accross the TI DP83867 Ethernet using the Reduced Gigabit Media Independet Interface (RGMII). It provides DHCP functionallity. 

In case of a TCP timeout the following default parameters are assigned: 

  |                     |                  | 
  |---------------------|------------------|
  | **IP Address**      |  192.168.1.10    |
  | **Network Mask**    |  255.255.255.0   |
  | **Default Gateway** |  192.168.1.1     |

After the setup of the network stack is completed, the device answers ICMP package and provides an TCP socket to receive commands on port **5025**.

##### Command Parser

The network interface accepts commands of the following json-like format: 

```json
<Command>\n
{"payload": { "key1": "value1", "key2": "value2"}}
```

e.g.

```json
MEASUREMENT START\n
{"payload": { "measure_type": "Read Latency", "start_address": "0x00", "stop_address": "0xffffffff", "init_value": "0x5555", "auxiliary_0": "0x46", "auxiliary_1": "trc"}}
```
Caution: Currently, all parameters must be enclosed in quotation marks! The self-implemented JSON parser uses them for parsing the values.

Currently the following commands are implemented 

| Command            | Identifier          | Description                                                 | Return                                               |
| ------------------- | -------------------- | ----------------------------------------------------------- | ----------------------------------------------------- |
| Identify            | `*IDN?\n`            | Returns a device identifier, used for device discovery.      | "Zynq Ultrascale+ MPSoC ZCU102 Evaluation Kit"       |
| Start Measurement   | `START MEASUREMENT\n` | Schedules a measurement on the PL, e.g., Row-Hammering, Read-Latency, or Write-Latency measurement | `{"status": "ok"}` when no error occurs                |
| Stop Measurement    | `STOP MEASUREMENT\n`  | Stops the measurement on the PL, when `status == running`, without resetting all settings on the PL | `{"status": "ok"}` when no error occurs               |
| Retrieve Status     | `STATUS\n`           | Returns the status of the current test execution            | e.g., `{"status": "running", "progress": "47"}` when the test is running and the progress is 47% |
| Reset PL            | `RESET\n`             | Resets all settings on the PL                                | `{"status": "ok"}` when no error occurs                |
         

When executing the **Start Measurement** command the following parameters can be set:

For **testType**:

| Value            | Description                     |
| ---------------- | ------------------------------- |
| "Read Latency"   | Executes a Read Latency Test.   |
| "Write Latency"  | Executes a Write Latency Test.  |
| "Row Hammering"  | Executes a Row-Hammering Test.  |


In the case of *Read Latency* Tests, the following parameters can be set:

| Key                          | Example         | Bit-Width | Description                                                                                       |
|------------------------------|-----------------|-----------|---------------------------------------------------------------------------------------------------|
| startAddress                 | e.g. 0x00000000 | 32-bit    | The address where the PL-module starts executing the test.                                      |
| stopAddress                  | e.g. 0xffffffff | 32-bit    | Defines the last address to be considered in the test.                                            |
| initValue                    | e.g. 0xaaaa      | 16-bit    | Defines the value with which all cells within the address range are initialized under normal timing conditions. |
| auxiliary_0 (writeCycleTime) | e.g. 0x00000046 | 32-bit    | Defines the write cycle time in nanoseconds, executed under normal conditions.                    |
| auxiliary_1 (readCycleTime)  | e.g. 0x00000036 | 32-bit    | Defines the read cycle time in nanoseconds, applied when executing read-latency tests.            |
| auxiliary_2                  | -               | 32-bit    | Unused.                                                                                                         |
| auxiliary_3                  | -               | 32-bit    | Unused.                                                                                                         |
| auxiliary_4                  | -               | 32-bit    | Unused.                                                                                                         |

Therefore, an example *Read Latency* command can have the following format:

```json
START MEASUREMENT\n
{
  "payload": {
    "measure_type": "Read Latency",
    "start_address": "0x00000000",
    "stop_address": "0x0000ffff",
    "init_value": "0x5555",
    "auxilary_0": "0x00000046",
    "auxilary_1": "0x00000036"
    "auxilary_2": "0x00000040"
    "auxilary_3": "0x00000000"
    "auxilary_4": "0x00000000"
  }
}
```


In the case of *Write Latency* Tests, the following parameters can be set:

| Key                                | Example         | Bit-Width | Description                                                                                       |
|------------------------------------|-----------------|-----------|---------------------------------------------------------------------------------------------------|
| start_address                      | e.g. 0x00000000 | 32-bit    | The address where the PL-module starts executing the test.                                      |
| stop_address                       | e.g. 0xffffffff | 32-bit    | Defines the last address to be considered in the test.                                            |
| init_value                         | e.g. 0xaaaa     | 16-bit    | Defines the value with which all cells within the address range are initialized under normal timing conditions. |
| auxilary_0 (writeValue)            | e.g. 0x5555     | 32-bit    | Defines the value with which is written with altered timing specifications. |
| auxilary_1 (writeCycleTime)        | e.g. 0x00000046 | 32-bit    | Defines the write cycle time in nanoseconds, executed under normal conditions.                    |
| auxilary_2 (alteredWriteCycleTime) | e.g. 0x00000040 | 32-bit    | Defines the write cycle time under which the tests are conducted.                    | 
| auxiliary_3                        | -               | 32-bit    | Unused.                                                                                                         |
| auxiliary_4                        | -               | 32-bit    | Unused.                                                                                                         |

Therefore, an example *Write Latency* command can have the following format:

```json
START MEASUREMENT\n
{
  "payload": {
    "measure_type": "Write Latency",
    "start_address": "0x00000000",
    "stop_address": "0x0000ffff",
    "init_value": "0xaaaa",
    "auxilary_0": "0x5555",
    "auxilary_1": "0x00000046",
    "auxilary_2": "0x00000040"
    "auxilary_3": "0x00000000"
    "auxilary_4": "0x00000000"
   }
}
```

In the case of *Row Hammering* Tests, the following parameters can be set:

| Key                            | Example         | Bit-Width | Description                                                                                                     |
|--------------------------------|-----------------|-----------|-----------------------------------------------------------------------------------------------------------------|
| start_address                  | e.g. 0x00000000 | 32-bit    | The address where the PL-module starts executing the test.                                                      |
| stop_address                   | e.g. 0xffffffff | 32-bit    | Defines the last address to be considered in the test.                                                          |
| init_value                     | e.g. 0xaaaa     | 16-bit    | Defines the value with which all cells within the address range are initialized under normal timing conditions. |
| auxiliary_0 (hammerValue)      | e.g. 0x5555     | 16-bit    | Defines the value with which data is written when executing row-hammering.                                      |
| auxiliary_1 (hammerIterations) | e.g. 0x03e8     | 32-bit    | Defines the number of row-hammering iterations.                                                                 |
| auxiliary_2 (rowOffset)        | e.g. 0x0040     | 32-bit    | Defines the row length on which row-hammering is performed, followed by a non-hammered row of rowOffset cells.  |
| auxiliary_3                    | -               | 32-bit    | Unused.                                                                                                         |
| auxiliary_4                    | -               | 32-bit    | Unused.                                                                                                         |

Therefore, an example *Row Hammering* command can have the following format:

```json
START MEASUREMENT\n
{
  "payload": {
    "measure_type": "Row Hammering",
    "start_address": "0x00000000",
    "stop_address": "0x0000ffff",
    "init_value": "0xaaaa",
    "auxiliary_0": "0x5555",
    "auxiliary_1": "0x03e8",
    "auxiliary_2": "0x0040",
    "auxiliary_3": "0x0000",
    "auxiliary_4": "0x0000",
  }
}
```

The values are parsed based on the slected 

##### AXI message format

The above mentioned commands must be parsed and transformed into a format which can be parsed on the PL. Therefore AXI light provides four 32-bit registers which can be read simoultanously. 
The data is transformed to an AXI slave interface, defined accessible starting from the address defined in the macro **XPAR_PS_PL_INTERFACE_0_AXI_LIGHT_SLAVE_BASEADDR**. 

The registers must be accessed in a four byte aligned format, which is done by the implementation in *ps_pl_controller.c,* by the fucntion *write_register(reg, value)*. 

In general a data command sent to the PL has the following format: 

<table>
<tr>
<td colspan=3>
Register 1 (32-bit)
</td>
<td>
Register 2 </br>(32-bit)
</td>
<td>
Register 3 </br>(32-bit)
</td>
<td>
Register 4 </br>(32-bit)
</td>
<td>
Register 1 </br>(32-bit)
</td>
<td>
Register 2 </br>(32-bit)
</td>
<td>
Register 3 </br>(32-bit)
</td>
<td>
Register 4 </br>(32-bit)
</td>
</tr>
<tr>
<td>
command (8-bit)
</td>
<td>
subcommand identifier (measure_type) (8-bit)
</td>
<td>
init_value (16-bit)
</td>
<td>
start_address
</td>
<td>
stop_address
</td>
<td>
auxiliary_0
</td>
<td>
auxiliary_1
</td>
<td>
auxiliary_2
</td>
<td>
auxiliary_3
</td>
<td>
auxiliary_4
</td>
</tr>
</table>

The command specific fields are filed orderd according to the json configurations depicted above.