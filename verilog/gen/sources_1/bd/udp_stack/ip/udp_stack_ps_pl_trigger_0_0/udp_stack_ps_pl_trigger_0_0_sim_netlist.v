// Copyright 1986-2022 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2022.1 (lin64) Build 3526262 Mon Apr 18 15:47:01 MDT 2022
// Date        : Thu Feb 29 12:37:56 2024
// Host        : user-OptiPlex-5000 running 64-bit Ubuntu 22.04.3 LTS
// Command     : write_verilog -force -mode funcsim
//               /home/florianfrank/Documents/xilinx_open_source_udp_stack/Verilog_UDP_Stack.gen/sources_1/bd/udp_stack/ip/udp_stack_ps_pl_trigger_0_0/udp_stack_ps_pl_trigger_0_0_sim_netlist.v
// Design      : udp_stack_ps_pl_trigger_0_0
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xczu9eg-ffvb1156-2-e
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CHECK_LICENSE_TYPE = "udp_stack_ps_pl_trigger_0_0,ps_pl_trigger,{}" *) (* DowngradeIPIdentifiedWarnings = "yes" *) (* IP_DEFINITION_SOURCE = "module_ref" *) 
(* X_CORE_INFO = "ps_pl_trigger,Vivado 2022.1" *) 
(* NotValidForBitStream *)
module udp_stack_ps_pl_trigger_0_0
   (clk,
    axi_master_done,
    trigger_axi_master_start,
    output_address,
    output_data);
  (* X_INTERFACE_INFO = "xilinx.com:signal:clock:1.0 clk CLK" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME clk, FREQ_HZ 99990005, FREQ_TOLERANCE_HZ 0, PHASE 0.0, CLK_DOMAIN udp_stack_zynq_ultra_ps_e_0_0_pl_clk0, INSERT_VIP 0" *) input clk;
  input axi_master_done;
  output trigger_axi_master_start;
  output [31:0]output_address;
  output [15:0]output_data;

  wire axi_master_done;
  wire clk;
  wire [31:0]output_address;
  wire [15:0]output_data;
  wire trigger_axi_master_start;

  udp_stack_ps_pl_trigger_0_0_ps_pl_trigger inst
       (.axi_master_done(axi_master_done),
        .clk(clk),
        .output_address(output_address),
        .output_data(output_data),
        .trigger_axi_master_start(trigger_axi_master_start));
endmodule

(* ORIG_REF_NAME = "ps_pl_trigger" *) 
module udp_stack_ps_pl_trigger_0_0_ps_pl_trigger
   (output_address,
    output_data,
    trigger_axi_master_start,
    clk,
    axi_master_done);
  output [31:0]output_address;
  output [15:0]output_data;
  output trigger_axi_master_start;
  input clk;
  input axi_master_done;

  wire \FSM_sequential_state[2]_i_1_n_0 ;
  wire axi_master_done;
  wire clk;
  wire ctr1__14_carry__0_i_1_n_0;
  wire ctr1__14_carry__0_i_2_n_0;
  wire ctr1__14_carry__0_i_3_n_0;
  wire ctr1__14_carry__0_i_4_n_0;
  wire ctr1__14_carry__0_i_5_n_0;
  wire ctr1__14_carry__0_n_4;
  wire ctr1__14_carry__0_n_5;
  wire ctr1__14_carry__0_n_6;
  wire ctr1__14_carry__0_n_7;
  wire ctr1__14_carry_i_10_n_0;
  wire ctr1__14_carry_i_11_n_0;
  wire ctr1__14_carry_i_12_n_0;
  wire ctr1__14_carry_i_13_n_0;
  wire ctr1__14_carry_i_1_n_0;
  wire ctr1__14_carry_i_2_n_0;
  wire ctr1__14_carry_i_3_n_0;
  wire ctr1__14_carry_i_4_n_0;
  wire ctr1__14_carry_i_5_n_0;
  wire ctr1__14_carry_i_6_n_0;
  wire ctr1__14_carry_i_7_n_0;
  wire ctr1__14_carry_i_8_n_0;
  wire ctr1__14_carry_i_9_n_0;
  wire ctr1__14_carry_n_0;
  wire ctr1__14_carry_n_1;
  wire ctr1__14_carry_n_2;
  wire ctr1__14_carry_n_3;
  wire ctr1__14_carry_n_4;
  wire ctr1__14_carry_n_5;
  wire ctr1__14_carry_n_6;
  wire ctr1__14_carry_n_7;
  wire ctr1_carry__0_i_1_n_0;
  wire ctr1_carry__0_i_2_n_0;
  wire ctr1_carry__0_i_3_n_0;
  wire ctr1_carry__0_i_4_n_0;
  wire ctr1_carry__0_i_5_n_0;
  wire ctr1_carry__0_i_6_n_0;
  wire ctr1_carry__0_i_7_n_0;
  wire ctr1_carry__0_n_1;
  wire ctr1_carry__0_n_2;
  wire ctr1_carry__0_n_3;
  wire ctr1_carry__0_n_4;
  wire ctr1_carry__0_n_5;
  wire ctr1_carry__0_n_6;
  wire ctr1_carry__0_n_7;
  wire ctr1_carry_i_10_n_0;
  wire ctr1_carry_i_11_n_0;
  wire ctr1_carry_i_12_n_0;
  wire ctr1_carry_i_1_n_0;
  wire ctr1_carry_i_2_n_0;
  wire ctr1_carry_i_3_n_0;
  wire ctr1_carry_i_4_n_0;
  wire ctr1_carry_i_5_n_0;
  wire ctr1_carry_i_6_n_0;
  wire ctr1_carry_i_7_n_0;
  wire ctr1_carry_i_8_n_0;
  wire ctr1_carry_i_9_n_0;
  wire ctr1_carry_n_0;
  wire ctr1_carry_n_1;
  wire ctr1_carry_n_2;
  wire ctr1_carry_n_3;
  wire ctr1_carry_n_4;
  wire ctr1_carry_n_5;
  wire ctr1_carry_n_6;
  wire ctr1_carry_n_7;
  wire \ctr[0]_i_1_n_0 ;
  wire \ctr[31]_i_1_n_0 ;
  wire \ctr[31]_i_2_n_0 ;
  wire \ctr_reg[16]_i_1_n_0 ;
  wire \ctr_reg[16]_i_1_n_1 ;
  wire \ctr_reg[16]_i_1_n_10 ;
  wire \ctr_reg[16]_i_1_n_11 ;
  wire \ctr_reg[16]_i_1_n_12 ;
  wire \ctr_reg[16]_i_1_n_13 ;
  wire \ctr_reg[16]_i_1_n_14 ;
  wire \ctr_reg[16]_i_1_n_15 ;
  wire \ctr_reg[16]_i_1_n_2 ;
  wire \ctr_reg[16]_i_1_n_3 ;
  wire \ctr_reg[16]_i_1_n_4 ;
  wire \ctr_reg[16]_i_1_n_5 ;
  wire \ctr_reg[16]_i_1_n_6 ;
  wire \ctr_reg[16]_i_1_n_7 ;
  wire \ctr_reg[16]_i_1_n_8 ;
  wire \ctr_reg[16]_i_1_n_9 ;
  wire \ctr_reg[24]_i_1_n_0 ;
  wire \ctr_reg[24]_i_1_n_1 ;
  wire \ctr_reg[24]_i_1_n_10 ;
  wire \ctr_reg[24]_i_1_n_11 ;
  wire \ctr_reg[24]_i_1_n_12 ;
  wire \ctr_reg[24]_i_1_n_13 ;
  wire \ctr_reg[24]_i_1_n_14 ;
  wire \ctr_reg[24]_i_1_n_15 ;
  wire \ctr_reg[24]_i_1_n_2 ;
  wire \ctr_reg[24]_i_1_n_3 ;
  wire \ctr_reg[24]_i_1_n_4 ;
  wire \ctr_reg[24]_i_1_n_5 ;
  wire \ctr_reg[24]_i_1_n_6 ;
  wire \ctr_reg[24]_i_1_n_7 ;
  wire \ctr_reg[24]_i_1_n_8 ;
  wire \ctr_reg[24]_i_1_n_9 ;
  wire \ctr_reg[31]_i_3_n_10 ;
  wire \ctr_reg[31]_i_3_n_11 ;
  wire \ctr_reg[31]_i_3_n_12 ;
  wire \ctr_reg[31]_i_3_n_13 ;
  wire \ctr_reg[31]_i_3_n_14 ;
  wire \ctr_reg[31]_i_3_n_15 ;
  wire \ctr_reg[31]_i_3_n_2 ;
  wire \ctr_reg[31]_i_3_n_3 ;
  wire \ctr_reg[31]_i_3_n_4 ;
  wire \ctr_reg[31]_i_3_n_5 ;
  wire \ctr_reg[31]_i_3_n_6 ;
  wire \ctr_reg[31]_i_3_n_7 ;
  wire \ctr_reg[31]_i_3_n_9 ;
  wire \ctr_reg[8]_i_1_n_0 ;
  wire \ctr_reg[8]_i_1_n_1 ;
  wire \ctr_reg[8]_i_1_n_10 ;
  wire \ctr_reg[8]_i_1_n_11 ;
  wire \ctr_reg[8]_i_1_n_12 ;
  wire \ctr_reg[8]_i_1_n_13 ;
  wire \ctr_reg[8]_i_1_n_14 ;
  wire \ctr_reg[8]_i_1_n_15 ;
  wire \ctr_reg[8]_i_1_n_2 ;
  wire \ctr_reg[8]_i_1_n_3 ;
  wire \ctr_reg[8]_i_1_n_4 ;
  wire \ctr_reg[8]_i_1_n_5 ;
  wire \ctr_reg[8]_i_1_n_6 ;
  wire \ctr_reg[8]_i_1_n_7 ;
  wire \ctr_reg[8]_i_1_n_8 ;
  wire \ctr_reg[8]_i_1_n_9 ;
  wire \ctr_reg_n_0_[0] ;
  wire \ctr_reg_n_0_[10] ;
  wire \ctr_reg_n_0_[11] ;
  wire \ctr_reg_n_0_[12] ;
  wire \ctr_reg_n_0_[13] ;
  wire \ctr_reg_n_0_[14] ;
  wire \ctr_reg_n_0_[15] ;
  wire \ctr_reg_n_0_[16] ;
  wire \ctr_reg_n_0_[17] ;
  wire \ctr_reg_n_0_[18] ;
  wire \ctr_reg_n_0_[19] ;
  wire \ctr_reg_n_0_[1] ;
  wire \ctr_reg_n_0_[20] ;
  wire \ctr_reg_n_0_[21] ;
  wire \ctr_reg_n_0_[22] ;
  wire \ctr_reg_n_0_[23] ;
  wire \ctr_reg_n_0_[24] ;
  wire \ctr_reg_n_0_[25] ;
  wire \ctr_reg_n_0_[26] ;
  wire \ctr_reg_n_0_[27] ;
  wire \ctr_reg_n_0_[28] ;
  wire \ctr_reg_n_0_[29] ;
  wire \ctr_reg_n_0_[2] ;
  wire \ctr_reg_n_0_[30] ;
  wire \ctr_reg_n_0_[31] ;
  wire \ctr_reg_n_0_[3] ;
  wire \ctr_reg_n_0_[4] ;
  wire \ctr_reg_n_0_[5] ;
  wire \ctr_reg_n_0_[6] ;
  wire \ctr_reg_n_0_[7] ;
  wire \ctr_reg_n_0_[8] ;
  wire \ctr_reg_n_0_[9] ;
  wire [31:0]output_address;
  wire \output_address_tmp[7]_i_2_n_0 ;
  wire \output_address_tmp_reg[15]_i_1_n_0 ;
  wire \output_address_tmp_reg[15]_i_1_n_1 ;
  wire \output_address_tmp_reg[15]_i_1_n_10 ;
  wire \output_address_tmp_reg[15]_i_1_n_11 ;
  wire \output_address_tmp_reg[15]_i_1_n_12 ;
  wire \output_address_tmp_reg[15]_i_1_n_13 ;
  wire \output_address_tmp_reg[15]_i_1_n_14 ;
  wire \output_address_tmp_reg[15]_i_1_n_15 ;
  wire \output_address_tmp_reg[15]_i_1_n_2 ;
  wire \output_address_tmp_reg[15]_i_1_n_3 ;
  wire \output_address_tmp_reg[15]_i_1_n_4 ;
  wire \output_address_tmp_reg[15]_i_1_n_5 ;
  wire \output_address_tmp_reg[15]_i_1_n_6 ;
  wire \output_address_tmp_reg[15]_i_1_n_7 ;
  wire \output_address_tmp_reg[15]_i_1_n_8 ;
  wire \output_address_tmp_reg[15]_i_1_n_9 ;
  wire \output_address_tmp_reg[23]_i_1_n_0 ;
  wire \output_address_tmp_reg[23]_i_1_n_1 ;
  wire \output_address_tmp_reg[23]_i_1_n_10 ;
  wire \output_address_tmp_reg[23]_i_1_n_11 ;
  wire \output_address_tmp_reg[23]_i_1_n_12 ;
  wire \output_address_tmp_reg[23]_i_1_n_13 ;
  wire \output_address_tmp_reg[23]_i_1_n_14 ;
  wire \output_address_tmp_reg[23]_i_1_n_15 ;
  wire \output_address_tmp_reg[23]_i_1_n_2 ;
  wire \output_address_tmp_reg[23]_i_1_n_3 ;
  wire \output_address_tmp_reg[23]_i_1_n_4 ;
  wire \output_address_tmp_reg[23]_i_1_n_5 ;
  wire \output_address_tmp_reg[23]_i_1_n_6 ;
  wire \output_address_tmp_reg[23]_i_1_n_7 ;
  wire \output_address_tmp_reg[23]_i_1_n_8 ;
  wire \output_address_tmp_reg[23]_i_1_n_9 ;
  wire \output_address_tmp_reg[31]_i_2_n_1 ;
  wire \output_address_tmp_reg[31]_i_2_n_10 ;
  wire \output_address_tmp_reg[31]_i_2_n_11 ;
  wire \output_address_tmp_reg[31]_i_2_n_12 ;
  wire \output_address_tmp_reg[31]_i_2_n_13 ;
  wire \output_address_tmp_reg[31]_i_2_n_14 ;
  wire \output_address_tmp_reg[31]_i_2_n_15 ;
  wire \output_address_tmp_reg[31]_i_2_n_2 ;
  wire \output_address_tmp_reg[31]_i_2_n_3 ;
  wire \output_address_tmp_reg[31]_i_2_n_4 ;
  wire \output_address_tmp_reg[31]_i_2_n_5 ;
  wire \output_address_tmp_reg[31]_i_2_n_6 ;
  wire \output_address_tmp_reg[31]_i_2_n_7 ;
  wire \output_address_tmp_reg[31]_i_2_n_8 ;
  wire \output_address_tmp_reg[31]_i_2_n_9 ;
  wire \output_address_tmp_reg[7]_i_1_n_0 ;
  wire \output_address_tmp_reg[7]_i_1_n_1 ;
  wire \output_address_tmp_reg[7]_i_1_n_10 ;
  wire \output_address_tmp_reg[7]_i_1_n_11 ;
  wire \output_address_tmp_reg[7]_i_1_n_12 ;
  wire \output_address_tmp_reg[7]_i_1_n_13 ;
  wire \output_address_tmp_reg[7]_i_1_n_14 ;
  wire \output_address_tmp_reg[7]_i_1_n_15 ;
  wire \output_address_tmp_reg[7]_i_1_n_2 ;
  wire \output_address_tmp_reg[7]_i_1_n_3 ;
  wire \output_address_tmp_reg[7]_i_1_n_4 ;
  wire \output_address_tmp_reg[7]_i_1_n_5 ;
  wire \output_address_tmp_reg[7]_i_1_n_6 ;
  wire \output_address_tmp_reg[7]_i_1_n_7 ;
  wire \output_address_tmp_reg[7]_i_1_n_8 ;
  wire \output_address_tmp_reg[7]_i_1_n_9 ;
  wire [15:0]output_data;
  wire [15:0]output_data_tmp0;
  wire output_data_tmp0_carry__0_i_1_n_0;
  wire output_data_tmp0_carry__0_i_2_n_0;
  wire output_data_tmp0_carry__0_i_3_n_0;
  wire output_data_tmp0_carry__0_i_4_n_0;
  wire output_data_tmp0_carry__0_i_5_n_0;
  wire output_data_tmp0_carry__0_i_6_n_0;
  wire output_data_tmp0_carry__0_i_7_n_0;
  wire output_data_tmp0_carry__0_n_2;
  wire output_data_tmp0_carry__0_n_3;
  wire output_data_tmp0_carry__0_n_4;
  wire output_data_tmp0_carry__0_n_5;
  wire output_data_tmp0_carry__0_n_6;
  wire output_data_tmp0_carry__0_n_7;
  wire output_data_tmp0_carry_i_1_n_0;
  wire output_data_tmp0_carry_i_2_n_0;
  wire output_data_tmp0_carry_i_3_n_0;
  wire output_data_tmp0_carry_i_4_n_0;
  wire output_data_tmp0_carry_i_5_n_0;
  wire output_data_tmp0_carry_i_6_n_0;
  wire output_data_tmp0_carry_i_7_n_0;
  wire output_data_tmp0_carry_i_8_n_0;
  wire output_data_tmp0_carry_n_0;
  wire output_data_tmp0_carry_n_1;
  wire output_data_tmp0_carry_n_2;
  wire output_data_tmp0_carry_n_3;
  wire output_data_tmp0_carry_n_4;
  wire output_data_tmp0_carry_n_5;
  wire output_data_tmp0_carry_n_6;
  wire output_data_tmp0_carry_n_7;
  wire p_0_in;
  wire [2:0]state;
  wire [2:0]state__0;
  wire trigger_axi_master_start;
  wire trigger_axi_master_start_i_10_n_0;
  wire trigger_axi_master_start_i_1_n_0;
  wire trigger_axi_master_start_i_2_n_0;
  wire trigger_axi_master_start_i_3_n_0;
  wire trigger_axi_master_start_i_4_n_0;
  wire trigger_axi_master_start_i_5_n_0;
  wire trigger_axi_master_start_i_6_n_0;
  wire trigger_axi_master_start_i_7_n_0;
  wire trigger_axi_master_start_i_8_n_0;
  wire trigger_axi_master_start_i_9_n_0;
  wire [7:0]NLW_ctr1__14_carry_O_UNCONNECTED;
  wire [7:5]NLW_ctr1__14_carry__0_CO_UNCONNECTED;
  wire [7:0]NLW_ctr1__14_carry__0_O_UNCONNECTED;
  wire [7:0]NLW_ctr1_carry_O_UNCONNECTED;
  wire [7:7]NLW_ctr1_carry__0_CO_UNCONNECTED;
  wire [7:0]NLW_ctr1_carry__0_O_UNCONNECTED;
  wire [7:6]\NLW_ctr_reg[31]_i_3_CO_UNCONNECTED ;
  wire [7:7]\NLW_ctr_reg[31]_i_3_O_UNCONNECTED ;
  wire [7:7]\NLW_output_address_tmp_reg[31]_i_2_CO_UNCONNECTED ;
  wire [7:6]NLW_output_data_tmp0_carry__0_CO_UNCONNECTED;
  wire [7:7]NLW_output_data_tmp0_carry__0_O_UNCONNECTED;

  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT4 #(
    .INIT(16'h0575)) 
    \FSM_sequential_state[0]_i_1 
       (.I0(state[0]),
        .I1(trigger_axi_master_start_i_2_n_0),
        .I2(state[1]),
        .I3(state[2]),
        .O(state__0[0]));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT4 #(
    .INIT(16'h070C)) 
    \FSM_sequential_state[1]_i_1 
       (.I0(trigger_axi_master_start_i_2_n_0),
        .I1(state[0]),
        .I2(state[2]),
        .I3(state[1]),
        .O(state__0[1]));
  LUT6 #(
    .INIT(64'h00FF00FF0350035F)) 
    \FSM_sequential_state[2]_i_1 
       (.I0(axi_master_done),
        .I1(ctr1_carry__0_n_1),
        .I2(state[1]),
        .I3(state[2]),
        .I4(p_0_in),
        .I5(state[0]),
        .O(\FSM_sequential_state[2]_i_1_n_0 ));
  (* FSM_ENCODED_STATES = "INIT:000,TRIGGER:001,WAIT_FOR_START:010,WAIT_FINISH:011,SLEEP:100" *) 
  FDRE #(
    .INIT(1'b0)) 
    \FSM_sequential_state_reg[0] 
       (.C(clk),
        .CE(\FSM_sequential_state[2]_i_1_n_0 ),
        .D(state__0[0]),
        .Q(state[0]),
        .R(1'b0));
  (* FSM_ENCODED_STATES = "INIT:000,TRIGGER:001,WAIT_FOR_START:010,WAIT_FINISH:011,SLEEP:100" *) 
  FDRE #(
    .INIT(1'b0)) 
    \FSM_sequential_state_reg[1] 
       (.C(clk),
        .CE(\FSM_sequential_state[2]_i_1_n_0 ),
        .D(state__0[1]),
        .Q(state[1]),
        .R(1'b0));
  (* FSM_ENCODED_STATES = "INIT:000,TRIGGER:001,WAIT_FOR_START:010,WAIT_FINISH:011,SLEEP:100" *) 
  FDRE #(
    .INIT(1'b0)) 
    \FSM_sequential_state_reg[2] 
       (.C(clk),
        .CE(\FSM_sequential_state[2]_i_1_n_0 ),
        .D(state__0[2]),
        .Q(state[2]),
        .R(1'b0));
  (* COMPARATOR_THRESHOLD = "11" *) 
  CARRY8 ctr1__14_carry
       (.CI(1'b0),
        .CI_TOP(1'b0),
        .CO({ctr1__14_carry_n_0,ctr1__14_carry_n_1,ctr1__14_carry_n_2,ctr1__14_carry_n_3,ctr1__14_carry_n_4,ctr1__14_carry_n_5,ctr1__14_carry_n_6,ctr1__14_carry_n_7}),
        .DI({1'b0,ctr1__14_carry_i_1_n_0,ctr1__14_carry_i_2_n_0,ctr1__14_carry_i_3_n_0,1'b0,1'b0,ctr1__14_carry_i_4_n_0,ctr1__14_carry_i_5_n_0}),
        .O(NLW_ctr1__14_carry_O_UNCONNECTED[7:0]),
        .S({ctr1__14_carry_i_6_n_0,ctr1__14_carry_i_7_n_0,ctr1__14_carry_i_8_n_0,ctr1__14_carry_i_9_n_0,ctr1__14_carry_i_10_n_0,ctr1__14_carry_i_11_n_0,ctr1__14_carry_i_12_n_0,ctr1__14_carry_i_13_n_0}));
  (* COMPARATOR_THRESHOLD = "11" *) 
  CARRY8 ctr1__14_carry__0
       (.CI(ctr1__14_carry_n_0),
        .CI_TOP(1'b0),
        .CO({NLW_ctr1__14_carry__0_CO_UNCONNECTED[7:5],p_0_in,ctr1__14_carry__0_n_4,ctr1__14_carry__0_n_5,ctr1__14_carry__0_n_6,ctr1__14_carry__0_n_7}),
        .DI({1'b0,1'b0,1'b0,\ctr_reg_n_0_[31] ,1'b0,1'b0,1'b0,1'b0}),
        .O(NLW_ctr1__14_carry__0_O_UNCONNECTED[7:0]),
        .S({1'b0,1'b0,1'b0,ctr1__14_carry__0_i_1_n_0,ctr1__14_carry__0_i_2_n_0,ctr1__14_carry__0_i_3_n_0,ctr1__14_carry__0_i_4_n_0,ctr1__14_carry__0_i_5_n_0}));
  LUT2 #(
    .INIT(4'h1)) 
    ctr1__14_carry__0_i_1
       (.I0(\ctr_reg_n_0_[31] ),
        .I1(\ctr_reg_n_0_[30] ),
        .O(ctr1__14_carry__0_i_1_n_0));
  LUT2 #(
    .INIT(4'h1)) 
    ctr1__14_carry__0_i_2
       (.I0(\ctr_reg_n_0_[29] ),
        .I1(\ctr_reg_n_0_[28] ),
        .O(ctr1__14_carry__0_i_2_n_0));
  LUT2 #(
    .INIT(4'h1)) 
    ctr1__14_carry__0_i_3
       (.I0(\ctr_reg_n_0_[27] ),
        .I1(\ctr_reg_n_0_[26] ),
        .O(ctr1__14_carry__0_i_3_n_0));
  LUT2 #(
    .INIT(4'h1)) 
    ctr1__14_carry__0_i_4
       (.I0(\ctr_reg_n_0_[25] ),
        .I1(\ctr_reg_n_0_[24] ),
        .O(ctr1__14_carry__0_i_4_n_0));
  LUT2 #(
    .INIT(4'h1)) 
    ctr1__14_carry__0_i_5
       (.I0(\ctr_reg_n_0_[22] ),
        .I1(\ctr_reg_n_0_[23] ),
        .O(ctr1__14_carry__0_i_5_n_0));
  LUT2 #(
    .INIT(4'h7)) 
    ctr1__14_carry_i_1
       (.I0(\ctr_reg_n_0_[19] ),
        .I1(\ctr_reg_n_0_[18] ),
        .O(ctr1__14_carry_i_1_n_0));
  LUT2 #(
    .INIT(4'h1)) 
    ctr1__14_carry_i_10
       (.I0(\ctr_reg_n_0_[13] ),
        .I1(\ctr_reg_n_0_[12] ),
        .O(ctr1__14_carry_i_10_n_0));
  LUT2 #(
    .INIT(4'h1)) 
    ctr1__14_carry_i_11
       (.I0(\ctr_reg_n_0_[11] ),
        .I1(\ctr_reg_n_0_[10] ),
        .O(ctr1__14_carry_i_11_n_0));
  LUT2 #(
    .INIT(4'h2)) 
    ctr1__14_carry_i_12
       (.I0(\ctr_reg_n_0_[9] ),
        .I1(\ctr_reg_n_0_[8] ),
        .O(ctr1__14_carry_i_12_n_0));
  LUT2 #(
    .INIT(4'h2)) 
    ctr1__14_carry_i_13
       (.I0(\ctr_reg_n_0_[6] ),
        .I1(\ctr_reg_n_0_[7] ),
        .O(ctr1__14_carry_i_13_n_0));
  LUT2 #(
    .INIT(4'h7)) 
    ctr1__14_carry_i_2
       (.I0(\ctr_reg_n_0_[17] ),
        .I1(\ctr_reg_n_0_[16] ),
        .O(ctr1__14_carry_i_2_n_0));
  LUT2 #(
    .INIT(4'h1)) 
    ctr1__14_carry_i_3
       (.I0(\ctr_reg_n_0_[15] ),
        .I1(\ctr_reg_n_0_[14] ),
        .O(ctr1__14_carry_i_3_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    ctr1__14_carry_i_4
       (.I0(\ctr_reg_n_0_[9] ),
        .O(ctr1__14_carry_i_4_n_0));
  LUT2 #(
    .INIT(4'h1)) 
    ctr1__14_carry_i_5
       (.I0(\ctr_reg_n_0_[7] ),
        .I1(\ctr_reg_n_0_[6] ),
        .O(ctr1__14_carry_i_5_n_0));
  LUT2 #(
    .INIT(4'h1)) 
    ctr1__14_carry_i_6
       (.I0(\ctr_reg_n_0_[21] ),
        .I1(\ctr_reg_n_0_[20] ),
        .O(ctr1__14_carry_i_6_n_0));
  LUT2 #(
    .INIT(4'h8)) 
    ctr1__14_carry_i_7
       (.I0(\ctr_reg_n_0_[18] ),
        .I1(\ctr_reg_n_0_[19] ),
        .O(ctr1__14_carry_i_7_n_0));
  LUT2 #(
    .INIT(4'h8)) 
    ctr1__14_carry_i_8
       (.I0(\ctr_reg_n_0_[16] ),
        .I1(\ctr_reg_n_0_[17] ),
        .O(ctr1__14_carry_i_8_n_0));
  LUT2 #(
    .INIT(4'h2)) 
    ctr1__14_carry_i_9
       (.I0(\ctr_reg_n_0_[14] ),
        .I1(\ctr_reg_n_0_[15] ),
        .O(ctr1__14_carry_i_9_n_0));
  (* COMPARATOR_THRESHOLD = "11" *) 
  CARRY8 ctr1_carry
       (.CI(1'b0),
        .CI_TOP(1'b0),
        .CO({ctr1_carry_n_0,ctr1_carry_n_1,ctr1_carry_n_2,ctr1_carry_n_3,ctr1_carry_n_4,ctr1_carry_n_5,ctr1_carry_n_6,ctr1_carry_n_7}),
        .DI({1'b0,1'b0,1'b0,1'b0,ctr1_carry_i_1_n_0,ctr1_carry_i_2_n_0,ctr1_carry_i_3_n_0,ctr1_carry_i_4_n_0}),
        .O(NLW_ctr1_carry_O_UNCONNECTED[7:0]),
        .S({ctr1_carry_i_5_n_0,ctr1_carry_i_6_n_0,ctr1_carry_i_7_n_0,ctr1_carry_i_8_n_0,ctr1_carry_i_9_n_0,ctr1_carry_i_10_n_0,ctr1_carry_i_11_n_0,ctr1_carry_i_12_n_0}));
  (* COMPARATOR_THRESHOLD = "11" *) 
  CARRY8 ctr1_carry__0
       (.CI(ctr1_carry_n_0),
        .CI_TOP(1'b0),
        .CO({NLW_ctr1_carry__0_CO_UNCONNECTED[7],ctr1_carry__0_n_1,ctr1_carry__0_n_2,ctr1_carry__0_n_3,ctr1_carry__0_n_4,ctr1_carry__0_n_5,ctr1_carry__0_n_6,ctr1_carry__0_n_7}),
        .DI({1'b0,\ctr_reg_n_0_[31] ,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .O(NLW_ctr1_carry__0_O_UNCONNECTED[7:0]),
        .S({1'b0,ctr1_carry__0_i_1_n_0,ctr1_carry__0_i_2_n_0,ctr1_carry__0_i_3_n_0,ctr1_carry__0_i_4_n_0,ctr1_carry__0_i_5_n_0,ctr1_carry__0_i_6_n_0,ctr1_carry__0_i_7_n_0}));
  LUT2 #(
    .INIT(4'h1)) 
    ctr1_carry__0_i_1
       (.I0(\ctr_reg_n_0_[31] ),
        .I1(\ctr_reg_n_0_[30] ),
        .O(ctr1_carry__0_i_1_n_0));
  LUT2 #(
    .INIT(4'h1)) 
    ctr1_carry__0_i_2
       (.I0(\ctr_reg_n_0_[29] ),
        .I1(\ctr_reg_n_0_[28] ),
        .O(ctr1_carry__0_i_2_n_0));
  LUT2 #(
    .INIT(4'h1)) 
    ctr1_carry__0_i_3
       (.I0(\ctr_reg_n_0_[27] ),
        .I1(\ctr_reg_n_0_[26] ),
        .O(ctr1_carry__0_i_3_n_0));
  LUT2 #(
    .INIT(4'h1)) 
    ctr1_carry__0_i_4
       (.I0(\ctr_reg_n_0_[25] ),
        .I1(\ctr_reg_n_0_[24] ),
        .O(ctr1_carry__0_i_4_n_0));
  LUT2 #(
    .INIT(4'h1)) 
    ctr1_carry__0_i_5
       (.I0(\ctr_reg_n_0_[22] ),
        .I1(\ctr_reg_n_0_[23] ),
        .O(ctr1_carry__0_i_5_n_0));
  LUT2 #(
    .INIT(4'h1)) 
    ctr1_carry__0_i_6
       (.I0(\ctr_reg_n_0_[21] ),
        .I1(\ctr_reg_n_0_[20] ),
        .O(ctr1_carry__0_i_6_n_0));
  LUT2 #(
    .INIT(4'h1)) 
    ctr1_carry__0_i_7
       (.I0(\ctr_reg_n_0_[19] ),
        .I1(\ctr_reg_n_0_[18] ),
        .O(ctr1_carry__0_i_7_n_0));
  LUT2 #(
    .INIT(4'h7)) 
    ctr1_carry_i_1
       (.I0(\ctr_reg_n_0_[9] ),
        .I1(\ctr_reg_n_0_[8] ),
        .O(ctr1_carry_i_1_n_0));
  LUT2 #(
    .INIT(4'h8)) 
    ctr1_carry_i_10
       (.I0(\ctr_reg_n_0_[6] ),
        .I1(\ctr_reg_n_0_[7] ),
        .O(ctr1_carry_i_10_n_0));
  LUT2 #(
    .INIT(4'h2)) 
    ctr1_carry_i_11
       (.I0(\ctr_reg_n_0_[5] ),
        .I1(\ctr_reg_n_0_[4] ),
        .O(ctr1_carry_i_11_n_0));
  LUT2 #(
    .INIT(4'h2)) 
    ctr1_carry_i_12
       (.I0(\ctr_reg_n_0_[3] ),
        .I1(\ctr_reg_n_0_[2] ),
        .O(ctr1_carry_i_12_n_0));
  LUT2 #(
    .INIT(4'h7)) 
    ctr1_carry_i_2
       (.I0(\ctr_reg_n_0_[7] ),
        .I1(\ctr_reg_n_0_[6] ),
        .O(ctr1_carry_i_2_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    ctr1_carry_i_3
       (.I0(\ctr_reg_n_0_[5] ),
        .O(ctr1_carry_i_3_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    ctr1_carry_i_4
       (.I0(\ctr_reg_n_0_[3] ),
        .O(ctr1_carry_i_4_n_0));
  LUT2 #(
    .INIT(4'h1)) 
    ctr1_carry_i_5
       (.I0(\ctr_reg_n_0_[17] ),
        .I1(\ctr_reg_n_0_[16] ),
        .O(ctr1_carry_i_5_n_0));
  LUT2 #(
    .INIT(4'h1)) 
    ctr1_carry_i_6
       (.I0(\ctr_reg_n_0_[15] ),
        .I1(\ctr_reg_n_0_[14] ),
        .O(ctr1_carry_i_6_n_0));
  LUT2 #(
    .INIT(4'h1)) 
    ctr1_carry_i_7
       (.I0(\ctr_reg_n_0_[13] ),
        .I1(\ctr_reg_n_0_[12] ),
        .O(ctr1_carry_i_7_n_0));
  LUT2 #(
    .INIT(4'h1)) 
    ctr1_carry_i_8
       (.I0(\ctr_reg_n_0_[11] ),
        .I1(\ctr_reg_n_0_[10] ),
        .O(ctr1_carry_i_8_n_0));
  LUT2 #(
    .INIT(4'h8)) 
    ctr1_carry_i_9
       (.I0(\ctr_reg_n_0_[8] ),
        .I1(\ctr_reg_n_0_[9] ),
        .O(ctr1_carry_i_9_n_0));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT1 #(
    .INIT(2'h1)) 
    \ctr[0]_i_1 
       (.I0(\ctr_reg_n_0_[0] ),
        .O(\ctr[0]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0A0A101500001015)) 
    \ctr[31]_i_1 
       (.I0(state[0]),
        .I1(ctr1_carry__0_n_1),
        .I2(state[2]),
        .I3(p_0_in),
        .I4(state[1]),
        .I5(trigger_axi_master_start_i_2_n_0),
        .O(\ctr[31]_i_1_n_0 ));
  LUT3 #(
    .INIT(8'h43)) 
    \ctr[31]_i_2 
       (.I0(state[2]),
        .I1(state[0]),
        .I2(state[1]),
        .O(\ctr[31]_i_2_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \ctr_reg[0] 
       (.C(clk),
        .CE(\ctr[31]_i_2_n_0 ),
        .D(\ctr[0]_i_1_n_0 ),
        .Q(\ctr_reg_n_0_[0] ),
        .R(\ctr[31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \ctr_reg[10] 
       (.C(clk),
        .CE(\ctr[31]_i_2_n_0 ),
        .D(\ctr_reg[16]_i_1_n_14 ),
        .Q(\ctr_reg_n_0_[10] ),
        .R(\ctr[31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \ctr_reg[11] 
       (.C(clk),
        .CE(\ctr[31]_i_2_n_0 ),
        .D(\ctr_reg[16]_i_1_n_13 ),
        .Q(\ctr_reg_n_0_[11] ),
        .R(\ctr[31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \ctr_reg[12] 
       (.C(clk),
        .CE(\ctr[31]_i_2_n_0 ),
        .D(\ctr_reg[16]_i_1_n_12 ),
        .Q(\ctr_reg_n_0_[12] ),
        .R(\ctr[31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \ctr_reg[13] 
       (.C(clk),
        .CE(\ctr[31]_i_2_n_0 ),
        .D(\ctr_reg[16]_i_1_n_11 ),
        .Q(\ctr_reg_n_0_[13] ),
        .R(\ctr[31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \ctr_reg[14] 
       (.C(clk),
        .CE(\ctr[31]_i_2_n_0 ),
        .D(\ctr_reg[16]_i_1_n_10 ),
        .Q(\ctr_reg_n_0_[14] ),
        .R(\ctr[31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \ctr_reg[15] 
       (.C(clk),
        .CE(\ctr[31]_i_2_n_0 ),
        .D(\ctr_reg[16]_i_1_n_9 ),
        .Q(\ctr_reg_n_0_[15] ),
        .R(\ctr[31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \ctr_reg[16] 
       (.C(clk),
        .CE(\ctr[31]_i_2_n_0 ),
        .D(\ctr_reg[16]_i_1_n_8 ),
        .Q(\ctr_reg_n_0_[16] ),
        .R(\ctr[31]_i_1_n_0 ));
  (* ADDER_THRESHOLD = "35" *) 
  CARRY8 \ctr_reg[16]_i_1 
       (.CI(\ctr_reg[8]_i_1_n_0 ),
        .CI_TOP(1'b0),
        .CO({\ctr_reg[16]_i_1_n_0 ,\ctr_reg[16]_i_1_n_1 ,\ctr_reg[16]_i_1_n_2 ,\ctr_reg[16]_i_1_n_3 ,\ctr_reg[16]_i_1_n_4 ,\ctr_reg[16]_i_1_n_5 ,\ctr_reg[16]_i_1_n_6 ,\ctr_reg[16]_i_1_n_7 }),
        .DI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .O({\ctr_reg[16]_i_1_n_8 ,\ctr_reg[16]_i_1_n_9 ,\ctr_reg[16]_i_1_n_10 ,\ctr_reg[16]_i_1_n_11 ,\ctr_reg[16]_i_1_n_12 ,\ctr_reg[16]_i_1_n_13 ,\ctr_reg[16]_i_1_n_14 ,\ctr_reg[16]_i_1_n_15 }),
        .S({\ctr_reg_n_0_[16] ,\ctr_reg_n_0_[15] ,\ctr_reg_n_0_[14] ,\ctr_reg_n_0_[13] ,\ctr_reg_n_0_[12] ,\ctr_reg_n_0_[11] ,\ctr_reg_n_0_[10] ,\ctr_reg_n_0_[9] }));
  FDRE #(
    .INIT(1'b0)) 
    \ctr_reg[17] 
       (.C(clk),
        .CE(\ctr[31]_i_2_n_0 ),
        .D(\ctr_reg[24]_i_1_n_15 ),
        .Q(\ctr_reg_n_0_[17] ),
        .R(\ctr[31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \ctr_reg[18] 
       (.C(clk),
        .CE(\ctr[31]_i_2_n_0 ),
        .D(\ctr_reg[24]_i_1_n_14 ),
        .Q(\ctr_reg_n_0_[18] ),
        .R(\ctr[31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \ctr_reg[19] 
       (.C(clk),
        .CE(\ctr[31]_i_2_n_0 ),
        .D(\ctr_reg[24]_i_1_n_13 ),
        .Q(\ctr_reg_n_0_[19] ),
        .R(\ctr[31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \ctr_reg[1] 
       (.C(clk),
        .CE(\ctr[31]_i_2_n_0 ),
        .D(\ctr_reg[8]_i_1_n_15 ),
        .Q(\ctr_reg_n_0_[1] ),
        .R(\ctr[31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \ctr_reg[20] 
       (.C(clk),
        .CE(\ctr[31]_i_2_n_0 ),
        .D(\ctr_reg[24]_i_1_n_12 ),
        .Q(\ctr_reg_n_0_[20] ),
        .R(\ctr[31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \ctr_reg[21] 
       (.C(clk),
        .CE(\ctr[31]_i_2_n_0 ),
        .D(\ctr_reg[24]_i_1_n_11 ),
        .Q(\ctr_reg_n_0_[21] ),
        .R(\ctr[31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \ctr_reg[22] 
       (.C(clk),
        .CE(\ctr[31]_i_2_n_0 ),
        .D(\ctr_reg[24]_i_1_n_10 ),
        .Q(\ctr_reg_n_0_[22] ),
        .R(\ctr[31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \ctr_reg[23] 
       (.C(clk),
        .CE(\ctr[31]_i_2_n_0 ),
        .D(\ctr_reg[24]_i_1_n_9 ),
        .Q(\ctr_reg_n_0_[23] ),
        .R(\ctr[31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \ctr_reg[24] 
       (.C(clk),
        .CE(\ctr[31]_i_2_n_0 ),
        .D(\ctr_reg[24]_i_1_n_8 ),
        .Q(\ctr_reg_n_0_[24] ),
        .R(\ctr[31]_i_1_n_0 ));
  (* ADDER_THRESHOLD = "35" *) 
  CARRY8 \ctr_reg[24]_i_1 
       (.CI(\ctr_reg[16]_i_1_n_0 ),
        .CI_TOP(1'b0),
        .CO({\ctr_reg[24]_i_1_n_0 ,\ctr_reg[24]_i_1_n_1 ,\ctr_reg[24]_i_1_n_2 ,\ctr_reg[24]_i_1_n_3 ,\ctr_reg[24]_i_1_n_4 ,\ctr_reg[24]_i_1_n_5 ,\ctr_reg[24]_i_1_n_6 ,\ctr_reg[24]_i_1_n_7 }),
        .DI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .O({\ctr_reg[24]_i_1_n_8 ,\ctr_reg[24]_i_1_n_9 ,\ctr_reg[24]_i_1_n_10 ,\ctr_reg[24]_i_1_n_11 ,\ctr_reg[24]_i_1_n_12 ,\ctr_reg[24]_i_1_n_13 ,\ctr_reg[24]_i_1_n_14 ,\ctr_reg[24]_i_1_n_15 }),
        .S({\ctr_reg_n_0_[24] ,\ctr_reg_n_0_[23] ,\ctr_reg_n_0_[22] ,\ctr_reg_n_0_[21] ,\ctr_reg_n_0_[20] ,\ctr_reg_n_0_[19] ,\ctr_reg_n_0_[18] ,\ctr_reg_n_0_[17] }));
  FDRE #(
    .INIT(1'b0)) 
    \ctr_reg[25] 
       (.C(clk),
        .CE(\ctr[31]_i_2_n_0 ),
        .D(\ctr_reg[31]_i_3_n_15 ),
        .Q(\ctr_reg_n_0_[25] ),
        .R(\ctr[31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \ctr_reg[26] 
       (.C(clk),
        .CE(\ctr[31]_i_2_n_0 ),
        .D(\ctr_reg[31]_i_3_n_14 ),
        .Q(\ctr_reg_n_0_[26] ),
        .R(\ctr[31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \ctr_reg[27] 
       (.C(clk),
        .CE(\ctr[31]_i_2_n_0 ),
        .D(\ctr_reg[31]_i_3_n_13 ),
        .Q(\ctr_reg_n_0_[27] ),
        .R(\ctr[31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \ctr_reg[28] 
       (.C(clk),
        .CE(\ctr[31]_i_2_n_0 ),
        .D(\ctr_reg[31]_i_3_n_12 ),
        .Q(\ctr_reg_n_0_[28] ),
        .R(\ctr[31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \ctr_reg[29] 
       (.C(clk),
        .CE(\ctr[31]_i_2_n_0 ),
        .D(\ctr_reg[31]_i_3_n_11 ),
        .Q(\ctr_reg_n_0_[29] ),
        .R(\ctr[31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \ctr_reg[2] 
       (.C(clk),
        .CE(\ctr[31]_i_2_n_0 ),
        .D(\ctr_reg[8]_i_1_n_14 ),
        .Q(\ctr_reg_n_0_[2] ),
        .R(\ctr[31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \ctr_reg[30] 
       (.C(clk),
        .CE(\ctr[31]_i_2_n_0 ),
        .D(\ctr_reg[31]_i_3_n_10 ),
        .Q(\ctr_reg_n_0_[30] ),
        .R(\ctr[31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \ctr_reg[31] 
       (.C(clk),
        .CE(\ctr[31]_i_2_n_0 ),
        .D(\ctr_reg[31]_i_3_n_9 ),
        .Q(\ctr_reg_n_0_[31] ),
        .R(\ctr[31]_i_1_n_0 ));
  (* ADDER_THRESHOLD = "35" *) 
  CARRY8 \ctr_reg[31]_i_3 
       (.CI(\ctr_reg[24]_i_1_n_0 ),
        .CI_TOP(1'b0),
        .CO({\NLW_ctr_reg[31]_i_3_CO_UNCONNECTED [7:6],\ctr_reg[31]_i_3_n_2 ,\ctr_reg[31]_i_3_n_3 ,\ctr_reg[31]_i_3_n_4 ,\ctr_reg[31]_i_3_n_5 ,\ctr_reg[31]_i_3_n_6 ,\ctr_reg[31]_i_3_n_7 }),
        .DI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .O({\NLW_ctr_reg[31]_i_3_O_UNCONNECTED [7],\ctr_reg[31]_i_3_n_9 ,\ctr_reg[31]_i_3_n_10 ,\ctr_reg[31]_i_3_n_11 ,\ctr_reg[31]_i_3_n_12 ,\ctr_reg[31]_i_3_n_13 ,\ctr_reg[31]_i_3_n_14 ,\ctr_reg[31]_i_3_n_15 }),
        .S({1'b0,\ctr_reg_n_0_[31] ,\ctr_reg_n_0_[30] ,\ctr_reg_n_0_[29] ,\ctr_reg_n_0_[28] ,\ctr_reg_n_0_[27] ,\ctr_reg_n_0_[26] ,\ctr_reg_n_0_[25] }));
  FDRE #(
    .INIT(1'b0)) 
    \ctr_reg[3] 
       (.C(clk),
        .CE(\ctr[31]_i_2_n_0 ),
        .D(\ctr_reg[8]_i_1_n_13 ),
        .Q(\ctr_reg_n_0_[3] ),
        .R(\ctr[31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \ctr_reg[4] 
       (.C(clk),
        .CE(\ctr[31]_i_2_n_0 ),
        .D(\ctr_reg[8]_i_1_n_12 ),
        .Q(\ctr_reg_n_0_[4] ),
        .R(\ctr[31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \ctr_reg[5] 
       (.C(clk),
        .CE(\ctr[31]_i_2_n_0 ),
        .D(\ctr_reg[8]_i_1_n_11 ),
        .Q(\ctr_reg_n_0_[5] ),
        .R(\ctr[31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \ctr_reg[6] 
       (.C(clk),
        .CE(\ctr[31]_i_2_n_0 ),
        .D(\ctr_reg[8]_i_1_n_10 ),
        .Q(\ctr_reg_n_0_[6] ),
        .R(\ctr[31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \ctr_reg[7] 
       (.C(clk),
        .CE(\ctr[31]_i_2_n_0 ),
        .D(\ctr_reg[8]_i_1_n_9 ),
        .Q(\ctr_reg_n_0_[7] ),
        .R(\ctr[31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \ctr_reg[8] 
       (.C(clk),
        .CE(\ctr[31]_i_2_n_0 ),
        .D(\ctr_reg[8]_i_1_n_8 ),
        .Q(\ctr_reg_n_0_[8] ),
        .R(\ctr[31]_i_1_n_0 ));
  (* ADDER_THRESHOLD = "35" *) 
  CARRY8 \ctr_reg[8]_i_1 
       (.CI(\ctr_reg_n_0_[0] ),
        .CI_TOP(1'b0),
        .CO({\ctr_reg[8]_i_1_n_0 ,\ctr_reg[8]_i_1_n_1 ,\ctr_reg[8]_i_1_n_2 ,\ctr_reg[8]_i_1_n_3 ,\ctr_reg[8]_i_1_n_4 ,\ctr_reg[8]_i_1_n_5 ,\ctr_reg[8]_i_1_n_6 ,\ctr_reg[8]_i_1_n_7 }),
        .DI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .O({\ctr_reg[8]_i_1_n_8 ,\ctr_reg[8]_i_1_n_9 ,\ctr_reg[8]_i_1_n_10 ,\ctr_reg[8]_i_1_n_11 ,\ctr_reg[8]_i_1_n_12 ,\ctr_reg[8]_i_1_n_13 ,\ctr_reg[8]_i_1_n_14 ,\ctr_reg[8]_i_1_n_15 }),
        .S({\ctr_reg_n_0_[8] ,\ctr_reg_n_0_[7] ,\ctr_reg_n_0_[6] ,\ctr_reg_n_0_[5] ,\ctr_reg_n_0_[4] ,\ctr_reg_n_0_[3] ,\ctr_reg_n_0_[2] ,\ctr_reg_n_0_[1] }));
  FDRE #(
    .INIT(1'b0)) 
    \ctr_reg[9] 
       (.C(clk),
        .CE(\ctr[31]_i_2_n_0 ),
        .D(\ctr_reg[16]_i_1_n_15 ),
        .Q(\ctr_reg_n_0_[9] ),
        .R(\ctr[31]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'h4000)) 
    \output_address_tmp[31]_i_1 
       (.I0(state[2]),
        .I1(state[1]),
        .I2(state[0]),
        .I3(trigger_axi_master_start_i_2_n_0),
        .O(state__0[2]));
  LUT1 #(
    .INIT(2'h1)) 
    \output_address_tmp[7]_i_2 
       (.I0(output_address[0]),
        .O(\output_address_tmp[7]_i_2_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \output_address_tmp_reg[0] 
       (.C(clk),
        .CE(state__0[2]),
        .D(\output_address_tmp_reg[7]_i_1_n_15 ),
        .Q(output_address[0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \output_address_tmp_reg[10] 
       (.C(clk),
        .CE(state__0[2]),
        .D(\output_address_tmp_reg[15]_i_1_n_13 ),
        .Q(output_address[10]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \output_address_tmp_reg[11] 
       (.C(clk),
        .CE(state__0[2]),
        .D(\output_address_tmp_reg[15]_i_1_n_12 ),
        .Q(output_address[11]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \output_address_tmp_reg[12] 
       (.C(clk),
        .CE(state__0[2]),
        .D(\output_address_tmp_reg[15]_i_1_n_11 ),
        .Q(output_address[12]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \output_address_tmp_reg[13] 
       (.C(clk),
        .CE(state__0[2]),
        .D(\output_address_tmp_reg[15]_i_1_n_10 ),
        .Q(output_address[13]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \output_address_tmp_reg[14] 
       (.C(clk),
        .CE(state__0[2]),
        .D(\output_address_tmp_reg[15]_i_1_n_9 ),
        .Q(output_address[14]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \output_address_tmp_reg[15] 
       (.C(clk),
        .CE(state__0[2]),
        .D(\output_address_tmp_reg[15]_i_1_n_8 ),
        .Q(output_address[15]),
        .R(1'b0));
  (* ADDER_THRESHOLD = "16" *) 
  CARRY8 \output_address_tmp_reg[15]_i_1 
       (.CI(\output_address_tmp_reg[7]_i_1_n_0 ),
        .CI_TOP(1'b0),
        .CO({\output_address_tmp_reg[15]_i_1_n_0 ,\output_address_tmp_reg[15]_i_1_n_1 ,\output_address_tmp_reg[15]_i_1_n_2 ,\output_address_tmp_reg[15]_i_1_n_3 ,\output_address_tmp_reg[15]_i_1_n_4 ,\output_address_tmp_reg[15]_i_1_n_5 ,\output_address_tmp_reg[15]_i_1_n_6 ,\output_address_tmp_reg[15]_i_1_n_7 }),
        .DI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .O({\output_address_tmp_reg[15]_i_1_n_8 ,\output_address_tmp_reg[15]_i_1_n_9 ,\output_address_tmp_reg[15]_i_1_n_10 ,\output_address_tmp_reg[15]_i_1_n_11 ,\output_address_tmp_reg[15]_i_1_n_12 ,\output_address_tmp_reg[15]_i_1_n_13 ,\output_address_tmp_reg[15]_i_1_n_14 ,\output_address_tmp_reg[15]_i_1_n_15 }),
        .S(output_address[15:8]));
  FDRE #(
    .INIT(1'b0)) 
    \output_address_tmp_reg[16] 
       (.C(clk),
        .CE(state__0[2]),
        .D(\output_address_tmp_reg[23]_i_1_n_15 ),
        .Q(output_address[16]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \output_address_tmp_reg[17] 
       (.C(clk),
        .CE(state__0[2]),
        .D(\output_address_tmp_reg[23]_i_1_n_14 ),
        .Q(output_address[17]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \output_address_tmp_reg[18] 
       (.C(clk),
        .CE(state__0[2]),
        .D(\output_address_tmp_reg[23]_i_1_n_13 ),
        .Q(output_address[18]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \output_address_tmp_reg[19] 
       (.C(clk),
        .CE(state__0[2]),
        .D(\output_address_tmp_reg[23]_i_1_n_12 ),
        .Q(output_address[19]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \output_address_tmp_reg[1] 
       (.C(clk),
        .CE(state__0[2]),
        .D(\output_address_tmp_reg[7]_i_1_n_14 ),
        .Q(output_address[1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \output_address_tmp_reg[20] 
       (.C(clk),
        .CE(state__0[2]),
        .D(\output_address_tmp_reg[23]_i_1_n_11 ),
        .Q(output_address[20]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \output_address_tmp_reg[21] 
       (.C(clk),
        .CE(state__0[2]),
        .D(\output_address_tmp_reg[23]_i_1_n_10 ),
        .Q(output_address[21]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \output_address_tmp_reg[22] 
       (.C(clk),
        .CE(state__0[2]),
        .D(\output_address_tmp_reg[23]_i_1_n_9 ),
        .Q(output_address[22]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \output_address_tmp_reg[23] 
       (.C(clk),
        .CE(state__0[2]),
        .D(\output_address_tmp_reg[23]_i_1_n_8 ),
        .Q(output_address[23]),
        .R(1'b0));
  (* ADDER_THRESHOLD = "16" *) 
  CARRY8 \output_address_tmp_reg[23]_i_1 
       (.CI(\output_address_tmp_reg[15]_i_1_n_0 ),
        .CI_TOP(1'b0),
        .CO({\output_address_tmp_reg[23]_i_1_n_0 ,\output_address_tmp_reg[23]_i_1_n_1 ,\output_address_tmp_reg[23]_i_1_n_2 ,\output_address_tmp_reg[23]_i_1_n_3 ,\output_address_tmp_reg[23]_i_1_n_4 ,\output_address_tmp_reg[23]_i_1_n_5 ,\output_address_tmp_reg[23]_i_1_n_6 ,\output_address_tmp_reg[23]_i_1_n_7 }),
        .DI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .O({\output_address_tmp_reg[23]_i_1_n_8 ,\output_address_tmp_reg[23]_i_1_n_9 ,\output_address_tmp_reg[23]_i_1_n_10 ,\output_address_tmp_reg[23]_i_1_n_11 ,\output_address_tmp_reg[23]_i_1_n_12 ,\output_address_tmp_reg[23]_i_1_n_13 ,\output_address_tmp_reg[23]_i_1_n_14 ,\output_address_tmp_reg[23]_i_1_n_15 }),
        .S(output_address[23:16]));
  FDRE #(
    .INIT(1'b0)) 
    \output_address_tmp_reg[24] 
       (.C(clk),
        .CE(state__0[2]),
        .D(\output_address_tmp_reg[31]_i_2_n_15 ),
        .Q(output_address[24]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \output_address_tmp_reg[25] 
       (.C(clk),
        .CE(state__0[2]),
        .D(\output_address_tmp_reg[31]_i_2_n_14 ),
        .Q(output_address[25]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \output_address_tmp_reg[26] 
       (.C(clk),
        .CE(state__0[2]),
        .D(\output_address_tmp_reg[31]_i_2_n_13 ),
        .Q(output_address[26]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \output_address_tmp_reg[27] 
       (.C(clk),
        .CE(state__0[2]),
        .D(\output_address_tmp_reg[31]_i_2_n_12 ),
        .Q(output_address[27]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \output_address_tmp_reg[28] 
       (.C(clk),
        .CE(state__0[2]),
        .D(\output_address_tmp_reg[31]_i_2_n_11 ),
        .Q(output_address[28]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \output_address_tmp_reg[29] 
       (.C(clk),
        .CE(state__0[2]),
        .D(\output_address_tmp_reg[31]_i_2_n_10 ),
        .Q(output_address[29]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \output_address_tmp_reg[2] 
       (.C(clk),
        .CE(state__0[2]),
        .D(\output_address_tmp_reg[7]_i_1_n_13 ),
        .Q(output_address[2]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \output_address_tmp_reg[30] 
       (.C(clk),
        .CE(state__0[2]),
        .D(\output_address_tmp_reg[31]_i_2_n_9 ),
        .Q(output_address[30]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \output_address_tmp_reg[31] 
       (.C(clk),
        .CE(state__0[2]),
        .D(\output_address_tmp_reg[31]_i_2_n_8 ),
        .Q(output_address[31]),
        .R(1'b0));
  (* ADDER_THRESHOLD = "16" *) 
  CARRY8 \output_address_tmp_reg[31]_i_2 
       (.CI(\output_address_tmp_reg[23]_i_1_n_0 ),
        .CI_TOP(1'b0),
        .CO({\NLW_output_address_tmp_reg[31]_i_2_CO_UNCONNECTED [7],\output_address_tmp_reg[31]_i_2_n_1 ,\output_address_tmp_reg[31]_i_2_n_2 ,\output_address_tmp_reg[31]_i_2_n_3 ,\output_address_tmp_reg[31]_i_2_n_4 ,\output_address_tmp_reg[31]_i_2_n_5 ,\output_address_tmp_reg[31]_i_2_n_6 ,\output_address_tmp_reg[31]_i_2_n_7 }),
        .DI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .O({\output_address_tmp_reg[31]_i_2_n_8 ,\output_address_tmp_reg[31]_i_2_n_9 ,\output_address_tmp_reg[31]_i_2_n_10 ,\output_address_tmp_reg[31]_i_2_n_11 ,\output_address_tmp_reg[31]_i_2_n_12 ,\output_address_tmp_reg[31]_i_2_n_13 ,\output_address_tmp_reg[31]_i_2_n_14 ,\output_address_tmp_reg[31]_i_2_n_15 }),
        .S(output_address[31:24]));
  FDRE #(
    .INIT(1'b0)) 
    \output_address_tmp_reg[3] 
       (.C(clk),
        .CE(state__0[2]),
        .D(\output_address_tmp_reg[7]_i_1_n_12 ),
        .Q(output_address[3]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \output_address_tmp_reg[4] 
       (.C(clk),
        .CE(state__0[2]),
        .D(\output_address_tmp_reg[7]_i_1_n_11 ),
        .Q(output_address[4]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \output_address_tmp_reg[5] 
       (.C(clk),
        .CE(state__0[2]),
        .D(\output_address_tmp_reg[7]_i_1_n_10 ),
        .Q(output_address[5]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \output_address_tmp_reg[6] 
       (.C(clk),
        .CE(state__0[2]),
        .D(\output_address_tmp_reg[7]_i_1_n_9 ),
        .Q(output_address[6]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \output_address_tmp_reg[7] 
       (.C(clk),
        .CE(state__0[2]),
        .D(\output_address_tmp_reg[7]_i_1_n_8 ),
        .Q(output_address[7]),
        .R(1'b0));
  (* ADDER_THRESHOLD = "16" *) 
  CARRY8 \output_address_tmp_reg[7]_i_1 
       (.CI(1'b0),
        .CI_TOP(1'b0),
        .CO({\output_address_tmp_reg[7]_i_1_n_0 ,\output_address_tmp_reg[7]_i_1_n_1 ,\output_address_tmp_reg[7]_i_1_n_2 ,\output_address_tmp_reg[7]_i_1_n_3 ,\output_address_tmp_reg[7]_i_1_n_4 ,\output_address_tmp_reg[7]_i_1_n_5 ,\output_address_tmp_reg[7]_i_1_n_6 ,\output_address_tmp_reg[7]_i_1_n_7 }),
        .DI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b1}),
        .O({\output_address_tmp_reg[7]_i_1_n_8 ,\output_address_tmp_reg[7]_i_1_n_9 ,\output_address_tmp_reg[7]_i_1_n_10 ,\output_address_tmp_reg[7]_i_1_n_11 ,\output_address_tmp_reg[7]_i_1_n_12 ,\output_address_tmp_reg[7]_i_1_n_13 ,\output_address_tmp_reg[7]_i_1_n_14 ,\output_address_tmp_reg[7]_i_1_n_15 }),
        .S({output_address[7:1],\output_address_tmp[7]_i_2_n_0 }));
  FDRE #(
    .INIT(1'b0)) 
    \output_address_tmp_reg[8] 
       (.C(clk),
        .CE(state__0[2]),
        .D(\output_address_tmp_reg[15]_i_1_n_15 ),
        .Q(output_address[8]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \output_address_tmp_reg[9] 
       (.C(clk),
        .CE(state__0[2]),
        .D(\output_address_tmp_reg[15]_i_1_n_14 ),
        .Q(output_address[9]),
        .R(1'b0));
  (* ADDER_THRESHOLD = "35" *) 
  CARRY8 output_data_tmp0_carry
       (.CI(output_data[0]),
        .CI_TOP(1'b0),
        .CO({output_data_tmp0_carry_n_0,output_data_tmp0_carry_n_1,output_data_tmp0_carry_n_2,output_data_tmp0_carry_n_3,output_data_tmp0_carry_n_4,output_data_tmp0_carry_n_5,output_data_tmp0_carry_n_6,output_data_tmp0_carry_n_7}),
        .DI(output_data[8:1]),
        .O(output_data_tmp0[8:1]),
        .S({output_data_tmp0_carry_i_1_n_0,output_data_tmp0_carry_i_2_n_0,output_data_tmp0_carry_i_3_n_0,output_data_tmp0_carry_i_4_n_0,output_data_tmp0_carry_i_5_n_0,output_data_tmp0_carry_i_6_n_0,output_data_tmp0_carry_i_7_n_0,output_data_tmp0_carry_i_8_n_0}));
  (* ADDER_THRESHOLD = "35" *) 
  CARRY8 output_data_tmp0_carry__0
       (.CI(output_data_tmp0_carry_n_0),
        .CI_TOP(1'b0),
        .CO({NLW_output_data_tmp0_carry__0_CO_UNCONNECTED[7:6],output_data_tmp0_carry__0_n_2,output_data_tmp0_carry__0_n_3,output_data_tmp0_carry__0_n_4,output_data_tmp0_carry__0_n_5,output_data_tmp0_carry__0_n_6,output_data_tmp0_carry__0_n_7}),
        .DI({1'b0,1'b0,output_data[14:9]}),
        .O({NLW_output_data_tmp0_carry__0_O_UNCONNECTED[7],output_data_tmp0[15:9]}),
        .S({1'b0,output_data_tmp0_carry__0_i_1_n_0,output_data_tmp0_carry__0_i_2_n_0,output_data_tmp0_carry__0_i_3_n_0,output_data_tmp0_carry__0_i_4_n_0,output_data_tmp0_carry__0_i_5_n_0,output_data_tmp0_carry__0_i_6_n_0,output_data_tmp0_carry__0_i_7_n_0}));
  LUT1 #(
    .INIT(2'h1)) 
    output_data_tmp0_carry__0_i_1
       (.I0(output_data[15]),
        .O(output_data_tmp0_carry__0_i_1_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    output_data_tmp0_carry__0_i_2
       (.I0(output_data[14]),
        .O(output_data_tmp0_carry__0_i_2_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    output_data_tmp0_carry__0_i_3
       (.I0(output_data[13]),
        .O(output_data_tmp0_carry__0_i_3_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    output_data_tmp0_carry__0_i_4
       (.I0(output_data[12]),
        .O(output_data_tmp0_carry__0_i_4_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    output_data_tmp0_carry__0_i_5
       (.I0(output_data[11]),
        .O(output_data_tmp0_carry__0_i_5_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    output_data_tmp0_carry__0_i_6
       (.I0(output_data[10]),
        .O(output_data_tmp0_carry__0_i_6_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    output_data_tmp0_carry__0_i_7
       (.I0(output_data[9]),
        .O(output_data_tmp0_carry__0_i_7_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    output_data_tmp0_carry_i_1
       (.I0(output_data[8]),
        .O(output_data_tmp0_carry_i_1_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    output_data_tmp0_carry_i_2
       (.I0(output_data[7]),
        .O(output_data_tmp0_carry_i_2_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    output_data_tmp0_carry_i_3
       (.I0(output_data[6]),
        .O(output_data_tmp0_carry_i_3_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    output_data_tmp0_carry_i_4
       (.I0(output_data[5]),
        .O(output_data_tmp0_carry_i_4_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    output_data_tmp0_carry_i_5
       (.I0(output_data[4]),
        .O(output_data_tmp0_carry_i_5_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    output_data_tmp0_carry_i_6
       (.I0(output_data[3]),
        .O(output_data_tmp0_carry_i_6_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    output_data_tmp0_carry_i_7
       (.I0(output_data[2]),
        .O(output_data_tmp0_carry_i_7_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    output_data_tmp0_carry_i_8
       (.I0(output_data[1]),
        .O(output_data_tmp0_carry_i_8_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    \output_data_tmp[0]_i_1 
       (.I0(output_data[0]),
        .O(output_data_tmp0[0]));
  FDRE #(
    .INIT(1'b1)) 
    \output_data_tmp_reg[0] 
       (.C(clk),
        .CE(state__0[2]),
        .D(output_data_tmp0[0]),
        .Q(output_data[0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b1)) 
    \output_data_tmp_reg[10] 
       (.C(clk),
        .CE(state__0[2]),
        .D(output_data_tmp0[10]),
        .Q(output_data[10]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b1)) 
    \output_data_tmp_reg[11] 
       (.C(clk),
        .CE(state__0[2]),
        .D(output_data_tmp0[11]),
        .Q(output_data[11]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b1)) 
    \output_data_tmp_reg[12] 
       (.C(clk),
        .CE(state__0[2]),
        .D(output_data_tmp0[12]),
        .Q(output_data[12]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b1)) 
    \output_data_tmp_reg[13] 
       (.C(clk),
        .CE(state__0[2]),
        .D(output_data_tmp0[13]),
        .Q(output_data[13]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b1)) 
    \output_data_tmp_reg[14] 
       (.C(clk),
        .CE(state__0[2]),
        .D(output_data_tmp0[14]),
        .Q(output_data[14]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b1)) 
    \output_data_tmp_reg[15] 
       (.C(clk),
        .CE(state__0[2]),
        .D(output_data_tmp0[15]),
        .Q(output_data[15]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b1)) 
    \output_data_tmp_reg[1] 
       (.C(clk),
        .CE(state__0[2]),
        .D(output_data_tmp0[1]),
        .Q(output_data[1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b1)) 
    \output_data_tmp_reg[2] 
       (.C(clk),
        .CE(state__0[2]),
        .D(output_data_tmp0[2]),
        .Q(output_data[2]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b1)) 
    \output_data_tmp_reg[3] 
       (.C(clk),
        .CE(state__0[2]),
        .D(output_data_tmp0[3]),
        .Q(output_data[3]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b1)) 
    \output_data_tmp_reg[4] 
       (.C(clk),
        .CE(state__0[2]),
        .D(output_data_tmp0[4]),
        .Q(output_data[4]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b1)) 
    \output_data_tmp_reg[5] 
       (.C(clk),
        .CE(state__0[2]),
        .D(output_data_tmp0[5]),
        .Q(output_data[5]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b1)) 
    \output_data_tmp_reg[6] 
       (.C(clk),
        .CE(state__0[2]),
        .D(output_data_tmp0[6]),
        .Q(output_data[6]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b1)) 
    \output_data_tmp_reg[7] 
       (.C(clk),
        .CE(state__0[2]),
        .D(output_data_tmp0[7]),
        .Q(output_data[7]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b1)) 
    \output_data_tmp_reg[8] 
       (.C(clk),
        .CE(state__0[2]),
        .D(output_data_tmp0[8]),
        .Q(output_data[8]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b1)) 
    \output_data_tmp_reg[9] 
       (.C(clk),
        .CE(state__0[2]),
        .D(output_data_tmp0[9]),
        .Q(output_data[9]),
        .R(1'b0));
  LUT6 #(
    .INIT(64'hFF38FFF800380038)) 
    trigger_axi_master_start_i_1
       (.I0(axi_master_done),
        .I1(state[1]),
        .I2(state[0]),
        .I3(state[2]),
        .I4(trigger_axi_master_start_i_2_n_0),
        .I5(trigger_axi_master_start),
        .O(trigger_axi_master_start_i_1_n_0));
  LUT2 #(
    .INIT(4'h7)) 
    trigger_axi_master_start_i_10
       (.I0(\ctr_reg_n_0_[17] ),
        .I1(\ctr_reg_n_0_[16] ),
        .O(trigger_axi_master_start_i_10_n_0));
  LUT6 #(
    .INIT(64'h0000000000000100)) 
    trigger_axi_master_start_i_2
       (.I0(\ctr_reg_n_0_[26] ),
        .I1(\ctr_reg_n_0_[25] ),
        .I2(\ctr_reg_n_0_[24] ),
        .I3(trigger_axi_master_start_i_3_n_0),
        .I4(trigger_axi_master_start_i_4_n_0),
        .I5(trigger_axi_master_start_i_5_n_0),
        .O(trigger_axi_master_start_i_2_n_0));
  LUT4 #(
    .INIT(16'h0004)) 
    trigger_axi_master_start_i_3
       (.I0(\ctr_reg_n_0_[29] ),
        .I1(axi_master_done),
        .I2(\ctr_reg_n_0_[28] ),
        .I3(\ctr_reg_n_0_[27] ),
        .O(trigger_axi_master_start_i_3_n_0));
  LUT5 #(
    .INIT(32'hFFFFFFBF)) 
    trigger_axi_master_start_i_4
       (.I0(trigger_axi_master_start_i_6_n_0),
        .I1(trigger_axi_master_start_i_7_n_0),
        .I2(trigger_axi_master_start_i_8_n_0),
        .I3(\ctr_reg_n_0_[7] ),
        .I4(\ctr_reg_n_0_[8] ),
        .O(trigger_axi_master_start_i_4_n_0));
  LUT6 #(
    .INIT(64'hFFFEFFFFFFFFFFFF)) 
    trigger_axi_master_start_i_5
       (.I0(trigger_axi_master_start_i_9_n_0),
        .I1(\ctr_reg_n_0_[20] ),
        .I2(trigger_axi_master_start_i_10_n_0),
        .I3(\ctr_reg_n_0_[21] ),
        .I4(\ctr_reg_n_0_[18] ),
        .I5(\ctr_reg_n_0_[19] ),
        .O(trigger_axi_master_start_i_5_n_0));
  LUT4 #(
    .INIT(16'hFFEF)) 
    trigger_axi_master_start_i_6
       (.I0(\ctr_reg_n_0_[12] ),
        .I1(\ctr_reg_n_0_[15] ),
        .I2(\ctr_reg_n_0_[14] ),
        .I3(\ctr_reg_n_0_[13] ),
        .O(trigger_axi_master_start_i_6_n_0));
  LUT4 #(
    .INIT(16'h0001)) 
    trigger_axi_master_start_i_7
       (.I0(\ctr_reg_n_0_[31] ),
        .I1(\ctr_reg_n_0_[30] ),
        .I2(\ctr_reg_n_0_[5] ),
        .I3(\ctr_reg_n_0_[4] ),
        .O(trigger_axi_master_start_i_7_n_0));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT4 #(
    .INIT(16'h0001)) 
    trigger_axi_master_start_i_8
       (.I0(\ctr_reg_n_0_[1] ),
        .I1(\ctr_reg_n_0_[0] ),
        .I2(\ctr_reg_n_0_[3] ),
        .I3(\ctr_reg_n_0_[2] ),
        .O(trigger_axi_master_start_i_8_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFBFF)) 
    trigger_axi_master_start_i_9
       (.I0(\ctr_reg_n_0_[10] ),
        .I1(\ctr_reg_n_0_[6] ),
        .I2(\ctr_reg_n_0_[11] ),
        .I3(\ctr_reg_n_0_[9] ),
        .I4(\ctr_reg_n_0_[22] ),
        .I5(\ctr_reg_n_0_[23] ),
        .O(trigger_axi_master_start_i_9_n_0));
  FDRE trigger_axi_master_start_reg
       (.C(clk),
        .CE(1'b1),
        .D(trigger_axi_master_start_i_1_n_0),
        .Q(trigger_axi_master_start),
        .R(1'b0));
endmodule
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;
    parameter GRES_WIDTH = 10000;
    parameter GRES_START = 10000;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    wire GRESTORE;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;
    reg GRESTORE_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;
    assign (strong1, weak0) GRESTORE = GRESTORE_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

    initial begin 
	GRESTORE_int = 1'b0;
	#(GRES_START);
	GRESTORE_int = 1'b1;
	#(GRES_WIDTH);
	GRESTORE_int = 1'b0;
    end

endmodule
`endif
