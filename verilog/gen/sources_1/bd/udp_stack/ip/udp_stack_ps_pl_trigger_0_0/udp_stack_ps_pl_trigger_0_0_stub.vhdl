-- Copyright 1986-2022 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2022.1 (lin64) Build 3526262 Mon Apr 18 15:47:01 MDT 2022
-- Date        : Thu Feb 29 12:37:56 2024
-- Host        : user-OptiPlex-5000 running 64-bit Ubuntu 22.04.3 LTS
-- Command     : write_vhdl -force -mode synth_stub
--               /home/florianfrank/Documents/xilinx_open_source_udp_stack/Verilog_UDP_Stack.gen/sources_1/bd/udp_stack/ip/udp_stack_ps_pl_trigger_0_0/udp_stack_ps_pl_trigger_0_0_stub.vhdl
-- Design      : udp_stack_ps_pl_trigger_0_0
-- Purpose     : Stub declaration of top-level module interface
-- Device      : xczu9eg-ffvb1156-2-e
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity udp_stack_ps_pl_trigger_0_0 is
  Port ( 
    clk : in STD_LOGIC;
    axi_master_done : in STD_LOGIC;
    trigger_axi_master_start : out STD_LOGIC;
    output_address : out STD_LOGIC_VECTOR ( 31 downto 0 );
    output_data : out STD_LOGIC_VECTOR ( 15 downto 0 )
  );

end udp_stack_ps_pl_trigger_0_0;

architecture stub of udp_stack_ps_pl_trigger_0_0 is
attribute syn_black_box : boolean;
attribute black_box_pad_pin : string;
attribute syn_black_box of stub : architecture is true;
attribute black_box_pad_pin of stub : architecture is "clk,axi_master_done,trigger_axi_master_start,output_address[31:0],output_data[15:0]";
attribute X_CORE_INFO : string;
attribute X_CORE_INFO of stub : architecture is "ps_pl_trigger,Vivado 2022.1";
begin
end;
