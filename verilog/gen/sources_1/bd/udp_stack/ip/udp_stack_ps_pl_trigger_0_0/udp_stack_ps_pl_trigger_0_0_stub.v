// Copyright 1986-2022 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2022.1 (lin64) Build 3526262 Mon Apr 18 15:47:01 MDT 2022
// Date        : Thu Feb 29 12:37:56 2024
// Host        : user-OptiPlex-5000 running 64-bit Ubuntu 22.04.3 LTS
// Command     : write_verilog -force -mode synth_stub
//               /home/florianfrank/Documents/xilinx_open_source_udp_stack/Verilog_UDP_Stack.gen/sources_1/bd/udp_stack/ip/udp_stack_ps_pl_trigger_0_0/udp_stack_ps_pl_trigger_0_0_stub.v
// Design      : udp_stack_ps_pl_trigger_0_0
// Purpose     : Stub declaration of top-level module interface
// Device      : xczu9eg-ffvb1156-2-e
// --------------------------------------------------------------------------------

// This empty module with port declaration file causes synthesis tools to infer a black box for IP.
// The synthesis directives are for Synopsys Synplify support to prevent IO buffer insertion.
// Please paste the declaration into a Verilog source file or add the file as an additional source.
(* X_CORE_INFO = "ps_pl_trigger,Vivado 2022.1" *)
module udp_stack_ps_pl_trigger_0_0(clk, axi_master_done, 
  trigger_axi_master_start, output_address, output_data)
/* synthesis syn_black_box black_box_pad_pin="clk,axi_master_done,trigger_axi_master_start,output_address[31:0],output_data[15:0]" */;
  input clk;
  input axi_master_done;
  output trigger_axi_master_start;
  output [31:0]output_address;
  output [15:0]output_data;
endmodule
