-- Copyright 1986-2022 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2022.1 (lin64) Build 3526262 Mon Apr 18 15:47:01 MDT 2022
-- Date        : Thu Feb 29 12:37:56 2024
-- Host        : user-OptiPlex-5000 running 64-bit Ubuntu 22.04.3 LTS
-- Command     : write_vhdl -force -mode funcsim
--               /home/florianfrank/Documents/xilinx_open_source_udp_stack/Verilog_UDP_Stack.gen/sources_1/bd/udp_stack/ip/udp_stack_ps_pl_trigger_0_0/udp_stack_ps_pl_trigger_0_0_sim_netlist.vhdl
-- Design      : udp_stack_ps_pl_trigger_0_0
-- Purpose     : This VHDL netlist is a functional simulation representation of the design and should not be modified or
--               synthesized. This netlist cannot be used for SDF annotated simulation.
-- Device      : xczu9eg-ffvb1156-2-e
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity udp_stack_ps_pl_trigger_0_0_ps_pl_trigger is
  port (
    output_address : out STD_LOGIC_VECTOR ( 31 downto 0 );
    output_data : out STD_LOGIC_VECTOR ( 15 downto 0 );
    trigger_axi_master_start : out STD_LOGIC;
    clk : in STD_LOGIC;
    axi_master_done : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of udp_stack_ps_pl_trigger_0_0_ps_pl_trigger : entity is "ps_pl_trigger";
end udp_stack_ps_pl_trigger_0_0_ps_pl_trigger;

architecture STRUCTURE of udp_stack_ps_pl_trigger_0_0_ps_pl_trigger is
  signal \FSM_sequential_state[2]_i_1_n_0\ : STD_LOGIC;
  signal \ctr1__14_carry__0_i_1_n_0\ : STD_LOGIC;
  signal \ctr1__14_carry__0_i_2_n_0\ : STD_LOGIC;
  signal \ctr1__14_carry__0_i_3_n_0\ : STD_LOGIC;
  signal \ctr1__14_carry__0_i_4_n_0\ : STD_LOGIC;
  signal \ctr1__14_carry__0_i_5_n_0\ : STD_LOGIC;
  signal \ctr1__14_carry__0_n_4\ : STD_LOGIC;
  signal \ctr1__14_carry__0_n_5\ : STD_LOGIC;
  signal \ctr1__14_carry__0_n_6\ : STD_LOGIC;
  signal \ctr1__14_carry__0_n_7\ : STD_LOGIC;
  signal \ctr1__14_carry_i_10_n_0\ : STD_LOGIC;
  signal \ctr1__14_carry_i_11_n_0\ : STD_LOGIC;
  signal \ctr1__14_carry_i_12_n_0\ : STD_LOGIC;
  signal \ctr1__14_carry_i_13_n_0\ : STD_LOGIC;
  signal \ctr1__14_carry_i_1_n_0\ : STD_LOGIC;
  signal \ctr1__14_carry_i_2_n_0\ : STD_LOGIC;
  signal \ctr1__14_carry_i_3_n_0\ : STD_LOGIC;
  signal \ctr1__14_carry_i_4_n_0\ : STD_LOGIC;
  signal \ctr1__14_carry_i_5_n_0\ : STD_LOGIC;
  signal \ctr1__14_carry_i_6_n_0\ : STD_LOGIC;
  signal \ctr1__14_carry_i_7_n_0\ : STD_LOGIC;
  signal \ctr1__14_carry_i_8_n_0\ : STD_LOGIC;
  signal \ctr1__14_carry_i_9_n_0\ : STD_LOGIC;
  signal \ctr1__14_carry_n_0\ : STD_LOGIC;
  signal \ctr1__14_carry_n_1\ : STD_LOGIC;
  signal \ctr1__14_carry_n_2\ : STD_LOGIC;
  signal \ctr1__14_carry_n_3\ : STD_LOGIC;
  signal \ctr1__14_carry_n_4\ : STD_LOGIC;
  signal \ctr1__14_carry_n_5\ : STD_LOGIC;
  signal \ctr1__14_carry_n_6\ : STD_LOGIC;
  signal \ctr1__14_carry_n_7\ : STD_LOGIC;
  signal \ctr1_carry__0_i_1_n_0\ : STD_LOGIC;
  signal \ctr1_carry__0_i_2_n_0\ : STD_LOGIC;
  signal \ctr1_carry__0_i_3_n_0\ : STD_LOGIC;
  signal \ctr1_carry__0_i_4_n_0\ : STD_LOGIC;
  signal \ctr1_carry__0_i_5_n_0\ : STD_LOGIC;
  signal \ctr1_carry__0_i_6_n_0\ : STD_LOGIC;
  signal \ctr1_carry__0_i_7_n_0\ : STD_LOGIC;
  signal \ctr1_carry__0_n_1\ : STD_LOGIC;
  signal \ctr1_carry__0_n_2\ : STD_LOGIC;
  signal \ctr1_carry__0_n_3\ : STD_LOGIC;
  signal \ctr1_carry__0_n_4\ : STD_LOGIC;
  signal \ctr1_carry__0_n_5\ : STD_LOGIC;
  signal \ctr1_carry__0_n_6\ : STD_LOGIC;
  signal \ctr1_carry__0_n_7\ : STD_LOGIC;
  signal ctr1_carry_i_10_n_0 : STD_LOGIC;
  signal ctr1_carry_i_11_n_0 : STD_LOGIC;
  signal ctr1_carry_i_12_n_0 : STD_LOGIC;
  signal ctr1_carry_i_1_n_0 : STD_LOGIC;
  signal ctr1_carry_i_2_n_0 : STD_LOGIC;
  signal ctr1_carry_i_3_n_0 : STD_LOGIC;
  signal ctr1_carry_i_4_n_0 : STD_LOGIC;
  signal ctr1_carry_i_5_n_0 : STD_LOGIC;
  signal ctr1_carry_i_6_n_0 : STD_LOGIC;
  signal ctr1_carry_i_7_n_0 : STD_LOGIC;
  signal ctr1_carry_i_8_n_0 : STD_LOGIC;
  signal ctr1_carry_i_9_n_0 : STD_LOGIC;
  signal ctr1_carry_n_0 : STD_LOGIC;
  signal ctr1_carry_n_1 : STD_LOGIC;
  signal ctr1_carry_n_2 : STD_LOGIC;
  signal ctr1_carry_n_3 : STD_LOGIC;
  signal ctr1_carry_n_4 : STD_LOGIC;
  signal ctr1_carry_n_5 : STD_LOGIC;
  signal ctr1_carry_n_6 : STD_LOGIC;
  signal ctr1_carry_n_7 : STD_LOGIC;
  signal \ctr[0]_i_1_n_0\ : STD_LOGIC;
  signal \ctr[31]_i_1_n_0\ : STD_LOGIC;
  signal \ctr[31]_i_2_n_0\ : STD_LOGIC;
  signal \ctr_reg[16]_i_1_n_0\ : STD_LOGIC;
  signal \ctr_reg[16]_i_1_n_1\ : STD_LOGIC;
  signal \ctr_reg[16]_i_1_n_10\ : STD_LOGIC;
  signal \ctr_reg[16]_i_1_n_11\ : STD_LOGIC;
  signal \ctr_reg[16]_i_1_n_12\ : STD_LOGIC;
  signal \ctr_reg[16]_i_1_n_13\ : STD_LOGIC;
  signal \ctr_reg[16]_i_1_n_14\ : STD_LOGIC;
  signal \ctr_reg[16]_i_1_n_15\ : STD_LOGIC;
  signal \ctr_reg[16]_i_1_n_2\ : STD_LOGIC;
  signal \ctr_reg[16]_i_1_n_3\ : STD_LOGIC;
  signal \ctr_reg[16]_i_1_n_4\ : STD_LOGIC;
  signal \ctr_reg[16]_i_1_n_5\ : STD_LOGIC;
  signal \ctr_reg[16]_i_1_n_6\ : STD_LOGIC;
  signal \ctr_reg[16]_i_1_n_7\ : STD_LOGIC;
  signal \ctr_reg[16]_i_1_n_8\ : STD_LOGIC;
  signal \ctr_reg[16]_i_1_n_9\ : STD_LOGIC;
  signal \ctr_reg[24]_i_1_n_0\ : STD_LOGIC;
  signal \ctr_reg[24]_i_1_n_1\ : STD_LOGIC;
  signal \ctr_reg[24]_i_1_n_10\ : STD_LOGIC;
  signal \ctr_reg[24]_i_1_n_11\ : STD_LOGIC;
  signal \ctr_reg[24]_i_1_n_12\ : STD_LOGIC;
  signal \ctr_reg[24]_i_1_n_13\ : STD_LOGIC;
  signal \ctr_reg[24]_i_1_n_14\ : STD_LOGIC;
  signal \ctr_reg[24]_i_1_n_15\ : STD_LOGIC;
  signal \ctr_reg[24]_i_1_n_2\ : STD_LOGIC;
  signal \ctr_reg[24]_i_1_n_3\ : STD_LOGIC;
  signal \ctr_reg[24]_i_1_n_4\ : STD_LOGIC;
  signal \ctr_reg[24]_i_1_n_5\ : STD_LOGIC;
  signal \ctr_reg[24]_i_1_n_6\ : STD_LOGIC;
  signal \ctr_reg[24]_i_1_n_7\ : STD_LOGIC;
  signal \ctr_reg[24]_i_1_n_8\ : STD_LOGIC;
  signal \ctr_reg[24]_i_1_n_9\ : STD_LOGIC;
  signal \ctr_reg[31]_i_3_n_10\ : STD_LOGIC;
  signal \ctr_reg[31]_i_3_n_11\ : STD_LOGIC;
  signal \ctr_reg[31]_i_3_n_12\ : STD_LOGIC;
  signal \ctr_reg[31]_i_3_n_13\ : STD_LOGIC;
  signal \ctr_reg[31]_i_3_n_14\ : STD_LOGIC;
  signal \ctr_reg[31]_i_3_n_15\ : STD_LOGIC;
  signal \ctr_reg[31]_i_3_n_2\ : STD_LOGIC;
  signal \ctr_reg[31]_i_3_n_3\ : STD_LOGIC;
  signal \ctr_reg[31]_i_3_n_4\ : STD_LOGIC;
  signal \ctr_reg[31]_i_3_n_5\ : STD_LOGIC;
  signal \ctr_reg[31]_i_3_n_6\ : STD_LOGIC;
  signal \ctr_reg[31]_i_3_n_7\ : STD_LOGIC;
  signal \ctr_reg[31]_i_3_n_9\ : STD_LOGIC;
  signal \ctr_reg[8]_i_1_n_0\ : STD_LOGIC;
  signal \ctr_reg[8]_i_1_n_1\ : STD_LOGIC;
  signal \ctr_reg[8]_i_1_n_10\ : STD_LOGIC;
  signal \ctr_reg[8]_i_1_n_11\ : STD_LOGIC;
  signal \ctr_reg[8]_i_1_n_12\ : STD_LOGIC;
  signal \ctr_reg[8]_i_1_n_13\ : STD_LOGIC;
  signal \ctr_reg[8]_i_1_n_14\ : STD_LOGIC;
  signal \ctr_reg[8]_i_1_n_15\ : STD_LOGIC;
  signal \ctr_reg[8]_i_1_n_2\ : STD_LOGIC;
  signal \ctr_reg[8]_i_1_n_3\ : STD_LOGIC;
  signal \ctr_reg[8]_i_1_n_4\ : STD_LOGIC;
  signal \ctr_reg[8]_i_1_n_5\ : STD_LOGIC;
  signal \ctr_reg[8]_i_1_n_6\ : STD_LOGIC;
  signal \ctr_reg[8]_i_1_n_7\ : STD_LOGIC;
  signal \ctr_reg[8]_i_1_n_8\ : STD_LOGIC;
  signal \ctr_reg[8]_i_1_n_9\ : STD_LOGIC;
  signal \ctr_reg_n_0_[0]\ : STD_LOGIC;
  signal \ctr_reg_n_0_[10]\ : STD_LOGIC;
  signal \ctr_reg_n_0_[11]\ : STD_LOGIC;
  signal \ctr_reg_n_0_[12]\ : STD_LOGIC;
  signal \ctr_reg_n_0_[13]\ : STD_LOGIC;
  signal \ctr_reg_n_0_[14]\ : STD_LOGIC;
  signal \ctr_reg_n_0_[15]\ : STD_LOGIC;
  signal \ctr_reg_n_0_[16]\ : STD_LOGIC;
  signal \ctr_reg_n_0_[17]\ : STD_LOGIC;
  signal \ctr_reg_n_0_[18]\ : STD_LOGIC;
  signal \ctr_reg_n_0_[19]\ : STD_LOGIC;
  signal \ctr_reg_n_0_[1]\ : STD_LOGIC;
  signal \ctr_reg_n_0_[20]\ : STD_LOGIC;
  signal \ctr_reg_n_0_[21]\ : STD_LOGIC;
  signal \ctr_reg_n_0_[22]\ : STD_LOGIC;
  signal \ctr_reg_n_0_[23]\ : STD_LOGIC;
  signal \ctr_reg_n_0_[24]\ : STD_LOGIC;
  signal \ctr_reg_n_0_[25]\ : STD_LOGIC;
  signal \ctr_reg_n_0_[26]\ : STD_LOGIC;
  signal \ctr_reg_n_0_[27]\ : STD_LOGIC;
  signal \ctr_reg_n_0_[28]\ : STD_LOGIC;
  signal \ctr_reg_n_0_[29]\ : STD_LOGIC;
  signal \ctr_reg_n_0_[2]\ : STD_LOGIC;
  signal \ctr_reg_n_0_[30]\ : STD_LOGIC;
  signal \ctr_reg_n_0_[31]\ : STD_LOGIC;
  signal \ctr_reg_n_0_[3]\ : STD_LOGIC;
  signal \ctr_reg_n_0_[4]\ : STD_LOGIC;
  signal \ctr_reg_n_0_[5]\ : STD_LOGIC;
  signal \ctr_reg_n_0_[6]\ : STD_LOGIC;
  signal \ctr_reg_n_0_[7]\ : STD_LOGIC;
  signal \ctr_reg_n_0_[8]\ : STD_LOGIC;
  signal \ctr_reg_n_0_[9]\ : STD_LOGIC;
  signal \^output_address\ : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal \output_address_tmp[7]_i_2_n_0\ : STD_LOGIC;
  signal \output_address_tmp_reg[15]_i_1_n_0\ : STD_LOGIC;
  signal \output_address_tmp_reg[15]_i_1_n_1\ : STD_LOGIC;
  signal \output_address_tmp_reg[15]_i_1_n_10\ : STD_LOGIC;
  signal \output_address_tmp_reg[15]_i_1_n_11\ : STD_LOGIC;
  signal \output_address_tmp_reg[15]_i_1_n_12\ : STD_LOGIC;
  signal \output_address_tmp_reg[15]_i_1_n_13\ : STD_LOGIC;
  signal \output_address_tmp_reg[15]_i_1_n_14\ : STD_LOGIC;
  signal \output_address_tmp_reg[15]_i_1_n_15\ : STD_LOGIC;
  signal \output_address_tmp_reg[15]_i_1_n_2\ : STD_LOGIC;
  signal \output_address_tmp_reg[15]_i_1_n_3\ : STD_LOGIC;
  signal \output_address_tmp_reg[15]_i_1_n_4\ : STD_LOGIC;
  signal \output_address_tmp_reg[15]_i_1_n_5\ : STD_LOGIC;
  signal \output_address_tmp_reg[15]_i_1_n_6\ : STD_LOGIC;
  signal \output_address_tmp_reg[15]_i_1_n_7\ : STD_LOGIC;
  signal \output_address_tmp_reg[15]_i_1_n_8\ : STD_LOGIC;
  signal \output_address_tmp_reg[15]_i_1_n_9\ : STD_LOGIC;
  signal \output_address_tmp_reg[23]_i_1_n_0\ : STD_LOGIC;
  signal \output_address_tmp_reg[23]_i_1_n_1\ : STD_LOGIC;
  signal \output_address_tmp_reg[23]_i_1_n_10\ : STD_LOGIC;
  signal \output_address_tmp_reg[23]_i_1_n_11\ : STD_LOGIC;
  signal \output_address_tmp_reg[23]_i_1_n_12\ : STD_LOGIC;
  signal \output_address_tmp_reg[23]_i_1_n_13\ : STD_LOGIC;
  signal \output_address_tmp_reg[23]_i_1_n_14\ : STD_LOGIC;
  signal \output_address_tmp_reg[23]_i_1_n_15\ : STD_LOGIC;
  signal \output_address_tmp_reg[23]_i_1_n_2\ : STD_LOGIC;
  signal \output_address_tmp_reg[23]_i_1_n_3\ : STD_LOGIC;
  signal \output_address_tmp_reg[23]_i_1_n_4\ : STD_LOGIC;
  signal \output_address_tmp_reg[23]_i_1_n_5\ : STD_LOGIC;
  signal \output_address_tmp_reg[23]_i_1_n_6\ : STD_LOGIC;
  signal \output_address_tmp_reg[23]_i_1_n_7\ : STD_LOGIC;
  signal \output_address_tmp_reg[23]_i_1_n_8\ : STD_LOGIC;
  signal \output_address_tmp_reg[23]_i_1_n_9\ : STD_LOGIC;
  signal \output_address_tmp_reg[31]_i_2_n_1\ : STD_LOGIC;
  signal \output_address_tmp_reg[31]_i_2_n_10\ : STD_LOGIC;
  signal \output_address_tmp_reg[31]_i_2_n_11\ : STD_LOGIC;
  signal \output_address_tmp_reg[31]_i_2_n_12\ : STD_LOGIC;
  signal \output_address_tmp_reg[31]_i_2_n_13\ : STD_LOGIC;
  signal \output_address_tmp_reg[31]_i_2_n_14\ : STD_LOGIC;
  signal \output_address_tmp_reg[31]_i_2_n_15\ : STD_LOGIC;
  signal \output_address_tmp_reg[31]_i_2_n_2\ : STD_LOGIC;
  signal \output_address_tmp_reg[31]_i_2_n_3\ : STD_LOGIC;
  signal \output_address_tmp_reg[31]_i_2_n_4\ : STD_LOGIC;
  signal \output_address_tmp_reg[31]_i_2_n_5\ : STD_LOGIC;
  signal \output_address_tmp_reg[31]_i_2_n_6\ : STD_LOGIC;
  signal \output_address_tmp_reg[31]_i_2_n_7\ : STD_LOGIC;
  signal \output_address_tmp_reg[31]_i_2_n_8\ : STD_LOGIC;
  signal \output_address_tmp_reg[31]_i_2_n_9\ : STD_LOGIC;
  signal \output_address_tmp_reg[7]_i_1_n_0\ : STD_LOGIC;
  signal \output_address_tmp_reg[7]_i_1_n_1\ : STD_LOGIC;
  signal \output_address_tmp_reg[7]_i_1_n_10\ : STD_LOGIC;
  signal \output_address_tmp_reg[7]_i_1_n_11\ : STD_LOGIC;
  signal \output_address_tmp_reg[7]_i_1_n_12\ : STD_LOGIC;
  signal \output_address_tmp_reg[7]_i_1_n_13\ : STD_LOGIC;
  signal \output_address_tmp_reg[7]_i_1_n_14\ : STD_LOGIC;
  signal \output_address_tmp_reg[7]_i_1_n_15\ : STD_LOGIC;
  signal \output_address_tmp_reg[7]_i_1_n_2\ : STD_LOGIC;
  signal \output_address_tmp_reg[7]_i_1_n_3\ : STD_LOGIC;
  signal \output_address_tmp_reg[7]_i_1_n_4\ : STD_LOGIC;
  signal \output_address_tmp_reg[7]_i_1_n_5\ : STD_LOGIC;
  signal \output_address_tmp_reg[7]_i_1_n_6\ : STD_LOGIC;
  signal \output_address_tmp_reg[7]_i_1_n_7\ : STD_LOGIC;
  signal \output_address_tmp_reg[7]_i_1_n_8\ : STD_LOGIC;
  signal \output_address_tmp_reg[7]_i_1_n_9\ : STD_LOGIC;
  signal \^output_data\ : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal output_data_tmp0 : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal \output_data_tmp0_carry__0_i_1_n_0\ : STD_LOGIC;
  signal \output_data_tmp0_carry__0_i_2_n_0\ : STD_LOGIC;
  signal \output_data_tmp0_carry__0_i_3_n_0\ : STD_LOGIC;
  signal \output_data_tmp0_carry__0_i_4_n_0\ : STD_LOGIC;
  signal \output_data_tmp0_carry__0_i_5_n_0\ : STD_LOGIC;
  signal \output_data_tmp0_carry__0_i_6_n_0\ : STD_LOGIC;
  signal \output_data_tmp0_carry__0_i_7_n_0\ : STD_LOGIC;
  signal \output_data_tmp0_carry__0_n_2\ : STD_LOGIC;
  signal \output_data_tmp0_carry__0_n_3\ : STD_LOGIC;
  signal \output_data_tmp0_carry__0_n_4\ : STD_LOGIC;
  signal \output_data_tmp0_carry__0_n_5\ : STD_LOGIC;
  signal \output_data_tmp0_carry__0_n_6\ : STD_LOGIC;
  signal \output_data_tmp0_carry__0_n_7\ : STD_LOGIC;
  signal output_data_tmp0_carry_i_1_n_0 : STD_LOGIC;
  signal output_data_tmp0_carry_i_2_n_0 : STD_LOGIC;
  signal output_data_tmp0_carry_i_3_n_0 : STD_LOGIC;
  signal output_data_tmp0_carry_i_4_n_0 : STD_LOGIC;
  signal output_data_tmp0_carry_i_5_n_0 : STD_LOGIC;
  signal output_data_tmp0_carry_i_6_n_0 : STD_LOGIC;
  signal output_data_tmp0_carry_i_7_n_0 : STD_LOGIC;
  signal output_data_tmp0_carry_i_8_n_0 : STD_LOGIC;
  signal output_data_tmp0_carry_n_0 : STD_LOGIC;
  signal output_data_tmp0_carry_n_1 : STD_LOGIC;
  signal output_data_tmp0_carry_n_2 : STD_LOGIC;
  signal output_data_tmp0_carry_n_3 : STD_LOGIC;
  signal output_data_tmp0_carry_n_4 : STD_LOGIC;
  signal output_data_tmp0_carry_n_5 : STD_LOGIC;
  signal output_data_tmp0_carry_n_6 : STD_LOGIC;
  signal output_data_tmp0_carry_n_7 : STD_LOGIC;
  signal p_0_in : STD_LOGIC;
  signal state : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \state__0\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \^trigger_axi_master_start\ : STD_LOGIC;
  signal trigger_axi_master_start_i_10_n_0 : STD_LOGIC;
  signal trigger_axi_master_start_i_1_n_0 : STD_LOGIC;
  signal trigger_axi_master_start_i_2_n_0 : STD_LOGIC;
  signal trigger_axi_master_start_i_3_n_0 : STD_LOGIC;
  signal trigger_axi_master_start_i_4_n_0 : STD_LOGIC;
  signal trigger_axi_master_start_i_5_n_0 : STD_LOGIC;
  signal trigger_axi_master_start_i_6_n_0 : STD_LOGIC;
  signal trigger_axi_master_start_i_7_n_0 : STD_LOGIC;
  signal trigger_axi_master_start_i_8_n_0 : STD_LOGIC;
  signal trigger_axi_master_start_i_9_n_0 : STD_LOGIC;
  signal \NLW_ctr1__14_carry_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal \NLW_ctr1__14_carry__0_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 7 downto 5 );
  signal \NLW_ctr1__14_carry__0_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal NLW_ctr1_carry_O_UNCONNECTED : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal \NLW_ctr1_carry__0_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 7 to 7 );
  signal \NLW_ctr1_carry__0_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal \NLW_ctr_reg[31]_i_3_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 7 downto 6 );
  signal \NLW_ctr_reg[31]_i_3_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 7 to 7 );
  signal \NLW_output_address_tmp_reg[31]_i_2_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 7 to 7 );
  signal \NLW_output_data_tmp0_carry__0_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 7 downto 6 );
  signal \NLW_output_data_tmp0_carry__0_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 7 to 7 );
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \FSM_sequential_state[0]_i_1\ : label is "soft_lutpair0";
  attribute SOFT_HLUTNM of \FSM_sequential_state[1]_i_1\ : label is "soft_lutpair0";
  attribute FSM_ENCODED_STATES : string;
  attribute FSM_ENCODED_STATES of \FSM_sequential_state_reg[0]\ : label is "INIT:000,TRIGGER:001,WAIT_FOR_START:010,WAIT_FINISH:011,SLEEP:100";
  attribute FSM_ENCODED_STATES of \FSM_sequential_state_reg[1]\ : label is "INIT:000,TRIGGER:001,WAIT_FOR_START:010,WAIT_FINISH:011,SLEEP:100";
  attribute FSM_ENCODED_STATES of \FSM_sequential_state_reg[2]\ : label is "INIT:000,TRIGGER:001,WAIT_FOR_START:010,WAIT_FINISH:011,SLEEP:100";
  attribute COMPARATOR_THRESHOLD : integer;
  attribute COMPARATOR_THRESHOLD of \ctr1__14_carry\ : label is 11;
  attribute COMPARATOR_THRESHOLD of \ctr1__14_carry__0\ : label is 11;
  attribute COMPARATOR_THRESHOLD of ctr1_carry : label is 11;
  attribute COMPARATOR_THRESHOLD of \ctr1_carry__0\ : label is 11;
  attribute SOFT_HLUTNM of \ctr[0]_i_1\ : label is "soft_lutpair1";
  attribute ADDER_THRESHOLD : integer;
  attribute ADDER_THRESHOLD of \ctr_reg[16]_i_1\ : label is 35;
  attribute ADDER_THRESHOLD of \ctr_reg[24]_i_1\ : label is 35;
  attribute ADDER_THRESHOLD of \ctr_reg[31]_i_3\ : label is 35;
  attribute ADDER_THRESHOLD of \ctr_reg[8]_i_1\ : label is 35;
  attribute ADDER_THRESHOLD of \output_address_tmp_reg[15]_i_1\ : label is 16;
  attribute ADDER_THRESHOLD of \output_address_tmp_reg[23]_i_1\ : label is 16;
  attribute ADDER_THRESHOLD of \output_address_tmp_reg[31]_i_2\ : label is 16;
  attribute ADDER_THRESHOLD of \output_address_tmp_reg[7]_i_1\ : label is 16;
  attribute ADDER_THRESHOLD of output_data_tmp0_carry : label is 35;
  attribute ADDER_THRESHOLD of \output_data_tmp0_carry__0\ : label is 35;
  attribute SOFT_HLUTNM of trigger_axi_master_start_i_8 : label is "soft_lutpair1";
begin
  output_address(31 downto 0) <= \^output_address\(31 downto 0);
  output_data(15 downto 0) <= \^output_data\(15 downto 0);
  trigger_axi_master_start <= \^trigger_axi_master_start\;
\FSM_sequential_state[0]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0575"
    )
        port map (
      I0 => state(0),
      I1 => trigger_axi_master_start_i_2_n_0,
      I2 => state(1),
      I3 => state(2),
      O => \state__0\(0)
    );
\FSM_sequential_state[1]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"070C"
    )
        port map (
      I0 => trigger_axi_master_start_i_2_n_0,
      I1 => state(0),
      I2 => state(2),
      I3 => state(1),
      O => \state__0\(1)
    );
\FSM_sequential_state[2]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00FF00FF0350035F"
    )
        port map (
      I0 => axi_master_done,
      I1 => \ctr1_carry__0_n_1\,
      I2 => state(1),
      I3 => state(2),
      I4 => p_0_in,
      I5 => state(0),
      O => \FSM_sequential_state[2]_i_1_n_0\
    );
\FSM_sequential_state_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => \FSM_sequential_state[2]_i_1_n_0\,
      D => \state__0\(0),
      Q => state(0),
      R => '0'
    );
\FSM_sequential_state_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => \FSM_sequential_state[2]_i_1_n_0\,
      D => \state__0\(1),
      Q => state(1),
      R => '0'
    );
\FSM_sequential_state_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => \FSM_sequential_state[2]_i_1_n_0\,
      D => \state__0\(2),
      Q => state(2),
      R => '0'
    );
\ctr1__14_carry\: unisim.vcomponents.CARRY8
     port map (
      CI => '0',
      CI_TOP => '0',
      CO(7) => \ctr1__14_carry_n_0\,
      CO(6) => \ctr1__14_carry_n_1\,
      CO(5) => \ctr1__14_carry_n_2\,
      CO(4) => \ctr1__14_carry_n_3\,
      CO(3) => \ctr1__14_carry_n_4\,
      CO(2) => \ctr1__14_carry_n_5\,
      CO(1) => \ctr1__14_carry_n_6\,
      CO(0) => \ctr1__14_carry_n_7\,
      DI(7) => '0',
      DI(6) => \ctr1__14_carry_i_1_n_0\,
      DI(5) => \ctr1__14_carry_i_2_n_0\,
      DI(4) => \ctr1__14_carry_i_3_n_0\,
      DI(3 downto 2) => B"00",
      DI(1) => \ctr1__14_carry_i_4_n_0\,
      DI(0) => \ctr1__14_carry_i_5_n_0\,
      O(7 downto 0) => \NLW_ctr1__14_carry_O_UNCONNECTED\(7 downto 0),
      S(7) => \ctr1__14_carry_i_6_n_0\,
      S(6) => \ctr1__14_carry_i_7_n_0\,
      S(5) => \ctr1__14_carry_i_8_n_0\,
      S(4) => \ctr1__14_carry_i_9_n_0\,
      S(3) => \ctr1__14_carry_i_10_n_0\,
      S(2) => \ctr1__14_carry_i_11_n_0\,
      S(1) => \ctr1__14_carry_i_12_n_0\,
      S(0) => \ctr1__14_carry_i_13_n_0\
    );
\ctr1__14_carry__0\: unisim.vcomponents.CARRY8
     port map (
      CI => \ctr1__14_carry_n_0\,
      CI_TOP => '0',
      CO(7 downto 5) => \NLW_ctr1__14_carry__0_CO_UNCONNECTED\(7 downto 5),
      CO(4) => p_0_in,
      CO(3) => \ctr1__14_carry__0_n_4\,
      CO(2) => \ctr1__14_carry__0_n_5\,
      CO(1) => \ctr1__14_carry__0_n_6\,
      CO(0) => \ctr1__14_carry__0_n_7\,
      DI(7 downto 5) => B"000",
      DI(4) => \ctr_reg_n_0_[31]\,
      DI(3 downto 0) => B"0000",
      O(7 downto 0) => \NLW_ctr1__14_carry__0_O_UNCONNECTED\(7 downto 0),
      S(7 downto 5) => B"000",
      S(4) => \ctr1__14_carry__0_i_1_n_0\,
      S(3) => \ctr1__14_carry__0_i_2_n_0\,
      S(2) => \ctr1__14_carry__0_i_3_n_0\,
      S(1) => \ctr1__14_carry__0_i_4_n_0\,
      S(0) => \ctr1__14_carry__0_i_5_n_0\
    );
\ctr1__14_carry__0_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \ctr_reg_n_0_[31]\,
      I1 => \ctr_reg_n_0_[30]\,
      O => \ctr1__14_carry__0_i_1_n_0\
    );
\ctr1__14_carry__0_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \ctr_reg_n_0_[29]\,
      I1 => \ctr_reg_n_0_[28]\,
      O => \ctr1__14_carry__0_i_2_n_0\
    );
\ctr1__14_carry__0_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \ctr_reg_n_0_[27]\,
      I1 => \ctr_reg_n_0_[26]\,
      O => \ctr1__14_carry__0_i_3_n_0\
    );
\ctr1__14_carry__0_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \ctr_reg_n_0_[25]\,
      I1 => \ctr_reg_n_0_[24]\,
      O => \ctr1__14_carry__0_i_4_n_0\
    );
\ctr1__14_carry__0_i_5\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \ctr_reg_n_0_[22]\,
      I1 => \ctr_reg_n_0_[23]\,
      O => \ctr1__14_carry__0_i_5_n_0\
    );
\ctr1__14_carry_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"7"
    )
        port map (
      I0 => \ctr_reg_n_0_[19]\,
      I1 => \ctr_reg_n_0_[18]\,
      O => \ctr1__14_carry_i_1_n_0\
    );
\ctr1__14_carry_i_10\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \ctr_reg_n_0_[13]\,
      I1 => \ctr_reg_n_0_[12]\,
      O => \ctr1__14_carry_i_10_n_0\
    );
\ctr1__14_carry_i_11\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \ctr_reg_n_0_[11]\,
      I1 => \ctr_reg_n_0_[10]\,
      O => \ctr1__14_carry_i_11_n_0\
    );
\ctr1__14_carry_i_12\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \ctr_reg_n_0_[9]\,
      I1 => \ctr_reg_n_0_[8]\,
      O => \ctr1__14_carry_i_12_n_0\
    );
\ctr1__14_carry_i_13\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \ctr_reg_n_0_[6]\,
      I1 => \ctr_reg_n_0_[7]\,
      O => \ctr1__14_carry_i_13_n_0\
    );
\ctr1__14_carry_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"7"
    )
        port map (
      I0 => \ctr_reg_n_0_[17]\,
      I1 => \ctr_reg_n_0_[16]\,
      O => \ctr1__14_carry_i_2_n_0\
    );
\ctr1__14_carry_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \ctr_reg_n_0_[15]\,
      I1 => \ctr_reg_n_0_[14]\,
      O => \ctr1__14_carry_i_3_n_0\
    );
\ctr1__14_carry_i_4\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \ctr_reg_n_0_[9]\,
      O => \ctr1__14_carry_i_4_n_0\
    );
\ctr1__14_carry_i_5\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \ctr_reg_n_0_[7]\,
      I1 => \ctr_reg_n_0_[6]\,
      O => \ctr1__14_carry_i_5_n_0\
    );
\ctr1__14_carry_i_6\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \ctr_reg_n_0_[21]\,
      I1 => \ctr_reg_n_0_[20]\,
      O => \ctr1__14_carry_i_6_n_0\
    );
\ctr1__14_carry_i_7\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \ctr_reg_n_0_[18]\,
      I1 => \ctr_reg_n_0_[19]\,
      O => \ctr1__14_carry_i_7_n_0\
    );
\ctr1__14_carry_i_8\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \ctr_reg_n_0_[16]\,
      I1 => \ctr_reg_n_0_[17]\,
      O => \ctr1__14_carry_i_8_n_0\
    );
\ctr1__14_carry_i_9\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \ctr_reg_n_0_[14]\,
      I1 => \ctr_reg_n_0_[15]\,
      O => \ctr1__14_carry_i_9_n_0\
    );
ctr1_carry: unisim.vcomponents.CARRY8
     port map (
      CI => '0',
      CI_TOP => '0',
      CO(7) => ctr1_carry_n_0,
      CO(6) => ctr1_carry_n_1,
      CO(5) => ctr1_carry_n_2,
      CO(4) => ctr1_carry_n_3,
      CO(3) => ctr1_carry_n_4,
      CO(2) => ctr1_carry_n_5,
      CO(1) => ctr1_carry_n_6,
      CO(0) => ctr1_carry_n_7,
      DI(7 downto 4) => B"0000",
      DI(3) => ctr1_carry_i_1_n_0,
      DI(2) => ctr1_carry_i_2_n_0,
      DI(1) => ctr1_carry_i_3_n_0,
      DI(0) => ctr1_carry_i_4_n_0,
      O(7 downto 0) => NLW_ctr1_carry_O_UNCONNECTED(7 downto 0),
      S(7) => ctr1_carry_i_5_n_0,
      S(6) => ctr1_carry_i_6_n_0,
      S(5) => ctr1_carry_i_7_n_0,
      S(4) => ctr1_carry_i_8_n_0,
      S(3) => ctr1_carry_i_9_n_0,
      S(2) => ctr1_carry_i_10_n_0,
      S(1) => ctr1_carry_i_11_n_0,
      S(0) => ctr1_carry_i_12_n_0
    );
\ctr1_carry__0\: unisim.vcomponents.CARRY8
     port map (
      CI => ctr1_carry_n_0,
      CI_TOP => '0',
      CO(7) => \NLW_ctr1_carry__0_CO_UNCONNECTED\(7),
      CO(6) => \ctr1_carry__0_n_1\,
      CO(5) => \ctr1_carry__0_n_2\,
      CO(4) => \ctr1_carry__0_n_3\,
      CO(3) => \ctr1_carry__0_n_4\,
      CO(2) => \ctr1_carry__0_n_5\,
      CO(1) => \ctr1_carry__0_n_6\,
      CO(0) => \ctr1_carry__0_n_7\,
      DI(7) => '0',
      DI(6) => \ctr_reg_n_0_[31]\,
      DI(5 downto 0) => B"000000",
      O(7 downto 0) => \NLW_ctr1_carry__0_O_UNCONNECTED\(7 downto 0),
      S(7) => '0',
      S(6) => \ctr1_carry__0_i_1_n_0\,
      S(5) => \ctr1_carry__0_i_2_n_0\,
      S(4) => \ctr1_carry__0_i_3_n_0\,
      S(3) => \ctr1_carry__0_i_4_n_0\,
      S(2) => \ctr1_carry__0_i_5_n_0\,
      S(1) => \ctr1_carry__0_i_6_n_0\,
      S(0) => \ctr1_carry__0_i_7_n_0\
    );
\ctr1_carry__0_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \ctr_reg_n_0_[31]\,
      I1 => \ctr_reg_n_0_[30]\,
      O => \ctr1_carry__0_i_1_n_0\
    );
\ctr1_carry__0_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \ctr_reg_n_0_[29]\,
      I1 => \ctr_reg_n_0_[28]\,
      O => \ctr1_carry__0_i_2_n_0\
    );
\ctr1_carry__0_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \ctr_reg_n_0_[27]\,
      I1 => \ctr_reg_n_0_[26]\,
      O => \ctr1_carry__0_i_3_n_0\
    );
\ctr1_carry__0_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \ctr_reg_n_0_[25]\,
      I1 => \ctr_reg_n_0_[24]\,
      O => \ctr1_carry__0_i_4_n_0\
    );
\ctr1_carry__0_i_5\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \ctr_reg_n_0_[22]\,
      I1 => \ctr_reg_n_0_[23]\,
      O => \ctr1_carry__0_i_5_n_0\
    );
\ctr1_carry__0_i_6\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \ctr_reg_n_0_[21]\,
      I1 => \ctr_reg_n_0_[20]\,
      O => \ctr1_carry__0_i_6_n_0\
    );
\ctr1_carry__0_i_7\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \ctr_reg_n_0_[19]\,
      I1 => \ctr_reg_n_0_[18]\,
      O => \ctr1_carry__0_i_7_n_0\
    );
ctr1_carry_i_1: unisim.vcomponents.LUT2
    generic map(
      INIT => X"7"
    )
        port map (
      I0 => \ctr_reg_n_0_[9]\,
      I1 => \ctr_reg_n_0_[8]\,
      O => ctr1_carry_i_1_n_0
    );
ctr1_carry_i_10: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \ctr_reg_n_0_[6]\,
      I1 => \ctr_reg_n_0_[7]\,
      O => ctr1_carry_i_10_n_0
    );
ctr1_carry_i_11: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \ctr_reg_n_0_[5]\,
      I1 => \ctr_reg_n_0_[4]\,
      O => ctr1_carry_i_11_n_0
    );
ctr1_carry_i_12: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \ctr_reg_n_0_[3]\,
      I1 => \ctr_reg_n_0_[2]\,
      O => ctr1_carry_i_12_n_0
    );
ctr1_carry_i_2: unisim.vcomponents.LUT2
    generic map(
      INIT => X"7"
    )
        port map (
      I0 => \ctr_reg_n_0_[7]\,
      I1 => \ctr_reg_n_0_[6]\,
      O => ctr1_carry_i_2_n_0
    );
ctr1_carry_i_3: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \ctr_reg_n_0_[5]\,
      O => ctr1_carry_i_3_n_0
    );
ctr1_carry_i_4: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \ctr_reg_n_0_[3]\,
      O => ctr1_carry_i_4_n_0
    );
ctr1_carry_i_5: unisim.vcomponents.LUT2
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \ctr_reg_n_0_[17]\,
      I1 => \ctr_reg_n_0_[16]\,
      O => ctr1_carry_i_5_n_0
    );
ctr1_carry_i_6: unisim.vcomponents.LUT2
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \ctr_reg_n_0_[15]\,
      I1 => \ctr_reg_n_0_[14]\,
      O => ctr1_carry_i_6_n_0
    );
ctr1_carry_i_7: unisim.vcomponents.LUT2
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \ctr_reg_n_0_[13]\,
      I1 => \ctr_reg_n_0_[12]\,
      O => ctr1_carry_i_7_n_0
    );
ctr1_carry_i_8: unisim.vcomponents.LUT2
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \ctr_reg_n_0_[11]\,
      I1 => \ctr_reg_n_0_[10]\,
      O => ctr1_carry_i_8_n_0
    );
ctr1_carry_i_9: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \ctr_reg_n_0_[8]\,
      I1 => \ctr_reg_n_0_[9]\,
      O => ctr1_carry_i_9_n_0
    );
\ctr[0]_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \ctr_reg_n_0_[0]\,
      O => \ctr[0]_i_1_n_0\
    );
\ctr[31]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0A0A101500001015"
    )
        port map (
      I0 => state(0),
      I1 => \ctr1_carry__0_n_1\,
      I2 => state(2),
      I3 => p_0_in,
      I4 => state(1),
      I5 => trigger_axi_master_start_i_2_n_0,
      O => \ctr[31]_i_1_n_0\
    );
\ctr[31]_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"43"
    )
        port map (
      I0 => state(2),
      I1 => state(0),
      I2 => state(1),
      O => \ctr[31]_i_2_n_0\
    );
\ctr_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => \ctr[31]_i_2_n_0\,
      D => \ctr[0]_i_1_n_0\,
      Q => \ctr_reg_n_0_[0]\,
      R => \ctr[31]_i_1_n_0\
    );
\ctr_reg[10]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => \ctr[31]_i_2_n_0\,
      D => \ctr_reg[16]_i_1_n_14\,
      Q => \ctr_reg_n_0_[10]\,
      R => \ctr[31]_i_1_n_0\
    );
\ctr_reg[11]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => \ctr[31]_i_2_n_0\,
      D => \ctr_reg[16]_i_1_n_13\,
      Q => \ctr_reg_n_0_[11]\,
      R => \ctr[31]_i_1_n_0\
    );
\ctr_reg[12]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => \ctr[31]_i_2_n_0\,
      D => \ctr_reg[16]_i_1_n_12\,
      Q => \ctr_reg_n_0_[12]\,
      R => \ctr[31]_i_1_n_0\
    );
\ctr_reg[13]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => \ctr[31]_i_2_n_0\,
      D => \ctr_reg[16]_i_1_n_11\,
      Q => \ctr_reg_n_0_[13]\,
      R => \ctr[31]_i_1_n_0\
    );
\ctr_reg[14]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => \ctr[31]_i_2_n_0\,
      D => \ctr_reg[16]_i_1_n_10\,
      Q => \ctr_reg_n_0_[14]\,
      R => \ctr[31]_i_1_n_0\
    );
\ctr_reg[15]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => \ctr[31]_i_2_n_0\,
      D => \ctr_reg[16]_i_1_n_9\,
      Q => \ctr_reg_n_0_[15]\,
      R => \ctr[31]_i_1_n_0\
    );
\ctr_reg[16]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => \ctr[31]_i_2_n_0\,
      D => \ctr_reg[16]_i_1_n_8\,
      Q => \ctr_reg_n_0_[16]\,
      R => \ctr[31]_i_1_n_0\
    );
\ctr_reg[16]_i_1\: unisim.vcomponents.CARRY8
     port map (
      CI => \ctr_reg[8]_i_1_n_0\,
      CI_TOP => '0',
      CO(7) => \ctr_reg[16]_i_1_n_0\,
      CO(6) => \ctr_reg[16]_i_1_n_1\,
      CO(5) => \ctr_reg[16]_i_1_n_2\,
      CO(4) => \ctr_reg[16]_i_1_n_3\,
      CO(3) => \ctr_reg[16]_i_1_n_4\,
      CO(2) => \ctr_reg[16]_i_1_n_5\,
      CO(1) => \ctr_reg[16]_i_1_n_6\,
      CO(0) => \ctr_reg[16]_i_1_n_7\,
      DI(7 downto 0) => B"00000000",
      O(7) => \ctr_reg[16]_i_1_n_8\,
      O(6) => \ctr_reg[16]_i_1_n_9\,
      O(5) => \ctr_reg[16]_i_1_n_10\,
      O(4) => \ctr_reg[16]_i_1_n_11\,
      O(3) => \ctr_reg[16]_i_1_n_12\,
      O(2) => \ctr_reg[16]_i_1_n_13\,
      O(1) => \ctr_reg[16]_i_1_n_14\,
      O(0) => \ctr_reg[16]_i_1_n_15\,
      S(7) => \ctr_reg_n_0_[16]\,
      S(6) => \ctr_reg_n_0_[15]\,
      S(5) => \ctr_reg_n_0_[14]\,
      S(4) => \ctr_reg_n_0_[13]\,
      S(3) => \ctr_reg_n_0_[12]\,
      S(2) => \ctr_reg_n_0_[11]\,
      S(1) => \ctr_reg_n_0_[10]\,
      S(0) => \ctr_reg_n_0_[9]\
    );
\ctr_reg[17]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => \ctr[31]_i_2_n_0\,
      D => \ctr_reg[24]_i_1_n_15\,
      Q => \ctr_reg_n_0_[17]\,
      R => \ctr[31]_i_1_n_0\
    );
\ctr_reg[18]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => \ctr[31]_i_2_n_0\,
      D => \ctr_reg[24]_i_1_n_14\,
      Q => \ctr_reg_n_0_[18]\,
      R => \ctr[31]_i_1_n_0\
    );
\ctr_reg[19]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => \ctr[31]_i_2_n_0\,
      D => \ctr_reg[24]_i_1_n_13\,
      Q => \ctr_reg_n_0_[19]\,
      R => \ctr[31]_i_1_n_0\
    );
\ctr_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => \ctr[31]_i_2_n_0\,
      D => \ctr_reg[8]_i_1_n_15\,
      Q => \ctr_reg_n_0_[1]\,
      R => \ctr[31]_i_1_n_0\
    );
\ctr_reg[20]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => \ctr[31]_i_2_n_0\,
      D => \ctr_reg[24]_i_1_n_12\,
      Q => \ctr_reg_n_0_[20]\,
      R => \ctr[31]_i_1_n_0\
    );
\ctr_reg[21]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => \ctr[31]_i_2_n_0\,
      D => \ctr_reg[24]_i_1_n_11\,
      Q => \ctr_reg_n_0_[21]\,
      R => \ctr[31]_i_1_n_0\
    );
\ctr_reg[22]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => \ctr[31]_i_2_n_0\,
      D => \ctr_reg[24]_i_1_n_10\,
      Q => \ctr_reg_n_0_[22]\,
      R => \ctr[31]_i_1_n_0\
    );
\ctr_reg[23]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => \ctr[31]_i_2_n_0\,
      D => \ctr_reg[24]_i_1_n_9\,
      Q => \ctr_reg_n_0_[23]\,
      R => \ctr[31]_i_1_n_0\
    );
\ctr_reg[24]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => \ctr[31]_i_2_n_0\,
      D => \ctr_reg[24]_i_1_n_8\,
      Q => \ctr_reg_n_0_[24]\,
      R => \ctr[31]_i_1_n_0\
    );
\ctr_reg[24]_i_1\: unisim.vcomponents.CARRY8
     port map (
      CI => \ctr_reg[16]_i_1_n_0\,
      CI_TOP => '0',
      CO(7) => \ctr_reg[24]_i_1_n_0\,
      CO(6) => \ctr_reg[24]_i_1_n_1\,
      CO(5) => \ctr_reg[24]_i_1_n_2\,
      CO(4) => \ctr_reg[24]_i_1_n_3\,
      CO(3) => \ctr_reg[24]_i_1_n_4\,
      CO(2) => \ctr_reg[24]_i_1_n_5\,
      CO(1) => \ctr_reg[24]_i_1_n_6\,
      CO(0) => \ctr_reg[24]_i_1_n_7\,
      DI(7 downto 0) => B"00000000",
      O(7) => \ctr_reg[24]_i_1_n_8\,
      O(6) => \ctr_reg[24]_i_1_n_9\,
      O(5) => \ctr_reg[24]_i_1_n_10\,
      O(4) => \ctr_reg[24]_i_1_n_11\,
      O(3) => \ctr_reg[24]_i_1_n_12\,
      O(2) => \ctr_reg[24]_i_1_n_13\,
      O(1) => \ctr_reg[24]_i_1_n_14\,
      O(0) => \ctr_reg[24]_i_1_n_15\,
      S(7) => \ctr_reg_n_0_[24]\,
      S(6) => \ctr_reg_n_0_[23]\,
      S(5) => \ctr_reg_n_0_[22]\,
      S(4) => \ctr_reg_n_0_[21]\,
      S(3) => \ctr_reg_n_0_[20]\,
      S(2) => \ctr_reg_n_0_[19]\,
      S(1) => \ctr_reg_n_0_[18]\,
      S(0) => \ctr_reg_n_0_[17]\
    );
\ctr_reg[25]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => \ctr[31]_i_2_n_0\,
      D => \ctr_reg[31]_i_3_n_15\,
      Q => \ctr_reg_n_0_[25]\,
      R => \ctr[31]_i_1_n_0\
    );
\ctr_reg[26]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => \ctr[31]_i_2_n_0\,
      D => \ctr_reg[31]_i_3_n_14\,
      Q => \ctr_reg_n_0_[26]\,
      R => \ctr[31]_i_1_n_0\
    );
\ctr_reg[27]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => \ctr[31]_i_2_n_0\,
      D => \ctr_reg[31]_i_3_n_13\,
      Q => \ctr_reg_n_0_[27]\,
      R => \ctr[31]_i_1_n_0\
    );
\ctr_reg[28]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => \ctr[31]_i_2_n_0\,
      D => \ctr_reg[31]_i_3_n_12\,
      Q => \ctr_reg_n_0_[28]\,
      R => \ctr[31]_i_1_n_0\
    );
\ctr_reg[29]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => \ctr[31]_i_2_n_0\,
      D => \ctr_reg[31]_i_3_n_11\,
      Q => \ctr_reg_n_0_[29]\,
      R => \ctr[31]_i_1_n_0\
    );
\ctr_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => \ctr[31]_i_2_n_0\,
      D => \ctr_reg[8]_i_1_n_14\,
      Q => \ctr_reg_n_0_[2]\,
      R => \ctr[31]_i_1_n_0\
    );
\ctr_reg[30]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => \ctr[31]_i_2_n_0\,
      D => \ctr_reg[31]_i_3_n_10\,
      Q => \ctr_reg_n_0_[30]\,
      R => \ctr[31]_i_1_n_0\
    );
\ctr_reg[31]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => \ctr[31]_i_2_n_0\,
      D => \ctr_reg[31]_i_3_n_9\,
      Q => \ctr_reg_n_0_[31]\,
      R => \ctr[31]_i_1_n_0\
    );
\ctr_reg[31]_i_3\: unisim.vcomponents.CARRY8
     port map (
      CI => \ctr_reg[24]_i_1_n_0\,
      CI_TOP => '0',
      CO(7 downto 6) => \NLW_ctr_reg[31]_i_3_CO_UNCONNECTED\(7 downto 6),
      CO(5) => \ctr_reg[31]_i_3_n_2\,
      CO(4) => \ctr_reg[31]_i_3_n_3\,
      CO(3) => \ctr_reg[31]_i_3_n_4\,
      CO(2) => \ctr_reg[31]_i_3_n_5\,
      CO(1) => \ctr_reg[31]_i_3_n_6\,
      CO(0) => \ctr_reg[31]_i_3_n_7\,
      DI(7 downto 0) => B"00000000",
      O(7) => \NLW_ctr_reg[31]_i_3_O_UNCONNECTED\(7),
      O(6) => \ctr_reg[31]_i_3_n_9\,
      O(5) => \ctr_reg[31]_i_3_n_10\,
      O(4) => \ctr_reg[31]_i_3_n_11\,
      O(3) => \ctr_reg[31]_i_3_n_12\,
      O(2) => \ctr_reg[31]_i_3_n_13\,
      O(1) => \ctr_reg[31]_i_3_n_14\,
      O(0) => \ctr_reg[31]_i_3_n_15\,
      S(7) => '0',
      S(6) => \ctr_reg_n_0_[31]\,
      S(5) => \ctr_reg_n_0_[30]\,
      S(4) => \ctr_reg_n_0_[29]\,
      S(3) => \ctr_reg_n_0_[28]\,
      S(2) => \ctr_reg_n_0_[27]\,
      S(1) => \ctr_reg_n_0_[26]\,
      S(0) => \ctr_reg_n_0_[25]\
    );
\ctr_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => \ctr[31]_i_2_n_0\,
      D => \ctr_reg[8]_i_1_n_13\,
      Q => \ctr_reg_n_0_[3]\,
      R => \ctr[31]_i_1_n_0\
    );
\ctr_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => \ctr[31]_i_2_n_0\,
      D => \ctr_reg[8]_i_1_n_12\,
      Q => \ctr_reg_n_0_[4]\,
      R => \ctr[31]_i_1_n_0\
    );
\ctr_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => \ctr[31]_i_2_n_0\,
      D => \ctr_reg[8]_i_1_n_11\,
      Q => \ctr_reg_n_0_[5]\,
      R => \ctr[31]_i_1_n_0\
    );
\ctr_reg[6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => \ctr[31]_i_2_n_0\,
      D => \ctr_reg[8]_i_1_n_10\,
      Q => \ctr_reg_n_0_[6]\,
      R => \ctr[31]_i_1_n_0\
    );
\ctr_reg[7]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => \ctr[31]_i_2_n_0\,
      D => \ctr_reg[8]_i_1_n_9\,
      Q => \ctr_reg_n_0_[7]\,
      R => \ctr[31]_i_1_n_0\
    );
\ctr_reg[8]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => \ctr[31]_i_2_n_0\,
      D => \ctr_reg[8]_i_1_n_8\,
      Q => \ctr_reg_n_0_[8]\,
      R => \ctr[31]_i_1_n_0\
    );
\ctr_reg[8]_i_1\: unisim.vcomponents.CARRY8
     port map (
      CI => \ctr_reg_n_0_[0]\,
      CI_TOP => '0',
      CO(7) => \ctr_reg[8]_i_1_n_0\,
      CO(6) => \ctr_reg[8]_i_1_n_1\,
      CO(5) => \ctr_reg[8]_i_1_n_2\,
      CO(4) => \ctr_reg[8]_i_1_n_3\,
      CO(3) => \ctr_reg[8]_i_1_n_4\,
      CO(2) => \ctr_reg[8]_i_1_n_5\,
      CO(1) => \ctr_reg[8]_i_1_n_6\,
      CO(0) => \ctr_reg[8]_i_1_n_7\,
      DI(7 downto 0) => B"00000000",
      O(7) => \ctr_reg[8]_i_1_n_8\,
      O(6) => \ctr_reg[8]_i_1_n_9\,
      O(5) => \ctr_reg[8]_i_1_n_10\,
      O(4) => \ctr_reg[8]_i_1_n_11\,
      O(3) => \ctr_reg[8]_i_1_n_12\,
      O(2) => \ctr_reg[8]_i_1_n_13\,
      O(1) => \ctr_reg[8]_i_1_n_14\,
      O(0) => \ctr_reg[8]_i_1_n_15\,
      S(7) => \ctr_reg_n_0_[8]\,
      S(6) => \ctr_reg_n_0_[7]\,
      S(5) => \ctr_reg_n_0_[6]\,
      S(4) => \ctr_reg_n_0_[5]\,
      S(3) => \ctr_reg_n_0_[4]\,
      S(2) => \ctr_reg_n_0_[3]\,
      S(1) => \ctr_reg_n_0_[2]\,
      S(0) => \ctr_reg_n_0_[1]\
    );
\ctr_reg[9]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => \ctr[31]_i_2_n_0\,
      D => \ctr_reg[16]_i_1_n_15\,
      Q => \ctr_reg_n_0_[9]\,
      R => \ctr[31]_i_1_n_0\
    );
\output_address_tmp[31]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"4000"
    )
        port map (
      I0 => state(2),
      I1 => state(1),
      I2 => state(0),
      I3 => trigger_axi_master_start_i_2_n_0,
      O => \state__0\(2)
    );
\output_address_tmp[7]_i_2\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \^output_address\(0),
      O => \output_address_tmp[7]_i_2_n_0\
    );
\output_address_tmp_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => \state__0\(2),
      D => \output_address_tmp_reg[7]_i_1_n_15\,
      Q => \^output_address\(0),
      R => '0'
    );
\output_address_tmp_reg[10]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => \state__0\(2),
      D => \output_address_tmp_reg[15]_i_1_n_13\,
      Q => \^output_address\(10),
      R => '0'
    );
\output_address_tmp_reg[11]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => \state__0\(2),
      D => \output_address_tmp_reg[15]_i_1_n_12\,
      Q => \^output_address\(11),
      R => '0'
    );
\output_address_tmp_reg[12]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => \state__0\(2),
      D => \output_address_tmp_reg[15]_i_1_n_11\,
      Q => \^output_address\(12),
      R => '0'
    );
\output_address_tmp_reg[13]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => \state__0\(2),
      D => \output_address_tmp_reg[15]_i_1_n_10\,
      Q => \^output_address\(13),
      R => '0'
    );
\output_address_tmp_reg[14]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => \state__0\(2),
      D => \output_address_tmp_reg[15]_i_1_n_9\,
      Q => \^output_address\(14),
      R => '0'
    );
\output_address_tmp_reg[15]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => \state__0\(2),
      D => \output_address_tmp_reg[15]_i_1_n_8\,
      Q => \^output_address\(15),
      R => '0'
    );
\output_address_tmp_reg[15]_i_1\: unisim.vcomponents.CARRY8
     port map (
      CI => \output_address_tmp_reg[7]_i_1_n_0\,
      CI_TOP => '0',
      CO(7) => \output_address_tmp_reg[15]_i_1_n_0\,
      CO(6) => \output_address_tmp_reg[15]_i_1_n_1\,
      CO(5) => \output_address_tmp_reg[15]_i_1_n_2\,
      CO(4) => \output_address_tmp_reg[15]_i_1_n_3\,
      CO(3) => \output_address_tmp_reg[15]_i_1_n_4\,
      CO(2) => \output_address_tmp_reg[15]_i_1_n_5\,
      CO(1) => \output_address_tmp_reg[15]_i_1_n_6\,
      CO(0) => \output_address_tmp_reg[15]_i_1_n_7\,
      DI(7 downto 0) => B"00000000",
      O(7) => \output_address_tmp_reg[15]_i_1_n_8\,
      O(6) => \output_address_tmp_reg[15]_i_1_n_9\,
      O(5) => \output_address_tmp_reg[15]_i_1_n_10\,
      O(4) => \output_address_tmp_reg[15]_i_1_n_11\,
      O(3) => \output_address_tmp_reg[15]_i_1_n_12\,
      O(2) => \output_address_tmp_reg[15]_i_1_n_13\,
      O(1) => \output_address_tmp_reg[15]_i_1_n_14\,
      O(0) => \output_address_tmp_reg[15]_i_1_n_15\,
      S(7 downto 0) => \^output_address\(15 downto 8)
    );
\output_address_tmp_reg[16]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => \state__0\(2),
      D => \output_address_tmp_reg[23]_i_1_n_15\,
      Q => \^output_address\(16),
      R => '0'
    );
\output_address_tmp_reg[17]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => \state__0\(2),
      D => \output_address_tmp_reg[23]_i_1_n_14\,
      Q => \^output_address\(17),
      R => '0'
    );
\output_address_tmp_reg[18]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => \state__0\(2),
      D => \output_address_tmp_reg[23]_i_1_n_13\,
      Q => \^output_address\(18),
      R => '0'
    );
\output_address_tmp_reg[19]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => \state__0\(2),
      D => \output_address_tmp_reg[23]_i_1_n_12\,
      Q => \^output_address\(19),
      R => '0'
    );
\output_address_tmp_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => \state__0\(2),
      D => \output_address_tmp_reg[7]_i_1_n_14\,
      Q => \^output_address\(1),
      R => '0'
    );
\output_address_tmp_reg[20]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => \state__0\(2),
      D => \output_address_tmp_reg[23]_i_1_n_11\,
      Q => \^output_address\(20),
      R => '0'
    );
\output_address_tmp_reg[21]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => \state__0\(2),
      D => \output_address_tmp_reg[23]_i_1_n_10\,
      Q => \^output_address\(21),
      R => '0'
    );
\output_address_tmp_reg[22]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => \state__0\(2),
      D => \output_address_tmp_reg[23]_i_1_n_9\,
      Q => \^output_address\(22),
      R => '0'
    );
\output_address_tmp_reg[23]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => \state__0\(2),
      D => \output_address_tmp_reg[23]_i_1_n_8\,
      Q => \^output_address\(23),
      R => '0'
    );
\output_address_tmp_reg[23]_i_1\: unisim.vcomponents.CARRY8
     port map (
      CI => \output_address_tmp_reg[15]_i_1_n_0\,
      CI_TOP => '0',
      CO(7) => \output_address_tmp_reg[23]_i_1_n_0\,
      CO(6) => \output_address_tmp_reg[23]_i_1_n_1\,
      CO(5) => \output_address_tmp_reg[23]_i_1_n_2\,
      CO(4) => \output_address_tmp_reg[23]_i_1_n_3\,
      CO(3) => \output_address_tmp_reg[23]_i_1_n_4\,
      CO(2) => \output_address_tmp_reg[23]_i_1_n_5\,
      CO(1) => \output_address_tmp_reg[23]_i_1_n_6\,
      CO(0) => \output_address_tmp_reg[23]_i_1_n_7\,
      DI(7 downto 0) => B"00000000",
      O(7) => \output_address_tmp_reg[23]_i_1_n_8\,
      O(6) => \output_address_tmp_reg[23]_i_1_n_9\,
      O(5) => \output_address_tmp_reg[23]_i_1_n_10\,
      O(4) => \output_address_tmp_reg[23]_i_1_n_11\,
      O(3) => \output_address_tmp_reg[23]_i_1_n_12\,
      O(2) => \output_address_tmp_reg[23]_i_1_n_13\,
      O(1) => \output_address_tmp_reg[23]_i_1_n_14\,
      O(0) => \output_address_tmp_reg[23]_i_1_n_15\,
      S(7 downto 0) => \^output_address\(23 downto 16)
    );
\output_address_tmp_reg[24]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => \state__0\(2),
      D => \output_address_tmp_reg[31]_i_2_n_15\,
      Q => \^output_address\(24),
      R => '0'
    );
\output_address_tmp_reg[25]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => \state__0\(2),
      D => \output_address_tmp_reg[31]_i_2_n_14\,
      Q => \^output_address\(25),
      R => '0'
    );
\output_address_tmp_reg[26]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => \state__0\(2),
      D => \output_address_tmp_reg[31]_i_2_n_13\,
      Q => \^output_address\(26),
      R => '0'
    );
\output_address_tmp_reg[27]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => \state__0\(2),
      D => \output_address_tmp_reg[31]_i_2_n_12\,
      Q => \^output_address\(27),
      R => '0'
    );
\output_address_tmp_reg[28]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => \state__0\(2),
      D => \output_address_tmp_reg[31]_i_2_n_11\,
      Q => \^output_address\(28),
      R => '0'
    );
\output_address_tmp_reg[29]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => \state__0\(2),
      D => \output_address_tmp_reg[31]_i_2_n_10\,
      Q => \^output_address\(29),
      R => '0'
    );
\output_address_tmp_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => \state__0\(2),
      D => \output_address_tmp_reg[7]_i_1_n_13\,
      Q => \^output_address\(2),
      R => '0'
    );
\output_address_tmp_reg[30]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => \state__0\(2),
      D => \output_address_tmp_reg[31]_i_2_n_9\,
      Q => \^output_address\(30),
      R => '0'
    );
\output_address_tmp_reg[31]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => \state__0\(2),
      D => \output_address_tmp_reg[31]_i_2_n_8\,
      Q => \^output_address\(31),
      R => '0'
    );
\output_address_tmp_reg[31]_i_2\: unisim.vcomponents.CARRY8
     port map (
      CI => \output_address_tmp_reg[23]_i_1_n_0\,
      CI_TOP => '0',
      CO(7) => \NLW_output_address_tmp_reg[31]_i_2_CO_UNCONNECTED\(7),
      CO(6) => \output_address_tmp_reg[31]_i_2_n_1\,
      CO(5) => \output_address_tmp_reg[31]_i_2_n_2\,
      CO(4) => \output_address_tmp_reg[31]_i_2_n_3\,
      CO(3) => \output_address_tmp_reg[31]_i_2_n_4\,
      CO(2) => \output_address_tmp_reg[31]_i_2_n_5\,
      CO(1) => \output_address_tmp_reg[31]_i_2_n_6\,
      CO(0) => \output_address_tmp_reg[31]_i_2_n_7\,
      DI(7 downto 0) => B"00000000",
      O(7) => \output_address_tmp_reg[31]_i_2_n_8\,
      O(6) => \output_address_tmp_reg[31]_i_2_n_9\,
      O(5) => \output_address_tmp_reg[31]_i_2_n_10\,
      O(4) => \output_address_tmp_reg[31]_i_2_n_11\,
      O(3) => \output_address_tmp_reg[31]_i_2_n_12\,
      O(2) => \output_address_tmp_reg[31]_i_2_n_13\,
      O(1) => \output_address_tmp_reg[31]_i_2_n_14\,
      O(0) => \output_address_tmp_reg[31]_i_2_n_15\,
      S(7 downto 0) => \^output_address\(31 downto 24)
    );
\output_address_tmp_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => \state__0\(2),
      D => \output_address_tmp_reg[7]_i_1_n_12\,
      Q => \^output_address\(3),
      R => '0'
    );
\output_address_tmp_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => \state__0\(2),
      D => \output_address_tmp_reg[7]_i_1_n_11\,
      Q => \^output_address\(4),
      R => '0'
    );
\output_address_tmp_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => \state__0\(2),
      D => \output_address_tmp_reg[7]_i_1_n_10\,
      Q => \^output_address\(5),
      R => '0'
    );
\output_address_tmp_reg[6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => \state__0\(2),
      D => \output_address_tmp_reg[7]_i_1_n_9\,
      Q => \^output_address\(6),
      R => '0'
    );
\output_address_tmp_reg[7]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => \state__0\(2),
      D => \output_address_tmp_reg[7]_i_1_n_8\,
      Q => \^output_address\(7),
      R => '0'
    );
\output_address_tmp_reg[7]_i_1\: unisim.vcomponents.CARRY8
     port map (
      CI => '0',
      CI_TOP => '0',
      CO(7) => \output_address_tmp_reg[7]_i_1_n_0\,
      CO(6) => \output_address_tmp_reg[7]_i_1_n_1\,
      CO(5) => \output_address_tmp_reg[7]_i_1_n_2\,
      CO(4) => \output_address_tmp_reg[7]_i_1_n_3\,
      CO(3) => \output_address_tmp_reg[7]_i_1_n_4\,
      CO(2) => \output_address_tmp_reg[7]_i_1_n_5\,
      CO(1) => \output_address_tmp_reg[7]_i_1_n_6\,
      CO(0) => \output_address_tmp_reg[7]_i_1_n_7\,
      DI(7 downto 0) => B"00000001",
      O(7) => \output_address_tmp_reg[7]_i_1_n_8\,
      O(6) => \output_address_tmp_reg[7]_i_1_n_9\,
      O(5) => \output_address_tmp_reg[7]_i_1_n_10\,
      O(4) => \output_address_tmp_reg[7]_i_1_n_11\,
      O(3) => \output_address_tmp_reg[7]_i_1_n_12\,
      O(2) => \output_address_tmp_reg[7]_i_1_n_13\,
      O(1) => \output_address_tmp_reg[7]_i_1_n_14\,
      O(0) => \output_address_tmp_reg[7]_i_1_n_15\,
      S(7 downto 1) => \^output_address\(7 downto 1),
      S(0) => \output_address_tmp[7]_i_2_n_0\
    );
\output_address_tmp_reg[8]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => \state__0\(2),
      D => \output_address_tmp_reg[15]_i_1_n_15\,
      Q => \^output_address\(8),
      R => '0'
    );
\output_address_tmp_reg[9]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => \state__0\(2),
      D => \output_address_tmp_reg[15]_i_1_n_14\,
      Q => \^output_address\(9),
      R => '0'
    );
output_data_tmp0_carry: unisim.vcomponents.CARRY8
     port map (
      CI => \^output_data\(0),
      CI_TOP => '0',
      CO(7) => output_data_tmp0_carry_n_0,
      CO(6) => output_data_tmp0_carry_n_1,
      CO(5) => output_data_tmp0_carry_n_2,
      CO(4) => output_data_tmp0_carry_n_3,
      CO(3) => output_data_tmp0_carry_n_4,
      CO(2) => output_data_tmp0_carry_n_5,
      CO(1) => output_data_tmp0_carry_n_6,
      CO(0) => output_data_tmp0_carry_n_7,
      DI(7 downto 0) => \^output_data\(8 downto 1),
      O(7 downto 0) => output_data_tmp0(8 downto 1),
      S(7) => output_data_tmp0_carry_i_1_n_0,
      S(6) => output_data_tmp0_carry_i_2_n_0,
      S(5) => output_data_tmp0_carry_i_3_n_0,
      S(4) => output_data_tmp0_carry_i_4_n_0,
      S(3) => output_data_tmp0_carry_i_5_n_0,
      S(2) => output_data_tmp0_carry_i_6_n_0,
      S(1) => output_data_tmp0_carry_i_7_n_0,
      S(0) => output_data_tmp0_carry_i_8_n_0
    );
\output_data_tmp0_carry__0\: unisim.vcomponents.CARRY8
     port map (
      CI => output_data_tmp0_carry_n_0,
      CI_TOP => '0',
      CO(7 downto 6) => \NLW_output_data_tmp0_carry__0_CO_UNCONNECTED\(7 downto 6),
      CO(5) => \output_data_tmp0_carry__0_n_2\,
      CO(4) => \output_data_tmp0_carry__0_n_3\,
      CO(3) => \output_data_tmp0_carry__0_n_4\,
      CO(2) => \output_data_tmp0_carry__0_n_5\,
      CO(1) => \output_data_tmp0_carry__0_n_6\,
      CO(0) => \output_data_tmp0_carry__0_n_7\,
      DI(7 downto 6) => B"00",
      DI(5 downto 0) => \^output_data\(14 downto 9),
      O(7) => \NLW_output_data_tmp0_carry__0_O_UNCONNECTED\(7),
      O(6 downto 0) => output_data_tmp0(15 downto 9),
      S(7) => '0',
      S(6) => \output_data_tmp0_carry__0_i_1_n_0\,
      S(5) => \output_data_tmp0_carry__0_i_2_n_0\,
      S(4) => \output_data_tmp0_carry__0_i_3_n_0\,
      S(3) => \output_data_tmp0_carry__0_i_4_n_0\,
      S(2) => \output_data_tmp0_carry__0_i_5_n_0\,
      S(1) => \output_data_tmp0_carry__0_i_6_n_0\,
      S(0) => \output_data_tmp0_carry__0_i_7_n_0\
    );
\output_data_tmp0_carry__0_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \^output_data\(15),
      O => \output_data_tmp0_carry__0_i_1_n_0\
    );
\output_data_tmp0_carry__0_i_2\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \^output_data\(14),
      O => \output_data_tmp0_carry__0_i_2_n_0\
    );
\output_data_tmp0_carry__0_i_3\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \^output_data\(13),
      O => \output_data_tmp0_carry__0_i_3_n_0\
    );
\output_data_tmp0_carry__0_i_4\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \^output_data\(12),
      O => \output_data_tmp0_carry__0_i_4_n_0\
    );
\output_data_tmp0_carry__0_i_5\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \^output_data\(11),
      O => \output_data_tmp0_carry__0_i_5_n_0\
    );
\output_data_tmp0_carry__0_i_6\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \^output_data\(10),
      O => \output_data_tmp0_carry__0_i_6_n_0\
    );
\output_data_tmp0_carry__0_i_7\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \^output_data\(9),
      O => \output_data_tmp0_carry__0_i_7_n_0\
    );
output_data_tmp0_carry_i_1: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \^output_data\(8),
      O => output_data_tmp0_carry_i_1_n_0
    );
output_data_tmp0_carry_i_2: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \^output_data\(7),
      O => output_data_tmp0_carry_i_2_n_0
    );
output_data_tmp0_carry_i_3: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \^output_data\(6),
      O => output_data_tmp0_carry_i_3_n_0
    );
output_data_tmp0_carry_i_4: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \^output_data\(5),
      O => output_data_tmp0_carry_i_4_n_0
    );
output_data_tmp0_carry_i_5: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \^output_data\(4),
      O => output_data_tmp0_carry_i_5_n_0
    );
output_data_tmp0_carry_i_6: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \^output_data\(3),
      O => output_data_tmp0_carry_i_6_n_0
    );
output_data_tmp0_carry_i_7: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \^output_data\(2),
      O => output_data_tmp0_carry_i_7_n_0
    );
output_data_tmp0_carry_i_8: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \^output_data\(1),
      O => output_data_tmp0_carry_i_8_n_0
    );
\output_data_tmp[0]_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \^output_data\(0),
      O => output_data_tmp0(0)
    );
\output_data_tmp_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '1'
    )
        port map (
      C => clk,
      CE => \state__0\(2),
      D => output_data_tmp0(0),
      Q => \^output_data\(0),
      R => '0'
    );
\output_data_tmp_reg[10]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '1'
    )
        port map (
      C => clk,
      CE => \state__0\(2),
      D => output_data_tmp0(10),
      Q => \^output_data\(10),
      R => '0'
    );
\output_data_tmp_reg[11]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '1'
    )
        port map (
      C => clk,
      CE => \state__0\(2),
      D => output_data_tmp0(11),
      Q => \^output_data\(11),
      R => '0'
    );
\output_data_tmp_reg[12]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '1'
    )
        port map (
      C => clk,
      CE => \state__0\(2),
      D => output_data_tmp0(12),
      Q => \^output_data\(12),
      R => '0'
    );
\output_data_tmp_reg[13]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '1'
    )
        port map (
      C => clk,
      CE => \state__0\(2),
      D => output_data_tmp0(13),
      Q => \^output_data\(13),
      R => '0'
    );
\output_data_tmp_reg[14]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '1'
    )
        port map (
      C => clk,
      CE => \state__0\(2),
      D => output_data_tmp0(14),
      Q => \^output_data\(14),
      R => '0'
    );
\output_data_tmp_reg[15]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '1'
    )
        port map (
      C => clk,
      CE => \state__0\(2),
      D => output_data_tmp0(15),
      Q => \^output_data\(15),
      R => '0'
    );
\output_data_tmp_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '1'
    )
        port map (
      C => clk,
      CE => \state__0\(2),
      D => output_data_tmp0(1),
      Q => \^output_data\(1),
      R => '0'
    );
\output_data_tmp_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '1'
    )
        port map (
      C => clk,
      CE => \state__0\(2),
      D => output_data_tmp0(2),
      Q => \^output_data\(2),
      R => '0'
    );
\output_data_tmp_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '1'
    )
        port map (
      C => clk,
      CE => \state__0\(2),
      D => output_data_tmp0(3),
      Q => \^output_data\(3),
      R => '0'
    );
\output_data_tmp_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '1'
    )
        port map (
      C => clk,
      CE => \state__0\(2),
      D => output_data_tmp0(4),
      Q => \^output_data\(4),
      R => '0'
    );
\output_data_tmp_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '1'
    )
        port map (
      C => clk,
      CE => \state__0\(2),
      D => output_data_tmp0(5),
      Q => \^output_data\(5),
      R => '0'
    );
\output_data_tmp_reg[6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '1'
    )
        port map (
      C => clk,
      CE => \state__0\(2),
      D => output_data_tmp0(6),
      Q => \^output_data\(6),
      R => '0'
    );
\output_data_tmp_reg[7]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '1'
    )
        port map (
      C => clk,
      CE => \state__0\(2),
      D => output_data_tmp0(7),
      Q => \^output_data\(7),
      R => '0'
    );
\output_data_tmp_reg[8]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '1'
    )
        port map (
      C => clk,
      CE => \state__0\(2),
      D => output_data_tmp0(8),
      Q => \^output_data\(8),
      R => '0'
    );
\output_data_tmp_reg[9]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '1'
    )
        port map (
      C => clk,
      CE => \state__0\(2),
      D => output_data_tmp0(9),
      Q => \^output_data\(9),
      R => '0'
    );
trigger_axi_master_start_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FF38FFF800380038"
    )
        port map (
      I0 => axi_master_done,
      I1 => state(1),
      I2 => state(0),
      I3 => state(2),
      I4 => trigger_axi_master_start_i_2_n_0,
      I5 => \^trigger_axi_master_start\,
      O => trigger_axi_master_start_i_1_n_0
    );
trigger_axi_master_start_i_10: unisim.vcomponents.LUT2
    generic map(
      INIT => X"7"
    )
        port map (
      I0 => \ctr_reg_n_0_[17]\,
      I1 => \ctr_reg_n_0_[16]\,
      O => trigger_axi_master_start_i_10_n_0
    );
trigger_axi_master_start_i_2: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000000100"
    )
        port map (
      I0 => \ctr_reg_n_0_[26]\,
      I1 => \ctr_reg_n_0_[25]\,
      I2 => \ctr_reg_n_0_[24]\,
      I3 => trigger_axi_master_start_i_3_n_0,
      I4 => trigger_axi_master_start_i_4_n_0,
      I5 => trigger_axi_master_start_i_5_n_0,
      O => trigger_axi_master_start_i_2_n_0
    );
trigger_axi_master_start_i_3: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0004"
    )
        port map (
      I0 => \ctr_reg_n_0_[29]\,
      I1 => axi_master_done,
      I2 => \ctr_reg_n_0_[28]\,
      I3 => \ctr_reg_n_0_[27]\,
      O => trigger_axi_master_start_i_3_n_0
    );
trigger_axi_master_start_i_4: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFFFFBF"
    )
        port map (
      I0 => trigger_axi_master_start_i_6_n_0,
      I1 => trigger_axi_master_start_i_7_n_0,
      I2 => trigger_axi_master_start_i_8_n_0,
      I3 => \ctr_reg_n_0_[7]\,
      I4 => \ctr_reg_n_0_[8]\,
      O => trigger_axi_master_start_i_4_n_0
    );
trigger_axi_master_start_i_5: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFEFFFFFFFFFFFF"
    )
        port map (
      I0 => trigger_axi_master_start_i_9_n_0,
      I1 => \ctr_reg_n_0_[20]\,
      I2 => trigger_axi_master_start_i_10_n_0,
      I3 => \ctr_reg_n_0_[21]\,
      I4 => \ctr_reg_n_0_[18]\,
      I5 => \ctr_reg_n_0_[19]\,
      O => trigger_axi_master_start_i_5_n_0
    );
trigger_axi_master_start_i_6: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFEF"
    )
        port map (
      I0 => \ctr_reg_n_0_[12]\,
      I1 => \ctr_reg_n_0_[15]\,
      I2 => \ctr_reg_n_0_[14]\,
      I3 => \ctr_reg_n_0_[13]\,
      O => trigger_axi_master_start_i_6_n_0
    );
trigger_axi_master_start_i_7: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0001"
    )
        port map (
      I0 => \ctr_reg_n_0_[31]\,
      I1 => \ctr_reg_n_0_[30]\,
      I2 => \ctr_reg_n_0_[5]\,
      I3 => \ctr_reg_n_0_[4]\,
      O => trigger_axi_master_start_i_7_n_0
    );
trigger_axi_master_start_i_8: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0001"
    )
        port map (
      I0 => \ctr_reg_n_0_[1]\,
      I1 => \ctr_reg_n_0_[0]\,
      I2 => \ctr_reg_n_0_[3]\,
      I3 => \ctr_reg_n_0_[2]\,
      O => trigger_axi_master_start_i_8_n_0
    );
trigger_axi_master_start_i_9: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFBFF"
    )
        port map (
      I0 => \ctr_reg_n_0_[10]\,
      I1 => \ctr_reg_n_0_[6]\,
      I2 => \ctr_reg_n_0_[11]\,
      I3 => \ctr_reg_n_0_[9]\,
      I4 => \ctr_reg_n_0_[22]\,
      I5 => \ctr_reg_n_0_[23]\,
      O => trigger_axi_master_start_i_9_n_0
    );
trigger_axi_master_start_reg: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => trigger_axi_master_start_i_1_n_0,
      Q => \^trigger_axi_master_start\,
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity udp_stack_ps_pl_trigger_0_0 is
  port (
    clk : in STD_LOGIC;
    axi_master_done : in STD_LOGIC;
    trigger_axi_master_start : out STD_LOGIC;
    output_address : out STD_LOGIC_VECTOR ( 31 downto 0 );
    output_data : out STD_LOGIC_VECTOR ( 15 downto 0 )
  );
  attribute NotValidForBitStream : boolean;
  attribute NotValidForBitStream of udp_stack_ps_pl_trigger_0_0 : entity is true;
  attribute CHECK_LICENSE_TYPE : string;
  attribute CHECK_LICENSE_TYPE of udp_stack_ps_pl_trigger_0_0 : entity is "udp_stack_ps_pl_trigger_0_0,ps_pl_trigger,{}";
  attribute DowngradeIPIdentifiedWarnings : string;
  attribute DowngradeIPIdentifiedWarnings of udp_stack_ps_pl_trigger_0_0 : entity is "yes";
  attribute IP_DEFINITION_SOURCE : string;
  attribute IP_DEFINITION_SOURCE of udp_stack_ps_pl_trigger_0_0 : entity is "module_ref";
  attribute X_CORE_INFO : string;
  attribute X_CORE_INFO of udp_stack_ps_pl_trigger_0_0 : entity is "ps_pl_trigger,Vivado 2022.1";
end udp_stack_ps_pl_trigger_0_0;

architecture STRUCTURE of udp_stack_ps_pl_trigger_0_0 is
  attribute X_INTERFACE_INFO : string;
  attribute X_INTERFACE_INFO of clk : signal is "xilinx.com:signal:clock:1.0 clk CLK";
  attribute X_INTERFACE_PARAMETER : string;
  attribute X_INTERFACE_PARAMETER of clk : signal is "XIL_INTERFACENAME clk, FREQ_HZ 99990005, FREQ_TOLERANCE_HZ 0, PHASE 0.0, CLK_DOMAIN udp_stack_zynq_ultra_ps_e_0_0_pl_clk0, INSERT_VIP 0";
begin
inst: entity work.udp_stack_ps_pl_trigger_0_0_ps_pl_trigger
     port map (
      axi_master_done => axi_master_done,
      clk => clk,
      output_address(31 downto 0) => output_address(31 downto 0),
      output_data(15 downto 0) => output_data(15 downto 0),
      trigger_axi_master_start => trigger_axi_master_start
    );
end STRUCTURE;
