`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 02/23/2024 08:43:49 AM
// Design Name: 
// Module Name: command_handler
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////

module command_handler #(parameter C_AXI_Light_Slave_DATA_WIDTH = 32, parameter BUFF_LEN = 12) (
    input wire clk,
    input wire [C_AXI_Light_Slave_DATA_WIDTH-1:0] slv_reg0,
    input wire [C_AXI_Light_Slave_DATA_WIDTH-1:0] slv_reg1,
    input wire [C_AXI_Light_Slave_DATA_WIDTH-1:0] slv_reg2,
    input wire [C_AXI_Light_Slave_DATA_WIDTH-1:0] slv_reg3,
    input wire [3:0] write_map,
    output reg [3:0] read_map,
    
    output wire [7:0]   cmd,
    output wire [7:0]   measure_type,
    output wire [15:0]  init_value,
    output wire [31:0]  start_addr,
    output wire [31:0]  stop_addr, 
    output wire [32:0]  time_value,
    output wire [127:0] auxilary_data,
    output wire [10:0] buffer_ctr_tmp,
    output wire cmd_ready
);

reg [(C_AXI_Light_Slave_DATA_WIDTH*BUFF_LEN)-1:0] command_buffer;
reg [11:0] buffer_ctr = 12'h0;
reg flush_buffer = 1'b0;

reg[7:0] tmp_cmd = 0;


/** 
 * Always block to buffer the input values
 */
always @(posedge clk) begin
    if (flush_cmd == 1'b1 || flush_buffer == 1'b1) begin
        command_buffer <= {C_AXI_Light_Slave_DATA_WIDTH*BUFF_LEN{1'b0}};
        read_map <= 4'b0;
        buffer_ctr <= 12'h0;
        flush_buffer <= 0;
    end 
    else begin
        if (write_map[0] == 1 && read_map[0] == 1'b0) begin
            command_buffer[(buffer_ctr * C_AXI_Light_Slave_DATA_WIDTH * 4) +: C_AXI_Light_Slave_DATA_WIDTH] = slv_reg0;
            read_map[0] <= 1'b1;
        end
        
        if (write_map[1] == 1 && read_map[1] == 1'b0) begin
            command_buffer[(buffer_ctr * C_AXI_Light_Slave_DATA_WIDTH * 4) + C_AXI_Light_Slave_DATA_WIDTH +: C_AXI_Light_Slave_DATA_WIDTH] = slv_reg1;
            read_map[1] <= 1'b1;
        end
        
        if (write_map[2] == 1 && read_map[2] == 1'b0) begin
            command_buffer[(buffer_ctr * C_AXI_Light_Slave_DATA_WIDTH * 4) + (2 * C_AXI_Light_Slave_DATA_WIDTH) +: C_AXI_Light_Slave_DATA_WIDTH] = slv_reg2;
            read_map[2] <= 1'b1;
        end
        
        if (write_map[3] == 1 && read_map[3] == 1'b0) begin
            command_buffer[(buffer_ctr * C_AXI_Light_Slave_DATA_WIDTH * 4) + (3 * C_AXI_Light_Slave_DATA_WIDTH) +: C_AXI_Light_Slave_DATA_WIDTH] = slv_reg3;
            read_map[3] <= 1'b1;
        end
        
        if(buffer_ctr < BUFF_LEN) begin
            if(read_map == 4'b1111) begin
                buffer_ctr <= buffer_ctr + 12'h1;
                read_map <= 4'h0;
             end
             end
         else begin
            flush_buffer <= 1;
         end
    end
end

localparam   CMD_UNDEFINED 		   = 8'hff;
localparam   CMD_IDN 			   = 8'h01;
localparam   CMD_START_MEASUREMENT = 8'h02;
localparam   CMD_STOP_MEASUREMENT  = 8'h03;
localparam   CMD_RETRIEVE_STATUS   = 8'h04;
localparam   CMD_RESET             = 8'h05;


reg[7:0]   tmp_measure_type = 8'h0;
reg[15:0]  tmp_init_value   = 16'h0;
reg[31:0]  tmp_start_addr   = 32'h0;
reg[31:0]  tmp_stop_addr    = 32'h0;  
reg[32:0]  tmp_time_value   = 32'h0;
reg[127:0] tmp_auxilary_data = 32'h0;


reg flush_cmd = 1'b0;

always @ (posedge clk) begin
    if (buffer_ctr == 12'h1) begin
        tmp_cmd  <= command_buffer[0+:8];
    end 
    else if (buffer_ctr == 12'h2)
    begin
        if (read_map == 4'b1111) begin 
            if(tmp_cmd  == CMD_START_MEASUREMENT) begin
                tmp_measure_type <= command_buffer[8+:8]; // Identifies the measure type, e.g. read- or write-latency measurement
                tmp_init_value <= command_buffer[16+:16];
                tmp_start_addr <= command_buffer[32+:32];
                tmp_stop_addr <= command_buffer[64+:32];
                tmp_time_value <= command_buffer[96+:32];
                tmp_auxilary_data <= command_buffer[128+:128];
                flush_cmd <= 1'b1;
            end
            else
                tmp_measure_type    <= 8'h0;
                tmp_init_value      <= 16'h0;
                tmp_start_addr      <= 32'h0;
                tmp_stop_addr       <= 32'h0;  
                tmp_time_value      <= 32'h0;
                tmp_auxilary_data   <= 32'h0;
                flush_cmd <= 1'b0;
        end
    end
    else if (buffer_ctr > 12'h2) begin 
                tmp_measure_type    <= 8'hff;
                tmp_init_value      <= 16'hffff;
                tmp_start_addr      <= 32'hffffffff;
                tmp_stop_addr       <= 32'hffffffff;  
                tmp_time_value      <= 32'hffffffff;
                tmp_auxilary_data   <= 32'hffffffff;
    end
end

assign measure_type = tmp_measure_type;
assign init_value = tmp_init_value;
assign start_addr = tmp_start_addr;
assign stop_addr = tmp_stop_addr;  
assign time_value = tmp_time_value;
assign auxilary_data = tmp_auxilary_data;
assign cmd = tmp_cmd;
assign buffer_ctr_tmp = buffer_ctr;
assign cmd_ready = flush_cmd;

endmodule
