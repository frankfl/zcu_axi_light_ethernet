
`timescale 1 ns / 1 ps

	module ps_pl_interface_v1_0 #
	(
		// Users to add parameters here

		// User parameters ends
		// Do not modify the parameters beyond this line


		// Parameters of Axi Slave Bus Interface AXI_Light_Slave
		parameter integer C_AXI_Light_Slave_DATA_WIDTH	= 32,
		parameter integer C_AXI_Light_Slave_ADDR_WIDTH	= 4,

		// Parameters of Axi Master Bus Interface AXI_Light_Master
		parameter  C_AXI_Light_Master_START_DATA_VALUE	= 32'hAA000000,
		parameter  C_AXI_Light_Master_TARGET_SLAVE_BASE_ADDR	= 32'h40000000,
		parameter integer C_AXI_Light_Master_ADDR_WIDTH	= 32,
		parameter integer C_AXI_Light_Master_DATA_WIDTH	= 32,
		parameter integer C_AXI_Light_Master_TRANSACTIONS_NUM	= 4
	)
	(
		// Users to add ports here

		// User ports ends
		// Do not modify the ports beyond this line


		// Ports of Axi Slave Bus Interface AXI_Light_Slave
		input wire  axi_light_slave_aclk,
		input wire  axi_light_slave_aresetn,
		input wire [C_AXI_Light_Slave_ADDR_WIDTH-1 : 0] axi_light_slave_awaddr,
		input wire [2 : 0] axi_light_slave_awprot,
		input wire  axi_light_slave_awvalid,
		output wire  axi_light_slave_awready,
		input wire [C_AXI_Light_Slave_DATA_WIDTH-1 : 0] axi_light_slave_wdata,
		input wire [(C_AXI_Light_Slave_DATA_WIDTH/8)-1 : 0] axi_light_slave_wstrb,
		input wire  axi_light_slave_wvalid,
		output wire  axi_light_slave_wready,
		output wire [1 : 0] axi_light_slave_bresp,
		output wire  axi_light_slave_bvalid,
		input wire  axi_light_slave_bready,
		input wire [C_AXI_Light_Slave_ADDR_WIDTH-1 : 0] axi_light_slave_araddr,
		input wire [2 : 0] axi_light_slave_arprot,
		input wire  axi_light_slave_arvalid,
		output wire  axi_light_slave_arready,
		output wire [C_AXI_Light_Slave_DATA_WIDTH-1 : 0] axi_light_slave_rdata,
		output wire [1 : 0] axi_light_slave_rresp,
		output wire  axi_light_slave_rvalid,
		input wire  axi_light_slave_rready,

		// Ports of Axi Master Bus Interface AXI_Light_Master
		input wire  axi_light_master_init_axi_txn,
		output wire  axi_light_master_error,
		output wire  axi_light_master_txn_done,
		input wire  axi_light_master_aclk,
		input wire  axi_light_master_aresetn,
		output wire [C_AXI_Light_Master_ADDR_WIDTH-1 : 0] axi_light_master_awaddr,
		output wire [2 : 0] axi_light_master_awprot,
		output wire  axi_light_master_awvalid,
		input wire  axi_light_master_awready,
		output wire [C_AXI_Light_Master_DATA_WIDTH-1 : 0] axi_light_master_wdata,
		output wire [C_AXI_Light_Master_DATA_WIDTH/8-1 : 0] axi_light_master_wstrb,
		output wire  axi_light_master_wvalid,
		input wire  axi_light_master_wready,
		input wire [1 : 0] axi_light_master_bresp,
		input wire  axi_light_master_bvalid,
		output wire  axi_light_master_bready,
		output wire [C_AXI_Light_Master_ADDR_WIDTH-1 : 0] axi_light_master_araddr,
		output wire [2 : 0] axi_light_master_arprot,
		output wire  axi_light_master_arvalid,
		input wire  axi_light_master_arready,
		input wire [C_AXI_Light_Master_DATA_WIDTH-1 : 0] axi_light_master_rdata,
		input wire [1 : 0] axi_light_master_rresp,
		input wire  axi_light_master_rvalid,
		output wire  axi_light_master_rready,

		        output wire led0,
        output wire led1,
        output wire led2,
        output wire led3
	);


		assign led0 = 1;
	assign led1 = 0;
	assign led2 = 1;
	assign led3 = 0;

// Instantiation of Axi Bus Interface AXI_Light_Slave
	ps_pl_interface_v1_0_AXI_Light_Slave # (
		.C_S_AXI_DATA_WIDTH(C_AXI_Light_Slave_DATA_WIDTH),
		.C_S_AXI_ADDR_WIDTH(C_AXI_Light_Slave_ADDR_WIDTH)
	) ps_pl_interface_v1_0_AXI_Light_Slave_inst (
		.S_AXI_ACLK(axi_light_slave_aclk),
		.S_AXI_ARESETN(axi_light_slave_aresetn),
		.S_AXI_AWADDR(axi_light_slave_awaddr),
		.S_AXI_AWPROT(axi_light_slave_awprot),
		.S_AXI_AWVALID(axi_light_slave_awvalid),
		.S_AXI_AWREADY(axi_light_slave_awready),
		.S_AXI_WDATA(axi_light_slave_wdata),
		.S_AXI_WSTRB(axi_light_slave_wstrb),
		.S_AXI_WVALID(axi_light_slave_wvalid),
		.S_AXI_WREADY(axi_light_slave_wready),
		.S_AXI_BRESP(axi_light_slave_bresp),
		.S_AXI_BVALID(axi_light_slave_bvalid),
		.S_AXI_BREADY(axi_light_slave_bready),
		.S_AXI_ARADDR(axi_light_slave_araddr),
		.S_AXI_ARPROT(axi_light_slave_arprot),
		.S_AXI_ARVALID(axi_light_slave_arvalid),
		.S_AXI_ARREADY(axi_light_slave_arready),
		.S_AXI_RDATA(axi_light_slave_rdata),
		.S_AXI_RRESP(axi_light_slave_rresp),
		.S_AXI_RVALID(axi_light_slave_rvalid),
		.S_AXI_RREADY(axi_light_slave_rready)
	);

// Instantiation of Axi Bus Interface AXI_Light_Master
	ps_pl_interface_v1_0_AXI_Light_Master # (
		.C_M_START_DATA_VALUE(C_AXI_Light_Master_START_DATA_VALUE),
		.C_M_TARGET_SLAVE_BASE_ADDR(C_AXI_Light_Master_TARGET_SLAVE_BASE_ADDR),
		.C_M_AXI_ADDR_WIDTH(C_AXI_Light_Master_ADDR_WIDTH),
		.C_M_AXI_DATA_WIDTH(C_AXI_Light_Master_DATA_WIDTH),
		.C_M_TRANSACTIONS_NUM(C_AXI_Light_Master_TRANSACTIONS_NUM)
	) ps_pl_interface_v1_0_AXI_Light_Master_inst (
		.INIT_AXI_TXN(axi_light_master_init_axi_txn),
		.ERROR(axi_light_master_error),
		.TXN_DONE(axi_light_master_txn_done),
		.M_AXI_ACLK(axi_light_master_aclk),
		.M_AXI_ARESETN(axi_light_master_aresetn),
		.M_AXI_AWADDR(axi_light_master_awaddr),
		.M_AXI_AWPROT(axi_light_master_awprot),
		.M_AXI_AWVALID(axi_light_master_awvalid),
		.M_AXI_AWREADY(axi_light_master_awready),
		.M_AXI_WDATA(axi_light_master_wdata),
		.M_AXI_WSTRB(axi_light_master_wstrb),
		.M_AXI_WVALID(axi_light_master_wvalid),
		.M_AXI_WREADY(axi_light_master_wready),
		.M_AXI_BRESP(axi_light_master_bresp),
		.M_AXI_BVALID(axi_light_master_bvalid),
		.M_AXI_BREADY(axi_light_master_bready),
		.M_AXI_ARADDR(axi_light_master_araddr),
		.M_AXI_ARPROT(axi_light_master_arprot),
		.M_AXI_ARVALID(axi_light_master_arvalid),
		.M_AXI_ARREADY(axi_light_master_arready),
		.M_AXI_RDATA(axi_light_master_rdata),
		.M_AXI_RRESP(axi_light_master_rresp),
		.M_AXI_RVALID(axi_light_master_rvalid),
		.M_AXI_RREADY(axi_light_master_rready)
	);

        wire clk1;
        wire rst1;
        wire [1:0] Guest_ID_in;
        wire [C_Hypervisor_Slave_DATA_WIDTH-1:0] ID_addr1_i;
        wire [C_Hypervisor_Slave_DATA_WIDTH-1:0] ID_addr2_i;
        wire [C_Hypervisor_Slave_DATA_WIDTH-1:0] ID_addr3_i;
        wire [C_Hypervisor_Slave_DATA_WIDTH-1:0] ID_addr4_i;

        wire [C_Hypervisor_Slave_DATA_WIDTH-1:0] Guest_ID_32;

        wire [C_Data_Slave_DATA_WIDTH-1:0] Data1;
        wire [C_Data_Slave_DATA_WIDTH-1:0] Data2;

        wire [63:0] Data;

        assign Guest_ID_in = Guest_ID_32 [1:0];

        assign Data = {Data2, Data1};

        assign clk1 = data_slave_aclk;
        assign rst1 = data_slave_aresetn;

        Prototipo_IPC Prototipo_IPC(

                        .clk(clk1),
                        .rst(rst1),
                        .Guest_ID_i(Guest_ID_in),
                        .ID_addr1_i(ID_addr1_i),
                        .ID_addr2_i(ID_addr2_i),
                        .ID_addr3_i(ID_addr3_i),
                        .ID_addr4_i(ID_addr4_i),
                        .Signal_Guest_o(Signal_Guest_o),
                        .Data_i(Data),
                        .LED_test1(LED_Test1),
                        .LED_test2(LED_Test2),
                        .LED_test3(LED_Test3),
                        .LED_test4(LED_Test4)
                );


        // User logic ends


        endmodule

        module Signal_Manager(
                input clk,
                input rst,
                input [1:0] Guest_ID_i,       // Active Guest ID
                input [1:0] Dest_Guest_ID_i,  // Destination Guest ID
                input Signal_Send_Data_i,     // When Active Guest ID send data to Dest Guest ID
                output reg Signal_Guest_o     // To signal Active Guest to read new message
        );

            reg [3:0] Guest_Signal_r4;

                always @ (posedge clk) begin
                        if (rst) begin
                                Guest_Signal_r4 <= 4'b0000;
                        end
                        if(Signal_Guest_o) begin
                                Guest_Signal_r4[Guest_ID_i] <= 0;
                        end
                        if(Signal_Send_Data_i) begin
                                Guest_Signal_r4[Dest_Guest_ID_i] <= 1'b1;
                        end
                end

            always @(Guest_ID_i) begin
                    Signal_Guest_o = Guest_Signal_r4[Guest_ID_i];
            end

        endmodule


        module Prototipo_IPC(
            input clk,
                input rst,
            input [1:0]  Guest_ID_i,
            input [31:0] ID_addr1_i,
            input [31:0] ID_addr2_i,
            input [31:0] ID_addr3_i,
            input [31:0] ID_addr4_i,

            output Signal_Guest_o,

            input wire  [2047:0] Data_i,

            output reg LED_test1,
            output reg LED_test2,
            output LED_test3,
            output LED_test4
            );

            reg [1:0] Guest_ID_r2;
            reg [31:0] ID_addr1_r32;
            reg [31:0] ID_addr2_r32;
            reg [31:0] ID_addr3_r32;
            reg [31:0] ID_addr4_r32;
            reg [2047:0] Data;

            always @ (ID_addr1_i) begin
                    ID_addr1_r32 <= ID_addr1_i [31:0];
            end

            always @ (ID_addr2_i) begin
                ID_addr2_r32 <= ID_addr2_i [31:0];
            end

            always @ (ID_addr3_i) begin
                ID_addr3_r32 <= ID_addr3_i [31:0];
            end

            always @ (ID_addr4_i) begin
                ID_addr4_r32 <= ID_addr4_i [31:0];
            end

            always @ (Guest_ID_i)
                    Guest_ID_r2 <= Guest_ID_i;

            always @ (Data_i)
                    Data <= Data_i;

            wire [31:0] Dest_Guest_ID_addr_w32;
            wire [1:0]  Dest_Guest_ID_w2;

            assign Dest_Guest_ID_addr_w32 = (Guest_ID_r2 == 0) ? ID_addr1_r32 :
                                                                             (Guest_ID_r2 == 1) ? ID_addr2_r32 :
                                                                         (Guest_ID_r2 == 2) ? ID_addr3_r32 :
                                                                             (Guest_ID_r2 == 3) ? ID_addr4_r32 : 0;

            assign Dest_Guest_Addr_o = Dest_Guest_ID_addr_w32[29:0];
                assign Dest_Guest_ID_w2  = Dest_Guest_ID_addr_w32[31:30];

                wire Dest_Guest_Signal;

                assign Dest_Guest_Signal = (Data != 0) ? 1 : 0;

                Signal_Manager Signal_Manager(
                        .clk(clk),
                        .rst(rst),
                        .Guest_ID_i(Guest_ID_r2),
                        .Dest_Guest_ID_i(Dest_Guest_ID_w2),
                        .Signal_Send_Data_i(Dest_Guest_Signal),
                        .Signal_Guest_o(Signal_Guest_o)
                );




                // Led 3 e 4 will show the Data sent by the active guest
                assign LED_test3 = Data[0];
                assign LED_test4 = Data[1];

                always @(Guest_ID_i)
                begin
                    case (Guest_ID_i)
                    // Led 1 e 2 will show the address selected where the message stored
                    0: begin
                        LED_test1 = ID_addr1_r32[0];
                            LED_test2 = ID_addr1_r32[1];
                    end
                    1:begin
                        LED_test1 = ID_addr2_r32[0];
                            LED_test2 = ID_addr2_r32[1];
                    end
                    2:begin
                        LED_test1 = ID_addr3_r32[0];
                            LED_test2 = ID_addr3_r32[1];
                    end
                    3:begin
                        LED_test1 = ID_addr4_r32[0];
                            LED_test2 = ID_addr4_r32[1];
                    end
                endcase
                end



	endmodule
