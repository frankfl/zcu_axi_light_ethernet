`timescale 1ns / 1ps

// Table 5 defines the optional interface to the device-specific transceiver or to the LVDS SelectIO technology
//transceiver implementation. The core is connected to the chosen transceiver in the HDL example design delivered
//with the core. For a complete description of the device-specific transceiver interface, see the transceiver user guide
//specific to your device. (For user guide information, see [Ref 6], [Ref 7], and [Ref 8] at the end of this document.)
module thousand_base_x_pcs_pma(

    // Connect to RXRESET signal of the transceiver.
    input mgt_rx_reset,


);

endmodule