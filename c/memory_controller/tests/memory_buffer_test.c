#include <stdio.h>
#include <pthread.h>
#include <stdint.h>
#include <signal.h>
#include "receive_buffer.h"

#include "unistd.h"

volatile int stop_condition = 0;

void sigIntHandler(int sig) {
    stop_condition = 1;
}

void printBuffer(const char* msg, const BufferElement *element) {
    printf("%sP: %x, A: %x, V: %x, CS: %x\n", msg, element->preamble, element->address, element->value, element->checksum);
}

void* fillBufferThreadFunction(void* arg) {
    for (uint32_t i = 0; i < 1000000000; i++) {
        BufferElement element = {0x55, i, 0xff, 0x55aa55};
        addElementToRecvBuffer(&element);
        printBuffer("[Thread 1] Add element: ", &element);
        usleep(10000);
    }
    pthread_exit(NULL);
}

void* retrieveDataFromBuffer(void* arg) {
    while (!stop_condition) {
        BufferElement retElement;
        popElementFromRecvBuffer(&retElement);
        printBuffer("   [Thread 2] Recv element: ", &retElement);

        // send socket here!
        usleep(50000);
    }
}

int main() {

    signal(SIGINT, sigIntHandler);
    printf("Initializing Buffer\n");
    initializeBuffer();

    pthread_t thread1, thread2;
    int ret1, ret2;

    ret1 = pthread_create(&thread1, NULL, fillBufferThreadFunction, NULL);
    if (ret1) {
        printf("Error: pthread_create() failed for Thread 1\n");
        return 1;
    }

    ret2 = pthread_create(&thread2, NULL, retrieveDataFromBuffer, NULL);
    if (ret2) {
        printf("Error: pthread_create() failed for Thread 2\n");
        return 1;
    }

    pthread_join(thread1, NULL);
    pthread_join(thread2, NULL);

    deInitializeBuffer();

    return 0;
}
