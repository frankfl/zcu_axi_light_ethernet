/**
 * @file logging.c
 * @brief Implementation of all logging-related functionalities.
 * @author Florian Frank
 * @copyright University of Passau - Chair of Computer Engineering
 */

#include "logging.h"
#include <stdarg.h>
#include "config/app_config.h"
#include "malloc.h"
#include <xil_printf.h>
#include "ip_handler.h"

#include <time.h>

char *log_buffer;

volatile int global_log_lvl;

int initialize_logging(log_lvl lvl) {
    global_log_lvl = lvl;
    log_buffer = malloc(LOG_BUFFER_LEN * LOG_BUFFER_LEN_ADD * sizeof(char));
    return 0;
    if (log_buffer != NULL)
        return 0;
    log_message(LOG_ERROR, _FILE_NAME_, __LINE__,  "malloc returned null!");
    return -1;

}

char* getCurrentTimeStamp(){
    // TODO
    return "00:00:00";
}

void deinitialize_logging() {
    free(log_buffer);
}


const char *getLogStr(log_lvl lvl) {
    if (lvl == LOG_DEBUG) return "Debug";
    else if (lvl == LOG_INFO) return "Info";
    else if (lvl == LOG_WARNING) return "Warning";
    else if (lvl == LOG_ERROR) return "Error";
    return "Undefined";
}

void log_message(log_lvl lvl, char* fileName, int lineNr, char *msg, ...) {
    if (lvl < global_log_lvl)
        return;

    va_list args;
    va_start(args, msg);
    char tmp_buffer[LOG_BUFFER_LEN];
    vsprintf(tmp_buffer, msg, args);
    va_end(args);

    xil_printf("%d [%s:%d] %s: %s \r\n", getCurrentTimeStamp(), fileName, lineNr, getLogStr(lvl), tmp_buffer);
}

void ip_to_str(const ip_addr_t *ip, char* retStr) {
    if(retStr && ip)
        sprintf(retStr, "%d.%d.%d.%d\n\r", ip4_addr1(ip), ip4_addr2(ip),
                   ip4_addr3(ip), ip4_addr4(ip));
    else
        log_message(LOG_ERROR, _FILE_NAME_, __LINE__, "Error IP or strBuf is null!");
}

void ip_settings_to_str(network_config *config, char* retStr) {
    char tmpBuffIP[18]; // max length of an IPV4 address or mask e.g. 192.168.178.125
    char tmpBuffNetMask[18];
    char tmpBuffGw[18];

    ip_to_str(&config->ipaddr, tmpBuffIP);
    ip_to_str(&config->netmask, tmpBuffNetMask);
    ip_to_str(&config->gw, tmpBuffGw);

    sprintf(retStr, "\nIP: %s Netmask: %s Gateway: %s\n", tmpBuffIP, tmpBuffNetMask, tmpBuffGw);
}
