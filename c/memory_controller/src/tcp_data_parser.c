/**
 * @file tcp_data_parser.c
 * @brief Implementation of all functions to identify commands, parse the payload and execute the corresponding actions.
 * @author Florian Frank
 * @copyright University of Passau - Chair of Computer Engineering
 */

#include "tcp_data_parser.h"
#include "logging.h"
#include "ps_pl_controller.h"
#include <string.h>

#define IDN_STR "Zync Ultrascale+ MPSoC ZCU102 Evaluation Kit"
#define STATUS_OK "{\"status\": \"ok\"}"
#define STATUS_RUNNING "{\"status\": \"running\"}"

#define JSON_TOKEN_SIZE 128

Command parseIdentifier(const char *message);
const char *cmdToIdn(const Command command);
void parsePayload(const char *inputMsg, Arguments *arguments);
void generateResponse(Command cmd, char *response);
const char* find_next(const char* str, const char* target);

/**
 * Identifies a command e.g. *IDN?\n
 * @param message message containing the identify and transforms them into a Command type.
 * If the command is undefined CMD_UNDEFINED is returned.
 * @return parsed Command
 */
Command parseIdentifier(const char *message) {
    Command cmd = CMD_UNDEFINED;
    if (strcmp(message, IDENT_CMD_IDN) == 0)
        cmd = CMD_IDN;
    else if (strstr(message, IDENT_START_MEASUREMENT))
        cmd = CMD_START_MEASUREMENT;
    else if (strstr(message, IDENT_STOP_MEASUREMENT))
        cmd = CMD_STOP_MEASUREMENT;
    else if (strstr(message, IDENT_RETRIEVE_STATUS))
        cmd = CMD_RETRIEVE_STATUS;
    else if (strstr(message, IDENT_RESET))
        cmd = CMD_RESET;

    if (cmd == CMD_UNDEFINED)
        log_message(LOG_WARNING, _FILE_NAME_, __LINE__, "Command %s undefined", message);
    else
        log_message(LOG_INFO, _FILE_NAME_, __LINE__, "%s Identified", cmdToIdn(cmd));

    return cmd;
}

/**
 * @brief Transforms a Command into a string identifier.
 * @param command command struct containing the command identifier.
 * @return command identifier as string.
 */
const char *cmdToIdn(const Command command) {
    if (command == CMD_IDN)
        return IDENT_CMD_IDN;
    else if (command == CMD_START_MEASUREMENT)
        return IDENT_START_MEASUREMENT;
    else if (command == CMD_STOP_MEASUREMENT)
        return IDENT_STOP_MEASUREMENT;
    else if (command == CMD_RESET)
        return IDENT_RESET;
    return "Undefined";
}

const char* find_next(const char* str, const char* target) {
    const char* result = strstr(str, target);
    return result ? result + strlen(target) : NULL;
}

/**
 * TODO improve stability of this json parsing method!!
 * @param inputMsg
 * @param key_list
 * @param value_list
 * @param retLen
 */
void parsePayload(const char *inputMsg, Arguments *arguments) {
    const char* payload_start = find_next(inputMsg, "\"payload\":");
    if (!payload_start) {
        log_message(LOG_ERROR, _FILE_NAME_, __LINE__,"Error: 'payload' not found in JSON.");
        return;
    }

    int keyIndexCtr = 0;

    const char* token_start = NULL;
    const char* token_end = NULL;

    while ((token_start = find_next(payload_start, "\"")) && (token_end = find_next(token_start + 1, "\""))) {
        int diff = token_end - token_start;
        if (diff > 0 && keyIndexCtr < 10) {
            char token[JSON_TOKEN_SIZE];
            strncpy(token, token_start, diff);
            token[diff] = '\0';

            if (keyIndexCtr % 2 == 0) {
                strcpy(arguments->key_list[keyIndexCtr / 2], token);
            } else {
                strcpy(arguments->value_list[arguments->nrActualArguments], token);
                (arguments->nrActualArguments)++;
            }

            payload_start = token_end + 1;
            keyIndexCtr++;
        } else {
            log_message(LOG_ERROR, _FILE_NAME_, __LINE__,"Error while parsing arguments");
            break;
        }
    }
}

/**
 * @brief Parses the input command and generates a response based on the command.
 * If the command is not CMD_UNDEFINED, the command is sent to the PL and the response is returned.
 * @param input_command
 * @param output_response
 */
void parseInputCommands(const char *input_command, Response *output_response) {
    output_response->cmd = CMD_UNDEFINED;


    log_message(LOG_INFO, _FILE_NAME_, __LINE__, "Received command: %s", input_command);

    output_response->cmd = parseIdentifier(input_command);

    // No interaction with PL when executing these two commands. Directly return response.
    if (output_response->cmd != CMD_UNDEFINED &&  output_response->cmd != CMD_IDN){
        output_response->arguments.nrActualArguments = 0;
        parsePayload(input_command, &output_response->arguments);
        sendCommandAXI(output_response);
    }

    generateResponse(output_response->cmd, output_response->responseMsg);
}

/**
 * @brief Generates a json response or device identifier, based on the passed command.
 * @param cmd command to be parsed and transformed into a response.
 * @param response response which is returned based on the provided command.
 */
void generateResponse(Command cmd, char *response) {
    if (cmd == CMD_IDN)
        strcpy(response, IDN_STR);
    else if (cmd == CMD_START_MEASUREMENT)
        strcpy(response, STATUS_OK);
    else if (cmd == CMD_STOP_MEASUREMENT)
        strcpy(response, STATUS_OK);
    else if (cmd == CMD_RETRIEVE_STATUS)
        strcpy(response, STATUS_RUNNING);
    else if (cmd == CMD_RESET)
        strcpy(response, STATUS_OK);
    else
        strcpy(response, "{\"error\": \"Command undefined\"}");
}