/**
 * @file irq_handler.c
 * @brief Implementation of an interrupt handler to react on notifications of incoming data from the PL.
 * @author Florian Frank
 * @copyright University of Passau - Chair of Computer Engineering
 */

#include "irq_handler.h"
#include "xscugic.h"
#include "platform/platform.h"
#include "xil_printf.h"
#include "xil_types.h"
#include <xparameters.h>
#include "ps_pl_controller.h"
#include "logging.h" // log_message
#include "ip_handler.h"
#include "receive_buffer.h"

static XScuGic_Config *intr_cfg;
static XScuGic intr_ctrl;

#define INTC_DEVICE_ID        XPAR_SCUGIC_0_DEVICE_ID
#define INTC_INTERRUPT_ID    XPS_FPGA0_INT_ID

//There are 32 priority levels supported with a step of 8. Hence the supported priorities are 0, 8, 16, 32, 40 ..., 248.
// 0 is the highest priory
#define PRIORITY_LVL    0x0A

// . Each bit pair describes the configuration for an INT_ID. SFI Read Only b10 always PPI Read Only depending on
// how the PPIs are configured. b01 Active HIGH level sensitive b11 Rising edge sensitive SPI LSB is read only. b01 Active HIGH level sensitive
// b11 Rising edge sensitive
// Our interrupt is rising edge sensitive
#define TRIGGER_TYPE    0x03

int initializeIRQHandler() {
    log_message(LOG_INFO, _FILE_NAME_, __LINE__, "Start initialize IRQ Handler");

    intr_cfg = XScuGic_LookupConfig(INTC_DEVICE_ID);
    if (NULL == intr_cfg) {
        log_message(LOG_ERROR, _FILE_NAME_, __LINE__, "XScuGic_LookupConfig returned null -> return XST_FAILURE");
        return XST_FAILURE;
    }

    s32 status = XScuGic_CfgInitialize(&intr_ctrl, intr_cfg, intr_cfg->CpuBaseAddress);
    if (status != XST_SUCCESS) {
        log_message(LOG_ERROR, _FILE_NAME_, __LINE__,
                    "XScuGic_CfgInitialize returned with error code %x -> return XST_FAILURE", status);
        return XST_FAILURE;
    }

    log_message(LOG_INFO, _FILE_NAME_, __LINE__, "Set interrupt with priority ");
    XScuGic_SetPriorityTriggerType(&intr_ctrl, INTC_INTERRUPT_ID, PRIORITY_LVL, TRIGGER_TYPE);

    // This handler is being called when the processor encounters the specified exception.
    Xil_ExceptionRegisterHandler(INTC_INTERRUPT_ID, (Xil_ExceptionHandler) XScuGic_InterruptHandler, &intr_ctrl);

    status = XScuGic_Connect(&intr_ctrl, INTC_INTERRUPT_ID, (Xil_InterruptHandler) plMessageReceiveInterruptHandler, NULL);
    if (status != XST_SUCCESS) {
        log_message(LOG_ERROR, _FILE_NAME_, __LINE__,
                    "XScuGic_Connect returned with error code %x -> return XST_FAILURE", status);
        return XST_FAILURE;
    }

    XScuGic_Enable(&intr_ctrl, INTC_INTERRUPT_ID);
    log_message(LOG_INFO, _FILE_NAME_, __LINE__, "Interrupt with ID %x enabled", INTC_INTERRUPT_ID);

    return XST_SUCCESS;
}

/**
 * Deinitializes the interrupt handler by disconnecting the interrupt and removing the handler.
 */
void deInitializeIRQHandler(){
    log_message(LOG_INFO, _FILE_NAME_, __LINE__, "Deinitialize interrupt with ID %x", INTC_INTERRUPT_ID);
    XScuGic_Disable(&intr_ctrl, INTC_INTERRUPT_ID);

    XScuGic_Disconnect (&intr_ctrl, INTC_INTERRUPT_ID);
    Xil_ExceptionRemoveHandler(INTC_INTERRUPT_ID);
}

/**
 * Interrupt handler is fired when a rising edge from the axi_light_master_txn_done signal on the PL is received.
 * It disables the interrupt while storing the data within the receive buffer.
 * @param CallbackRef Currently not used.
 */
void plMessageReceiveInterruptHandler(void *CallbackRef) {
    // TODO how to get feedback from the PL when interrupt is disabled?
    XScuGic_Disable(&intr_ctrl, INTC_INTERRUPT_ID);

    uint32_t preamble;
    uint32_t address;
    uint32_t value;
    uint32_t checksum;
    read_data_frame(&preamble, &address, &value, &checksum);
    addElementToRecvBuffer(preamble, address, value, checksum);

    /*char buffer[256];
    int len = sprintf(buffer, "P: %x, A: %x, V: %x, C: %x\n", preamble, address, value, checksum);

    sendTCPPackageToServer(buffer, len);*/
    XScuGic_Enable(&intr_ctrl, INTC_INTERRUPT_ID);
}
