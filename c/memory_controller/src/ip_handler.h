#ifndef _IP_HANDLER_
#define _IP_HANDLER_

#include "ip_handler.h"
#include "lwip/dhcp.h" // TODO less generic import

struct network_config {
	struct netif *netif_handle;
	ip_addr_t ipaddr, netmask, gw;

} typedef network_config;


int initializeNetworkStack(network_config *config);
void cleanupNetworkStack(network_config *config);
int initializeBoardSpecificComponents();

int setDefaultIPConfig(network_config *config);
void resetIPConfig(network_config *config);
int startDHCPService(network_config *config);
int startServerMode();
int connectToClient(char* ipAddress, uint16_t port);
void sendPackageToServer(char *buffer, uint32_t len);
int isClientConnected();
void runRecvSendLoop(network_config *config);

#endif // _IP_HANDLER_
