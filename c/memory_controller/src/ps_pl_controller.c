/**
 * @file ps_pl_controller.c
 * @brief Implements the interaction between the PS and PL utilizing a AXI-light master and a dedicated slave interface.
 * @author Florian Frank
 * @copyright University of Passau - Chair of Computer Engineering
 */

#include "ps_pl_controller.h"

#include <stdio.h>
#include "xil_printf.h"
#include "xil_io.h"
#include "platform/platform.h"
#include "logging.h"
#include <xparameters.h>
#include "xil_cache.h"

#define ADDRESS_OFFSET 0x40000000

/**
 * Inserts a value 0x55aa into a register with a certain width and offset. .e.g a 16 bit value should be inserted with an offset of 8-bit
 * it results in a register 0x0055aa00;
 * @param valueToSet value which is inserted in the register.
 * @param width width of the value. (All input values are 32-bit integers)
 * @param offset offset in bit to insert.
 * @param value value stored as 32 bit integer to insert at a certain position.
 */
void setRegister(uint32_t *valueToSet, uint32_t width, uint32_t offset, uint32_t value) {
    // Calculate a mask e.g. when width == 16, the mask is 0x0000FFFF
    uint32_t mask = (width < 0x20) ? (~(0xFFFFFFFF << width) ) : 0xFFFFFFFF;
    // Shift result of value and mask and insert it into the register.
    // e.g. when the mask is 0x0000FFFF and the value is 0x55aa, the result is 0x000055aa shifted by offset of 8 results in 0x0055aa00
    // if e.g. 0x02 is already written in the register, the result is 0x0055aa02.
    *valueToSet |= (mask & value) << offset;
}

/**
 * Parses the input response message, retrieves the command parameters and sends them to the PL utilizing the AXI-light interface,
 * mapped into DRAM memory.
 * @param response containing the received command, such as a list of arguments
 * @return
 */
int sendCommandAXI(Response *response) {
    log_message(LOG_INFO, _FILE_NAME_, __LINE__, "Send command to AXI interface");
    //write_register(0, response->arguments.);
    uint32_t reg0 = 0x00;
    uint32_t reg1 = 0x00;
    uint32_t reg2 = 0x00;
    uint32_t reg3 = 0x00;
    uint32_t reg4 = 0x00;
    uint32_t reg5 = 0x00;
    uint32_t reg6 = 0x00;
    uint32_t reg7 = 0x00;

    // set 8 bit command type, e.g. START_MEASUREMENT, STOP_MEASUREMENT or RESET
    setRegister(&reg0, WIDTH_CMD, 0, response->cmd);

    if (response->cmd == CMD_START_MEASUREMENT) {

        for (int argumentCtr = 0; argumentCtr < response->arguments.nrActualArguments; argumentCtr++) {
            if (strcmp(response->arguments.key_list[argumentCtr], STR_IDENTIFIER_MEASURE_TYPE) == 0) {
                // Set 8-bit measurement type, e.g. WRITE_LATENCY, READ_LATENCY, etc.
                setRegister(&reg0, WIDTH_MEASURE_TYPE, WIDTH_CMD, atoi(response->arguments.value_list[argumentCtr]));
            } else if (strcmp(response->arguments.key_list[argumentCtr], STR_IDENTIFIER_INIT_VALUE) == 0) {
                // Set 16-bit init value, e.g. 0x5555
                setRegister(&reg0, WIDTH_INIT_VALUE, WIDTH_CMD + WIDTH_MEASURE_TYPE,
                            atoi(response->arguments.value_list[argumentCtr]));
            } else if (strcmp(response->arguments.key_list[argumentCtr], STR_IDENTIFIER_START_ADDRESS) == 0) {
                // Set 32-bit startAddress
                setRegister(&reg1, WIDTH_ADDRESS, 0, atoi(response->arguments.value_list[argumentCtr]));
            } else if (strcmp(response->arguments.key_list[argumentCtr], STR_IDENTIFIER_STOP_ADDRESS) == 0) {
                // Set 32-bit stopAddress
                setRegister(&reg2, WIDTH_ADDRESS, 0, atoi(response->arguments.value_list[argumentCtr]));
            } else if (strcmp(response->arguments.key_list[argumentCtr], STR_IDENTIFIER_AUXILIARY_0) == 0) {
                // Set 32-bit auxiliary data 0, e.g. timing parameter when executing read or write latency tests.
                setRegister(&reg3, WIDTH_ADDRESS, 0, atoi(response->arguments.value_list[argumentCtr]));
            } else if (strcmp(response->arguments.key_list[argumentCtr], STR_IDENTIFIER_AUXILIARY_1) == 0) {
                // Set 32-bit auxiliary data 1
                setRegister(&reg4, WIDTH_ADDRESS, 0, atoi(response->arguments.value_list[argumentCtr]));
            } else if (strcmp(response->arguments.key_list[argumentCtr], STR_IDENTIFIER_AUXILIARY_2) == 0) {
                // Set 32-bit auxiliary data 2
                setRegister(&reg5, WIDTH_ADDRESS, 0, atoi(response->arguments.value_list[argumentCtr]));
            } else if (strcmp(response->arguments.key_list[argumentCtr], STR_IDENTIFIER_AUXILIARY_3) == 0) {
                // Set 32-bit auxiliary data 3
                setRegister(&reg6, WIDTH_ADDRESS, 0, atoi(response->arguments.value_list[argumentCtr]));
            } else if (strcmp(response->arguments.key_list[argumentCtr], STR_IDENTIFIER_AUXILIARY_4) == 0) {
                // Set 32-bit auxiliary data 4
                setRegister(&reg7, WIDTH_ADDRESS, 0, atoi(response->arguments.value_list[argumentCtr]));
            }
        }
    } else if (response->cmd == CMD_STOP_MEASUREMENT) {
        // Currently no specific parameters are required.
    } else if (response->cmd == CMD_RESET) {
        // Currently no specific parameters are required
    } else if (response->cmd == CMD_RETRIEVE_STATUS) {
        // Currently no specific parameters are required
    } else {
        log_message(LOG_ERROR, _FILE_NAME_, __LINE__, "Command with opcode: 0x%x not supported", response->cmd);
        return -1;
    }

    log_message(LOG_INFO, _FILE_NAME_, __LINE__, "Write AXI registers");
    write_register(0x00, reg0);
    write_register(0x01, reg1);
    write_register(0x02, reg2);
    write_register(0x03, reg3);

    // TODO adjust timing or extend set of registers
    int i = 0;
    while (i < 10) {
        i++;
    }

    write_register(0x00, reg4);
    write_register(0x01, reg5);
    write_register(0x02, reg6);
    write_register(0x03, reg7);

    return 0;
}

/**
 * Sends an empty START_MEASUREMENT command. Only used for starting the AXI data transfer.
 * Otherwise the interface starts sending with WREAD being high before entering the main function.
 * @return 0 if no error occurred, else return error code.
 */
int initialize_axi_interface() {
    log_message(LOG_INFO, _FILE_NAME_, __LINE__, "Initialize AXI interface");

    Response response;
    response.cmd = CMD_START_MEASUREMENT;
    response.arguments.nrActualArguments = 0;
    return sendCommandAXI(&response);
}

/**
 * Reads four byte from the AXI-slave interface, containing the preamble, address, value and checksum.
 * @param preamble indicates the start of a frame.
 * @param address returns the processed memory address.
 * @param value returns the read value at the address.
 * @param checksum checksum to detect transmission errors.
 */
void read_data_frame(uint32_t *preamble, uint32_t *address, uint32_t *value, uint32_t *checksum) {
    *preamble = Xil_In32(XPAR_PSU_DDR_0_S_AXI_BASEADDR + ADDRESS_OFFSET);
    *address =  Xil_In32(XPAR_PSU_DDR_0_S_AXI_BASEADDR + ADDRESS_OFFSET + 0x04);
    *value =    Xil_In32(XPAR_PSU_DDR_0_S_AXI_BASEADDR + ADDRESS_OFFSET + 0x08);
    *checksum = Xil_In32(XPAR_PSU_DDR_0_S_AXI_BASEADDR + ADDRESS_OFFSET + 0x0c);
    Xil_DCacheFlush();
}

/**
 * Write a single register, starting at the base address.
 * @param reg Describes the offset from the base address, multiplied by four, due to 32-bit packages transmitted across the AXI-bus.
 * @param value 32-bit value to write.
 */
void write_register(uint8_t reg, uint32_t value) {
    Xil_Out32(XPAR_PS_PL_INTERFACE_0_AXI_LIGHT_SLAVE_BASEADDR + (reg * 0x04), value);
}


