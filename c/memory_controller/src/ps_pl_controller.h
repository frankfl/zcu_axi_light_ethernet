#ifndef _AXI_CONTROLLER_
#define _AXI_CONTROLLER_

#include "command_config.h"

#include <stdint.h> // uint8_t, uint32_t


int initialize_axi_interface();
int sendCommandAXI(Response *response);
void write_register(uint8_t reg, uint32_t value);
void read_data_frame(uint32_t *preamble, uint32_t *address, uint32_t * value, uint32_t *checksum);


#endif // _AXI_CONTROLLER_
