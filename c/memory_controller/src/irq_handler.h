#ifndef _IRQ_HANDLER_H_
#define _IRQ_HANDLER_H_

int initializeIRQHandler();
void deInitializeIRQHandler();
void plMessageReceiveInterruptHandler (void *CallbackRef);

#endif // _IRQ_HANDLER_H_
