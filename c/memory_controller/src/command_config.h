/**
 * @file command_config.h
 * @brief This file contains all definitions required to parse commands from the TCP/IP interface and forwards them to the PL.
 * @author Florian Frank
 * @copyright University of Passau - Chair of Computer Engineering
 */

#ifndef MEMORY_TEST_CONTROLLER_COMMAND_CONFIG_H
#define MEMORY_TEST_CONTROLLER_COMMAND_CONFIG_H

// Command identifier also used by the PL module to select the corresponding argument parser.
#define CMD_UNDEFINED 			        0xff
#define CMD_IDN 				        0x01
#define CMD_START_MEASUREMENT 	        0x02
#define CMD_STOP_MEASUREMENT 	        0x03
#define CMD_RETRIEVE_STATUS 	        0x04
#define CMD_RESET 			    	    0x05

// Widths of the commands, forwarded to the PL
#define WIDTH_CMD                       0x08
#define WIDTH_MEASURE_TYPE              0x08
#define WIDTH_INIT_VALUE                0x10
#define WIDTH_ADDRESS                   0x20
#define WIDTH_AUXILIARY_DATA_BLOCK      0x20

// General string identifier for all supported commands
#define IDENT_CMD_IDN			        "*IDN?\n"
#define IDENT_START_MEASUREMENT         "MEASUREMENT START\n"
#define IDENT_STOP_MEASUREMENT	        "MEASUREMENT STOP\n"
#define IDENT_RETRIEVE_STATUS           "STATUS\n"
#define IDENT_RESET			            "RESET\n"

// Argument identifier when receiving a IDENT_START_MEASUREMENT command
#define STR_IDENTIFIER_MEASURE_TYPE     "measure_type"
#define STR_IDENTIFIER_INIT_VALUE       "init_value"
#define STR_IDENTIFIER_START_ADDRESS    "start_address"
#define STR_IDENTIFIER_STOP_ADDRESS     "stop_address"
#define STR_IDENTIFIER_AUXILIARY_0      "auxiliary_0"
#define STR_IDENTIFIER_AUXILIARY_1      "auxiliary_1"
#define STR_IDENTIFIER_AUXILIARY_2      "auxiliary_2"
#define STR_IDENTIFIER_AUXILIARY_3      "auxiliary_3"
#define STR_IDENTIFIER_AUXILIARY_4      "auxiliary_4"

#define MAX_NR_ARGUMENTS                10
#define MAX_KEY_VALUE_LEN               128
#define MAX_RESPONSE_STR_LEN            256

typedef int Command;

/**
 * Struct holding the arguments of a command, organized in key values. Based on the key identifier, the
 * data type is identified and the values are parsed properly.
 */
struct Arguments {
    char key_list[MAX_NR_ARGUMENTS][MAX_KEY_VALUE_LEN];
    char value_list[MAX_NR_ARGUMENTS][MAX_KEY_VALUE_LEN];
    int nrActualArguments;
} typedef Arguments;

/**
 * Response struct containing the command identifier and corresponding arguments.
 */
struct Response {
    Command cmd;
    Arguments arguments;
    char responseMsg[MAX_RESPONSE_STR_LEN]; // TODO not necessary?
} typedef Response;

#endif //MEMORY_TEST_CONTROLLER_COMMAND_CONFIG_H
