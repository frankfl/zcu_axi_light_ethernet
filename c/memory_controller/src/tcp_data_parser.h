#ifndef _TCP_DATA_PARSER_
#define _TCP_DATA_PARSER_

#include "command_config.h"

void parseInputCommands(const char* command, Response* response);

#endif // _TCP_DATA_PARSER_
