/**
 * @file receive_buffer.c
 * @brief Implementation of a buffer to store data received from the PL/PS interface.
 * @author Florian Frank
 * @copyright University of Passau - Chair of Computer Engineering
 */

#include "receive_buffer.h"
#include "malloc.h"
#include "logging.h"

#define BUFFER_SIZE 2048

BufferElement *recvBuffer;
uint32_t readIndexCtr;
uint32_t writeIndexCtr;

// TODO gegen buffer overflows absichern !!
uint64_t overallWriteCtr;
uint64_t overallReadCtr;


int initializeBuffer() {
    log_message(LOG_INFO, _FILE_NAME_, __LINE__, "Setup receive Buffer of size %x", BUFFER_SIZE);
    recvBuffer = malloc(BUFFER_SIZE * sizeof(BufferElement));
    if(recvBuffer){
        readIndexCtr = 0;
        writeIndexCtr = 0;
        overallWriteCtr = 0;
        return 0;
    }
    return -1;
}

void deInitializeBuffer() {
    free(recvBuffer);
}

void addElementToRecvBuffer(BufferElement *bufferElem) {
    // TODO noch absichern falls write den readcounter überschreibt
    recvBuffer[writeIndexCtr] = *bufferElem;
    overallWriteCtr++;
    writeIndexCtr = (writeIndexCtr + 1) % BUFFER_SIZE;
}

int popElementFromRecvBuffer(BufferElement *recvElement){
    if (overallReadCtr < overallWriteCtr) {
        *recvElement = recvBuffer[readIndexCtr];
        overallReadCtr++;
        readIndexCtr = (readIndexCtr + 1) % BUFFER_SIZE;
        return 1;
    }
    return 0;
}